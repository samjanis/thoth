CC = gcc
CFLAGS = `pkg-config --libs --cflags gtk+-3.0 libcurl libcrypto`
VPATH = src
BUILD_DIR = build
APP_NAME = thoth

all: build test

test: main.c
	$(CC) -o $(APP_NAME) $< $(CFLAGS)

build:
	mkdir -p $(BUILD_DIR)

.PHONY: clean

clean:
	rm -rf $(BUILD_DIR) $(APP_NAME)
