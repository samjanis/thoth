/**
 * Thoth
 * https://www.mirrorisland.com/thoth
 * Copyright 2024
 * Sam Allen <samjanis@mirrorisland.com>
 *
 * gcc src/main.c -o thoth `pkg-config --libs --cflags gtk+-3.0 libcurl libcrypto`
 */

#include <gtk/gtk.h>
#include <glib/gstdio.h>

#include <curl/curl.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

#include "main.h"

#define MAXPATH 65535
#define AUTOSAVE_TIMEOUT_SECONDS 1

/* Sidepanel (SPL) treestore columns */
enum {
	SPL_COL_ICON = 0,
	SPL_COL_COLOUR,
	SPL_COL_TITLE,
	SPL_COL_URI,
	SPL_COL_TOOLTIP,
	SPL_COL_MODIFIED,
	SPL_COL_VISIBLE,
	SPL_COL_ID,
	SPL_COL_CATEGORY,
	SPL_NUM_COLS
};

/* Password dialog (PWD) treestore columns */
enum {
	PWD_COL_PAGEID = 0,
	PWD_COL_CHECKBOX,
	PWD_COL_URI,
	PWD_COL_PROTOCOL,
	PWD_COL_USERNAME,
	PWD_COL_PASSWORD,
	PWD_NUM_COLS
};

/* Web open/save Recent Combobox (WRC) list store columns */
enum {
	WRC_COL_PAGEID = 0,
	WRC_COL_TITLE,
	WRC_COL_URI,
	WRC_COL_PROTOCOL,
	WRC_COL_USERNAME,
	WRC_COL_PASSWORD,
	WRC_COL_BASE64IV,
	WRC_NUM_COLS
};

/* FTP Browser dialog (FTP) listview columns */
enum {
	FTP_COL_TOOLTIP = 0,
	FTP_COL_PERMISSIONS,
	FTP_COL_ISDIRECTORY,
	FTP_COL_SIZE,
	FTK_COL_DATE,
	FTP_COL_FILENAME,
	FTP_NUM_COLS
};

/* Keys for undo/redo. Saved in undodata->command */
enum {
	UNDO_INS = 0,
	UNDO_BS,
	UNDO_DEL
};

/* Close button actions. */
enum {
	CLOSEBTN_QUIT = 0,
	CLOSEBTN_MINIMISE,
	CLOSEBTN_CLOSE_FILE,
	CLOSEBTN_CLOSE_FILE_AND_MINIMISE,
	CLOSEBTN_CLOSE_FILE_AND_QUIT
};

/* Menu items */
enum {
	MENU_INCOMPLETE = 0,
	FILE_NEW,
	FILE_OPEN,
	FILE_OPEN_WEB_ADDRESS,
	FILE_RELOAD,
	FILE_SAVE_AS,
	FILE_SAVE_TO_WEB,
	FILE_SHARE,
	FILE_SEPARATOR_1, /* Needed to keep FILE_RECENT_PLACEHOLDER correct */
	FILE_PRINT_PREVIEW,
	FILE_PRINT,
	FILE_EXPORT,
	FILE_PROPERTIES,
	FILE_SEPARATOR_2, /* Needed to keep FILE_RECENT_PLACEHOLDER correct */
	FILE_CLOSE,
	FILE_QUIT,
	FILE_SEPARATOR_3, /* Needed to keep FILE_RECENT_PLACEHOLDER correct */
	FILE_RECENT_PLACEHOLDER,
	RECENT_MENU_INSERT,
	EDIT_CUT,
	EDIT_COPY,
	EDIT_PASTE,
	EDIT_SELECT_ALL,
	EDIT_FIND,
	EDIT_JUMPTO,
	EDIT_INDENT,
	EDIT_UNINDENT,
	EDIT_UPPERCASE,
	EDIT_LOWERCASE,
	EDIT_REMOVE_TRAILING_SPACES,
	OPTIONS_WRAP_LINES,
	OPTIONS_SHOW_LINE_NUMBERS,
	OPTIONS_AUTOINDENT,
	OPTIONS_KEEP_ABOVE,
	OPTIONS_REOPEN_FILES_ON_STARTUP,
	OPTIONS_PREFERENCES,
	OPTIONS_ABOUT,
	NUM_MENU_ITEMS, /* End menu items here. Everything else below are global commands (e.g. toolbar buttons) */
	COMMAND_INCOMPLETE,
	COMMAND_SIDEPANEL,
	COMMAND_SAVE,
	COMMAND_UNDO,
	COMMAND_REDO,
	COMMAND_FIND_PREVIOUS,
	COMMAND_FIND_NEXT,
	COMMAND_FIND_REPLACE,
	COMMAND_FIND_REPLACE_ALL,
	COMMAND_FIND_CLOSE,
	COMMAND_JUMPTO,
	COMMAND_CLOSE_WINDOW,
	COMMAND_NEW,
	COMMAND_PASTE_NEW,
	COMMAND_OPEN,
	COMMAND_OPEN_WEB,
	COMMAND_QUIT,
	NUM_COMMAND_ITEMS
};

/* Assorted enumerations for function values, etc. */
enum {
	THOTH_UNKNOWN = 0,
	THOTH_TREEVIEW,
	THOTH_PAGE,
	THOTH_NOTE,
	THOTH_FILE,
	THOTH_WEB_OPEN,
	THOTH_WEB_SAVE,
	THOTH_HTTP,
	THOTH_FTP
};

/* Modifier keys for shortcuts, eg. the switcher.
 * These are in powers of 2 for bitwise operations */
enum {
	THOTH_MODKEY_UNKNOWN = 0,
	THOTH_MODKEY_CTRL_L = 1,
	THOTH_MODKEY_CTRL_R = 2
};

/**
 * Unix file permissions.
 */
enum {
	THOTH_OWNERR = 0b100000000,
	THOTH_OWNERW = 0b10000000,
	THOTH_OWNERX = 0b1000000,
	THOTH_GROUPR = 0b100000,
	THOTH_GROUPW = 0b10000,
	THOTH_GROUPX = 0b1000,
	THOTH_WORLDR = 0b100,
	THOTH_WORLDW = 0b10,
	THOTH_WORLDX = 0b1
};

typedef struct {
	gint command;
	gint start;
	gint end;
	gboolean chain; /* Chain with the previous undo item */
	gchar *str;
} ThothUndoStruct;

typedef struct {
	GtkWidget *menuitem;
	gchar *uri;
} ThothRecentStruct;

typedef struct {
	gint id;
	gint category;
	gchar *uri;
	gchar *filepath;
	gchar *title;
	gchar *protocol;
	gchar *username;
	gchar *password;
	gchar *base64iv;
	gint unixpermissions; /* User/group/world; dec. representation of octal (777) */
	gint mrispermissions; /* Mirror Island View/Edit/Reply/Approve/Moderate/Tag; 6 digits of 0-5 (555555) */
	GtkWidget *scrolledwindow;
	GtkWidget *textview;
	GtkTextBuffer *textbuffer;
	GtkTreeIter *treeiter;
	GList *undolist;
	gint undoindex;
	gint64 accesstime; /* Used for switcher and tray icon menu */
	gulong modifiedchangedid;
	gulong keypresseventid;
	gulong scrolleventid;
	gulong inserttextid;
	gulong deleterangeid;
	gulong beginuseractionid;
	gulong enduseractionid;
	gboolean modified;
	gboolean wraplines;
	gboolean linenumbers;
} ThothEditorStruct;

typedef struct {
	ThothEditorStruct *editor;
	GtkWidget *button;
} ThothSwitcherStruct;

typedef struct {
	char *chars;
	size_t len;
} ThothCharString;

typedef struct {
	GtkListStore *webrecentliststore;
	GtkWidget *protocolcombo;
	GtkWidget *addressentry;
	GtkWidget *usernameentry;
	GtkWidget *passwordentry;
} ThothWebLoginStruct;

/* Thoth Inline Copy Button menu item */
struct _TicbMenuItemPrivate {
	GtkWidget *label;
	GtkWidget *button;
	GtkWidget *hbox;
	GtkWidget *button_box;

	ThothEditorStruct *editor;
};

G_DEFINE_TYPE_WITH_PRIVATE (TicbMenuItem, ticb_menu_item, GTK_TYPE_MENU_ITEM);

gint dbg = 0; /* printf output incrementer */

/* Global variables */
gint thoth_loaded = FALSE;
GtkStatusIcon *thoth_statusicon = NULL;
GtkWidget *thoth_statusmenu;
GList *thoth_statusmenulist;
GtkWidget *thoth_menuitem[NUM_MENU_ITEMS];
gulong thoth_signalhandler[NUM_COMMAND_ITEMS]; /* Disconnect using these. Includes menu and global commands */
GtkWidget *thoth_buffertarget;
GtkWidget *thoth_filesmenu;
GtkWidget *thoth_preferences_dialog;
GtkApplication *thoth_app;
GtkToolItem *thoth_sidepaneltoolitem;
GtkWidget *thoth_sidepanelframe;
gchar *thoth_uri; /* URI of open file. */
gchar *thoth_filepath; /* Path to local copy of file */
gchar *thoth_protocol;
gchar *thoth_username;
gchar *thoth_password;
gchar *thoth_base64iv;
gchar *thoth_master_password;
gboolean thoth_savepassword;
GtkClipboard *thoth_clipboard;
static GList *thoth_editorlist = NULL;
ThothEditorStruct *thoth_editor;
GtkWidget *thoth_window;
GtkWidget *thoth_editoroverlay;
gint thoth_width = 540, thoth_height = 400;
gint thoth_left = 0, thoth_top = 0;
GtkWidget *thoth_label;
GtkWidget *thoth_findwidget;
GtkWidget *thoth_findentry, *thoth_replaceentry, *thoth_casesensitivecheck;
GtkWidget *thoth_jumptohbox, *thoth_jumptospinentry = NULL;
GtkWidget *thoth_permissionsentry;
static GList *thoth_permissionscheckboxlist = NULL;
gboolean thoth_permissionsentrychanging = FALSE;
GtkToolItem *thoth_savetoolitem;
GtkToolItem *thoth_undotoolitem;
GtkToolItem *thoth_redotoolitem;
GtkTreeIter thoth_notesiter;
GtkTreeIter thoth_pagesiter;
GtkTreeIter thoth_openfilesiter;
static gint thoth_last_keyval = 0;
GtkTreeModel *thoth_treemodel;
GtkWidget *thoth_treeview;
GtkWidget *thoth_notebook;
gint thoth_lasteditorid = 0;
gint64 thoth_lastgetrealtime = 0;
guint thoth_autosavetimeout = 0;
gint thoth_linenumberswidth = 10; /* Default minimum */
GtkListStore *thoth_passwordliststore;
GtkWidget *thoth_passwordtreeview;
GtkWidget *thoth_switcher;
gint thoth_switcherlist_max = 16; /* Needs to be user configurable */
GList *thoth_switcherlist;
gint thoth_switcherlist_index = 0;
gint thoth_switcherlist_count = 0;
gint thoth_modkeys = 0; /* Store left and right Ctrl */
gboolean thoth_undo_chain; /* Set TRUE to chain undo actions */
gboolean thoth_undo_chain_started; /* Set FALSE for first chain item */
gboolean thoth_new_undo_action = FALSE; /* TRUE makes next edit a separate undo action */
static GtkPrintSettings *thoth_printsettings = NULL;
static PangoLayout *thoth_print_layout;
static gint thoth_print_line_count;
static gint thoth_print_lines_per_page;
static gint thoth_print_text_height;
static gint thoth_print_n_pages;
static gdouble thoth_print_page_width;
static gdouble thoth_print_page_height;

/* Settings saved on exit and loaded on startup. */
GtkWidget *thoth_editorpaned;
gint thoth_recentfilemax = 10;
gint thoth_tabsize = 8;
gint thoth_closebuttonaction = 0;
GList *thoth_recentfilelist = NULL;
gboolean thoth_sidepanelvisible;
gboolean thoth_wraplines = FALSE;
gboolean thoth_showlinenumbers = FALSE;
gboolean thoth_autoindent = FALSE;
gboolean thoth_keepabove = FALSE;
gboolean thoth_reopenfilesonstartup = FALSE;
gchar *thoth_textview_font = NULL;
GdkRGBA thoth_textview_fg_rgba;
GdkRGBA thoth_textview_bg_rgba;

static ThothEditorStruct *thoth_new_window_add_editor(gint category);
static gboolean thoth_save_file(ThothEditorStruct *editor, gchar *filename);
static void thoth_command_activate(GtkWidget *widget, gpointer data);

/* Set up the Thoth Inline Copy Button (ticb) custom class */
static void ticb_menu_item_finalize(GObject *object);
static GtkWidget *ticb_menu_item_get_widget_at_event(TicbMenuItem *menuitem, GdkEventButton *event);
static gboolean ticb_menu_item_button_press_event(GtkWidget *menuitem, GdkEventButton *event);
static gboolean ticb_menu_item_button_release_event(GtkWidget *menuitem, GdkEventButton *event);
static void ticb_copy_clicked_event(GtkButton *button, gpointer user_data);
static void ticb_menu_item_activate_event(GtkMenuItem *item, gpointer user_data);
static GtkWidget *ticb_label_new(const gchar *text);
static GtkWidget *ticb_menu_item_new(const gchar *title);

static void thoth_set_textview_font(GtkWidget *textview, const gchar *font_name);
static GtkTreeIter *thoth_treemodel_iter_find_id(GtkTreeModel *treemodel, gint id);
static void thoth_select_sidepanel_iter(GtkTreeModel *treemodel, GtkTreeIter *target_iter,
		gboolean show_editor);

/**
 * Private API
 */
static void ticb_menu_item_class_init (TicbMenuItemClass *item_class)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(item_class);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(item_class);

	gobject_class->finalize = ticb_menu_item_finalize;
	widget_class->button_press_event = ticb_menu_item_button_press_event;
	widget_class->button_release_event = ticb_menu_item_button_release_event;
}

static void ticb_menu_item_init(TicbMenuItem *item)
{
	TicbMenuItemPrivate *priv = ticb_menu_item_get_instance_private(item);

	priv->label = NULL;
	priv->button = NULL;
	priv->hbox = NULL;
	priv->button_box = NULL;

	priv->editor = NULL;

	return;
}

static void ticb_menu_item_finalize(GObject *object)
{
	TicbMenuItem *item = TICB_MENU_ITEM(object);
	TicbMenuItemPrivate *priv = ticb_menu_item_get_instance_private(item);

	g_object_unref(priv->label);
	g_object_unref(priv->button);
	g_object_unref(priv->hbox);
	g_object_unref(priv->button_box);

	(*G_OBJECT_CLASS (ticb_menu_item_parent_class)->finalize) (object);
}

static GtkWidget *ticb_menu_item_get_widget_at_event(TicbMenuItem *menuitem,
		GdkEventButton *event)
{
	GtkAllocation alloc;
	gint x, y;

	g_return_val_if_fail(IS_TICB_MENU_ITEM(menuitem), NULL);

	TicbMenuItemPrivate *priv = ticb_menu_item_get_instance_private(menuitem);

	gtk_widget_get_allocation(priv->button_box, &alloc);
	gtk_widget_translate_coordinates(GTK_WIDGET(menuitem), priv->button_box,
			event->x, event->y, &x, &y);

	if ((x > 0) && (x < alloc.width) && (y > 0) && (y < alloc.height)) {
		gtk_widget_get_allocation(priv->button, &alloc);
		gtk_widget_translate_coordinates(GTK_WIDGET(menuitem),
				priv->button, event->x, event->y, &x, &y);

		if ((x > 0) && (x < alloc.width) && (y > 0) && (y < alloc.height))
			return GTK_WIDGET(priv->button);
	}

	return GTK_WIDGET(menuitem);
}

static gboolean ticb_menu_item_button_press_event(GtkWidget *menuitem,
		GdkEventButton *event)
{
	g_return_val_if_fail(IS_TICB_MENU_ITEM(menuitem), FALSE);

	GtkWidget *widget = ticb_menu_item_get_widget_at_event(
			TICB_MENU_ITEM(menuitem), event);

	if (widget == NULL)
		return FALSE;

	if (widget == menuitem)
		return FALSE;

	gtk_widget_event(widget, (GdkEvent *)event);

	return TRUE;
}

static gboolean ticb_menu_item_button_release_event(GtkWidget *menuitem,
		GdkEventButton *event)
{
	g_return_val_if_fail(IS_TICB_MENU_ITEM(menuitem), FALSE);

	GtkWidget *widget = ticb_menu_item_get_widget_at_event(
			TICB_MENU_ITEM(menuitem), event);

	if (widget == NULL)
		return FALSE;

	if (widget == menuitem)
		return FALSE;

	gtk_widget_event(widget, (GdkEvent *)event);

	return TRUE;
}

static void ticb_copy_clicked_event(GtkButton *button, gpointer user_data)
{
	TicbMenuItem *item = user_data;
	TicbMenuItemPrivate *priv = ticb_menu_item_get_instance_private(item);
	ThothEditorStruct *editor = priv->editor;

	GtkClipboard *clipboard = gtk_widget_get_clipboard(
			GTK_WIDGET(editor->textview), GDK_SELECTION_CLIPBOARD);
	GtkTextIter startiter, enditer;
	gtk_text_buffer_get_start_iter(editor->textbuffer, &startiter);
	gtk_text_buffer_get_end_iter(editor->textbuffer, &enditer);
	gchar *text = gtk_text_buffer_get_text(editor->textbuffer,
			&startiter, &enditer, FALSE);
	gtk_clipboard_set_text(clipboard, text, -1);
	g_free(text);

	gtk_menu_shell_deactivate(GTK_MENU_SHELL(thoth_statusmenu));
}

static void ticb_menu_item_activate_event (GtkMenuItem *menuitem, gpointer user_data)
{
	TicbMenuItem *item = user_data;
	TicbMenuItemPrivate *priv = ticb_menu_item_get_instance_private(item);
	ThothEditorStruct *editor = priv->editor;

	/* Raise menu item */
	GtkTreeIter *treeiter = thoth_treemodel_iter_find_id(
			thoth_treemodel, editor->id);
	thoth_select_sidepanel_iter(thoth_treemodel, treeiter, TRUE);

	if (!gtk_widget_get_visible(thoth_window)) {
		gtk_window_resize(GTK_WINDOW(thoth_window), thoth_width, thoth_height);
		gtk_window_move(GTK_WINDOW(thoth_window), thoth_left, thoth_top);
	}
	gtk_window_present(GTK_WINDOW(thoth_window));
}

static GtkWidget *ticb_label_new(const gchar *text)
{
	GtkWidget *label = gtk_label_new(text);
	gtk_label_set_max_width_chars(GTK_LABEL(label), 30);
	gtk_label_set_ellipsize(GTK_LABEL(label), PANGO_ELLIPSIZE_END);

	gtk_label_set_xalign(GTK_LABEL(label), 0.0);
	gtk_widget_set_halign(label, GTK_ALIGN_START);

	return label;
}

static GtkWidget *ticb_menu_item_new(const gchar *title)
{
	TicbMenuItem *menu_item = TICB_MENU_ITEM(g_object_new(TYPE_TICB_MENU_ITEM, NULL));
	TicbMenuItemPrivate *priv = ticb_menu_item_get_instance_private(menu_item);

	g_return_val_if_fail(IS_TICB_MENU_ITEM(menu_item), NULL);

	priv->label = ticb_label_new(title);
	priv->button = gtk_button_new_from_icon_name("edit-copy", GTK_ICON_SIZE_MENU);
	priv->hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	priv->button_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

	gtk_widget_set_tooltip_text(priv->button, "Copy to clipboard");
	//gtk_button_set_relief(GTK_BUTTON(priv->button), GTK_RELIEF_NONE);
	GtkStyleContext *context = gtk_widget_get_style_context(priv->button);
	gtk_style_context_add_class(context, "menuitembutton");

	gtk_box_pack_start(GTK_BOX(priv->button_box), priv->button, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(priv->hbox), priv->label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(priv->hbox), priv->button_box, FALSE, FALSE, 0);

	g_signal_connect(priv->button, "clicked", G_CALLBACK(ticb_copy_clicked_event), menu_item);
	g_signal_connect(menu_item, "activate", G_CALLBACK(ticb_menu_item_activate_event), menu_item);

	g_object_ref(priv->label);
	g_object_ref(priv->button);
	g_object_ref(priv->hbox);
	g_object_ref(priv->button_box);

	gtk_widget_show_all(priv->hbox);

	gtk_container_add(GTK_CONTAINER(menu_item), priv->hbox);

	GtkWidget *widget = GTK_WIDGET(menu_item);

	gtk_widget_show_all(widget);

	return widget;
}

/**
 * Utility functions.
 */
static gchar *thoth_filename_from_uri(gchar *uri)
{
	GError *err = NULL;
	gchar *filename = g_filename_from_uri(uri, NULL, &err);
	if (err) printf("%s:%4d.%4d g_filename_from_uri() err->message=%s;\n",
			__FILE__, __LINE__, dbg++, err->message);
	return filename;
}

static gchar *thoth_uri_from_filename(gchar *filename)
{
	GError *err = NULL;
	gchar *uri = g_filename_to_uri(filename, NULL, &err);
	if (err) printf("%s:%4d.%4d g_filename_to_uri() err->message=%s;\n",
			__FILE__, __LINE__, dbg++, err->message);
	return uri;
}

static GtkWidget *thoth_create_left_label(gchar *text)
{
	GtkWidget *label = gtk_label_new(text);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_show(label);

	return label;
}

static gchar *thoth_filepermissions_inttooctstr(int pint)
{
	gchar *pstr = g_strdup("000");
	char digits[] = "01234567";

	int owner = (pint & 0b111000000) >> 6;
	int group = (pint & 0b111000) >> 3;
	int world = (pint & 0b111);

	pstr[0] = digits[owner];
	pstr[1] = digits[group];
	pstr[2] = digits[world];

	return pstr;
}

/**
 * Converts ls style permissions to a 9-bit integer.
 * - listingentry: 9 or 10-character string from an ls entry
 *   eg. '-rwxr-x---'
 * Returns a 9-bit integer of the above string.
 */
static gint thoth_filepermissions_listingtoint(gchar *listingentry)
{
	gint permissions = 0;
	GMatchInfo *matchinfo;
	GRegex *listingregex = g_regex_new("[d-]?([r-])([w-])([x-])([r-])([w-])([x-])([r-])([w-])([x-])",
			G_REGEX_CASELESS, 0, NULL);
	g_regex_match(listingregex, listingentry, 0, &matchinfo);
	if (g_match_info_matches(matchinfo)) {
		gchar *owner_read = g_match_info_fetch(matchinfo, 1);
		gchar *owner_write = g_match_info_fetch(matchinfo, 2);
		gchar *owner_execute = g_match_info_fetch(matchinfo, 3);
		gchar *group_read = g_match_info_fetch(matchinfo, 4);
		gchar *group_write = g_match_info_fetch(matchinfo, 5);
		gchar *group_execute = g_match_info_fetch(matchinfo, 6);
		gchar *world_read = g_match_info_fetch(matchinfo, 7);
		gchar *world_write = g_match_info_fetch(matchinfo, 8);
		gchar *world_execute = g_match_info_fetch(matchinfo, 9);

		if (owner_read[0] == 'r') permissions |= THOTH_OWNERR;
		if (owner_write[0] == 'w') permissions |= THOTH_OWNERW;
		if (owner_execute[0] == 'x') permissions |= THOTH_OWNERX;

		if (group_read[0] == 'r') permissions |= THOTH_GROUPR;
		if (group_write[0] == 'w') permissions |= THOTH_GROUPW;
		if (group_execute[0] == 'x') permissions |= THOTH_GROUPX;

		if (world_read[0] == 'r') permissions |= THOTH_WORLDR;
		if (world_write[0] == 'w') permissions |= THOTH_WORLDW;
		if (world_execute[0] == 'x') permissions |= THOTH_WORLDX;

		g_free(owner_read);
		g_free(owner_write);
		g_free(owner_execute);
		g_free(group_read);
		g_free(group_write);
		g_free(group_execute);
		g_free(world_read);
		g_free(world_write);
		g_free(world_execute);
	}
	g_match_info_free(matchinfo);
	g_regex_unref(listingregex);

	return permissions;
}

/**
 * Get the parent directory of a file or directory.
 * - uri: a URI or standard local path. Doesn't need trailing slash.
 *   eg. ftp://server.com/files/ or /home/user/folder/
 * Returns the parent folder of the URI, if possible. If a root URI was given
 * (eg. ftp://server.com/) the URI will be returned with a trailing slash.
 */
static gchar *thoth_get_uri_parent(gchar *uri)
{
	gint ct;
	gchar *parentstr = NULL;
	gchar *prevtoken = NULL;
	gchar **tokens = g_strsplit(uri, "/", 64);
	for (ct = 0; tokens[ct] != NULL; ct++) {
		g_strstrip(tokens[ct]);
		if (!strlen(tokens[ct])) {
			if (prevtoken && (ct == 1)) {
				/* Assume the first blank follows the URI protocol
				 * (eg. ftp: or http:) and a second forward slash */
				gchar *nsa = g_strconcat(prevtoken, "/", NULL);
				g_free(prevtoken);
				prevtoken = g_strdup(nsa);
				g_free(nsa);
			}
			continue; /* Skip blanks */
		}

		if (prevtoken && !parentstr) {
			parentstr = g_strconcat(prevtoken, "/", NULL);
		} else if (prevtoken) {
			gchar *nsa = g_strconcat(parentstr, prevtoken, "/", NULL);
			g_free(parentstr);
			parentstr = g_strdup(nsa);
			g_free(nsa);
		}
		g_free(prevtoken);
		prevtoken = g_strdup(tokens[ct]);
	}

	/* Special case: Handle URIs with root only (eg. ftp://server.com/) */
	if ((ct == 3) || ((ct == 4) && !strlen(tokens[3]))) {
		gchar *nsa = g_strconcat(parentstr, prevtoken, "/", NULL);
		g_free(parentstr);
		parentstr = g_strdup(nsa);
		g_free(nsa);
	}
	g_free(prevtoken);
	g_strfreev(tokens);

	return parentstr;
}

/**
 * Take a web-based URI and return a Base64 version under /pages/ subfolder.
 */
static gchar *thoth_web_address_to_local_page_file(gchar *address)
{
	gchar *pageid = g_base64_encode(address, g_utf8_strlen(address, -1));
	gchar *filepath = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"pages", pageid, NULL);
	g_free(pageid);

	return filepath;
}

/**
 * Find an open editor window by its editor ID.
 */
static GList* thoth_editorlist_find_id(gint editorid)
{
	GList *list = thoth_editorlist;
	while (list != NULL) {
		if (((ThothEditorStruct *)list->data)->id == editorid) {
			return list;
		}
		list = list->next;
	}
	return NULL;
}

/**
 * The following 2 functions find a treemodel entry by its editor ID
 * and return its iter, otherwise return FALSE if nothing found.
 * - thoth_treemodel_iter_find_id(treemodel, id) is the function to call.
 */
static GtkTreeIter *thoth_treemodel_iter_find_id_iter = NULL;
static GtkTreeIter thoth_treemodel_iter_find_id_match_iter;

static gboolean thoth_treestore_iter_find_id_func(GtkTreeModel *treemodel,
		GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	gboolean isfound = FALSE;
	gint id;
	gint target_id = GPOINTER_TO_INT(data);

	gtk_tree_model_get(treemodel, iter, SPL_COL_ID, &id, -1);
	if (id == target_id) {
		isfound = TRUE;
		thoth_treemodel_iter_find_id_match_iter = *iter;
		thoth_treemodel_iter_find_id_iter = &thoth_treemodel_iter_find_id_match_iter;
	}
	return isfound;
}

static GtkTreeIter *thoth_treemodel_iter_find_id(GtkTreeModel *treemodel, gint id)
{
	thoth_treemodel_iter_find_id_iter = NULL;
	gtk_tree_model_foreach(treemodel, thoth_treestore_iter_find_id_func,
			GINT_TO_POINTER(id));

	return thoth_treemodel_iter_find_id_iter; /* NULL if not found. */
}

/**
 * The following 2 functions find a treestore entry by its URI
 * and return its iter, otherwise return FALSE if nothing found.
 * - thoth_treestore_iter_find_uri(treemodel, uri) is the function to call.
 */
static GtkTreeIter *thoth_treestore_iter_find_uri_iter = NULL;
static GtkTreeIter thoth_treestore_iter_find_uri_match_iter;

static gboolean thoth_treestore_iter_find_uri_func(GtkTreeModel *treemodel,
		GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	gboolean isfound = FALSE;
	gchar *uri;
	const gchar *target_uri = data;

	gtk_tree_model_get(treemodel, iter, SPL_COL_URI, &uri, -1);
	if (g_strcmp0(uri, target_uri) == 0) {
		isfound = TRUE;
		thoth_treestore_iter_find_uri_match_iter = *iter;
		thoth_treestore_iter_find_uri_iter = &thoth_treestore_iter_find_uri_match_iter;
	}
	return isfound;
}

static GtkTreeIter *thoth_treestore_iter_find_uri(GtkTreeModel *treemodel, gchar *uri)
{
	thoth_treestore_iter_find_uri_iter = NULL;
	gtk_tree_model_foreach(treemodel, thoth_treestore_iter_find_uri_func, uri);

	return thoth_treestore_iter_find_uri_iter; /* NULL if not found. */
}

/**
 * Obtain a short extract from the first few characters of a text buffer.
 */
static gchar *thoth_get_snippet(gint length, gboolean strip_linebreaks)
{
	GtkTextIter startiter, enditer;
	gtk_text_buffer_get_start_iter(thoth_editor->textbuffer, &startiter);
	gtk_text_buffer_get_iter_at_offset(thoth_editor->textbuffer, &enditer, length);
	gchar *snippet = gtk_text_buffer_get_text(thoth_editor->textbuffer,
			&startiter, &enditer, FALSE);

	/* Leave early if not stripping line breaks... */
	if (!strip_linebreaks) {
		return snippet;
	}

	GRegex *remove_linebreaks_regex = g_regex_new("\\s+", G_REGEX_OPTIMIZE, 0, NULL);
	gchar *snippet_ret = g_regex_replace_literal(remove_linebreaks_regex, snippet,
			-1, 0, " ", 0, NULL);
	g_regex_unref(remove_linebreaks_regex);
	g_free(snippet);

	return snippet_ret;
}

/**
 * Update window title. Uses (gchar*)thoth_editor->title for base string.
 */
static void thoth_update_title()
{
	gchar *label, *title, *tooltip;
	gint category = 0;
	gint editorid = 0;
	if (thoth_editor) {
		editorid = thoth_editor->id;
		category = thoth_editor->category;
	}

	if ((category == THOTH_NOTE) || (category == THOTH_PAGE)) {
		g_free(thoth_editor->title);
		thoth_editor->title = thoth_get_snippet(80, TRUE);
	}

	if ((thoth_editor->title == NULL) || (g_utf8_strlen(thoth_editor->title, -1) == 0)) {
		title = g_strdup("(Untitled)");
	} else {
		title = g_strdup(thoth_editor->title);
	}

	if (category == THOTH_NOTE) {
		label = g_strconcat("(Note) ", title, NULL);
		tooltip = g_markup_escape_text(title, -1);
	} else if (category == THOTH_PAGE) {
		label = g_strconcat("(", thoth_editor->uri, ") ", title, NULL);
		tooltip = g_markup_escape_text(thoth_editor->uri, -1);
	} else {
		label = g_strdup(title);
		tooltip = g_markup_escape_text(thoth_editor->uri, -1);
	}

	gboolean show_modified = FALSE;
	if ((thoth_editor->modified) && (category != THOTH_NOTE)) {
		show_modified = TRUE;
		gchar *l = g_strconcat("(Modified) ", label, NULL);
		g_free(label);
		label = l;
	}

	gtk_label_set_text(GTK_LABEL(thoth_label), label);
	gtk_window_set_title(GTK_WINDOW(thoth_window), label);

	if (category == THOTH_NOTE) {
		gtk_widget_set_tooltip_text(GTK_WIDGET(thoth_label), label);
	} else if (thoth_editor) {
		gtk_widget_set_tooltip_text(GTK_WIDGET(thoth_label), thoth_editor->uri);
	}

	GtkTreeIter *treeiter = thoth_treemodel_iter_find_id(
			thoth_treemodel, thoth_editor->id);
	if (treeiter) {
		GdkRGBA rgba;
		if (show_modified) {
			gdk_rgba_parse(&rgba, "#c00");
		} else {
			gdk_rgba_parse(&rgba, "#000");
		}
		gtk_tree_store_set(GTK_TREE_STORE(thoth_treemodel), treeiter,
				SPL_COL_TOOLTIP, tooltip, SPL_COL_COLOUR, &rgba, -1);

		/* Set sidepanel item title */
		if ((category == THOTH_NOTE) || (category == THOTH_PAGE)) {
			gtk_tree_store_set(GTK_TREE_STORE(thoth_treemodel), treeiter,
					SPL_COL_TITLE, title, -1);
		}
	}
	g_free(label);
	g_free(title);
	g_free(tooltip);
}

static void thoth_update_undo_buttons()
{
	if (!thoth_editor)
		return;

	gboolean undo_enabled = FALSE;
	gboolean redo_enabled = FALSE;
	gint undo_len = g_list_length(thoth_editor->undolist);

	undo_enabled = (thoth_editor->undoindex > 0);

	redo_enabled = (thoth_editor->undoindex < undo_len);

	gtk_widget_set_sensitive(GTK_WIDGET(thoth_undotoolitem), undo_enabled);
	gtk_widget_set_sensitive(GTK_WIDGET(thoth_redotoolitem), redo_enabled);
}

/**
 * Convenience function to set both undo chain variables.
 */
static void thoth_set_undochain(gboolean chain)
{
	thoth_undo_chain = chain;
	thoth_undo_chain_started = FALSE;
}

static void thoth_set_current_uri(gboolean update_now)
{
	if (update_now) {
		g_free(thoth_editor->title);
		if ((thoth_editor->category == THOTH_NOTE)
				|| (thoth_editor->category == THOTH_PAGE)) {
			gchar *title = thoth_get_snippet(255, TRUE);
			thoth_editor->title = (g_utf8_strlen(title, -1)) ? g_strdup(title) : NULL;
			g_free(title);
		} else {
			thoth_editor->title = g_path_get_basename(thoth_editor->filepath);
		}
	}
	thoth_update_title();
	thoth_update_undo_buttons();
}

void thoth_scroll_to_cursor(GtkTextView *textview, gdouble margin)
{
	GtkTextIter iter;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));

	gtk_text_buffer_get_iter_at_mark(buffer, &iter,
			gtk_text_buffer_get_insert(buffer));
	gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(textview),
			&iter, margin, FALSE, 0.5, 0.5);
}

static void thoth_callback_modified_changed(GtkTextBuffer *textbuffer, gpointer data)
{
	thoth_editor->modified = gtk_text_buffer_get_modified(textbuffer);
	gint save_enabled = thoth_editor->modified;
	if (thoth_editor->modified) {
		if (thoth_editor->category == THOTH_NOTE) {
			save_enabled = FALSE;
		} else {
			gtk_widget_set_sensitive(GTK_WIDGET(thoth_redotoolitem), FALSE);
		}
	}
	gtk_widget_set_sensitive(GTK_WIDGET(thoth_savetoolitem), save_enabled);

	thoth_update_title();
	thoth_update_undo_buttons();
}

/**
 * Indent the new line, depending on the leading spaces on the current line.
 * TODO: Do not unindent blank lines (e.g. pressing enter twice)
 */
static void thoth_autoindent_now(GtkWidget *widget)
{
	GtkTextIter itercur, startiter, enditer;
	gunichar ch;
	gchar *ind, *str;

	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
	//g_signal_emit_by_name(G_OBJECT(textbuffer), "begin-user-action");
	gtk_text_buffer_delete_selection(textbuffer, TRUE, TRUE);
	//g_signal_emit_by_name(G_OBJECT(textbuffer), "end-user-action");

	/* Before the new line is added, find any leading spaces first. */
	GtkTextMark *mark = gtk_text_buffer_get_insert(textbuffer);
	gtk_text_buffer_get_iter_at_mark(textbuffer, &itercur, mark);
	gtk_text_buffer_get_iter_at_line(textbuffer, &startiter,
			gtk_text_iter_get_line(&itercur));
	enditer = startiter;
	ch = gtk_text_iter_get_char(&enditer);
	while (g_unichar_isspace(ch) && (ch != '\n')) {
		if (!gtk_text_iter_forward_char(&enditer)) {
			break;
		}
		ch = gtk_text_iter_get_char(&enditer);
	}

	/* No spaces? Just enter new line, otherwise add indentation. */
	if (gtk_text_iter_equal(&startiter, &enditer)) {
		str = g_strdup("\n");
	} else {
		str = g_strconcat("\n", gtk_text_iter_get_slice(&startiter, &enditer), NULL);
	}

	g_signal_emit_by_name(G_OBJECT(textbuffer), "begin-user-action");
	gtk_text_buffer_insert(textbuffer, &itercur, str, -1);
	g_signal_emit_by_name(G_OBJECT(textbuffer), "end-user-action");

	g_free(str);
}

/**
 * Following functions indent/unindent multiple lines if they are selected,
 * otherwise indent/unindent the current line only.
 */
static void thoth_indent_line(GtkTextIter *iter)
{
	gtk_text_buffer_insert(thoth_editor->textbuffer, iter, "\t", -1);
}

static void thoth_unindent_line(GtkTextIter *iter)
{
	GtkTextIter startiter = *iter;
	GtkTextIter enditer = *iter;

	gint columnsize = thoth_tabsize;
	gunichar c;
	while (columnsize > 0) {
		c = gtk_text_iter_get_char(&enditer);

		if (c == '\t') {
			columnsize -= thoth_tabsize;
		} else if (c == ' ') {
			columnsize--;
		} else {
			break; /* No valid chars to remove for unindent */
		}

		gtk_text_iter_forward_char(&enditer);
	}

	if (!gtk_text_iter_equal(&startiter, &enditer)) {
		gtk_text_buffer_delete(thoth_editor->textbuffer, &startiter, &enditer);
	}
}

/**
 * thoth_indent accepts a single variable:
 * - direction: 1 to indent, -1 to unindent.
 * Returns TRUE on multiline indent, otherwise it returns FALSE.
 */
static gboolean thoth_indent(gint direction)
{
	GtkTextIter startiter, enditer;
	if (!gtk_text_buffer_get_selection_bounds(thoth_editor->textbuffer, &startiter, &enditer))
		/* Return if no selection has been made */
		return FALSE;

	gint startline = gtk_text_iter_get_line(&startiter);
	gint endline = gtk_text_iter_get_line(&enditer);
	if ((startline == endline)
			&& (!gtk_text_iter_starts_line(&startiter) || !gtk_text_iter_ends_line(&enditer))) {
		/* Return if the selection doesn't cover 2 or more lines,
		 * or isn't a complete single line. Partially selected
		 * line text gets replaced by tab character(s) instead. */
		return FALSE;
	}

	gtk_text_buffer_begin_user_action(thoth_editor->textbuffer);
	thoth_set_undochain(TRUE);
	gint i;
	for (i = startline; i <= endline; i++) {
		gtk_text_buffer_get_iter_at_line(thoth_editor->textbuffer,
				&startiter, i);

		if (gtk_text_iter_ends_line(&startiter)) {
			continue; /* Don't indent/unindent empty lines */
		}

		if (direction == 1) {
			thoth_indent_line(&startiter);
		} else if (direction == -1) {
			thoth_unindent_line(&startiter);
		}
	}
	gtk_text_buffer_end_user_action(thoth_editor->textbuffer);
	thoth_set_undochain(FALSE);

	/* If we get to this point, a multiline indent has occurred */
	return TRUE;
}

/**
 * WARNING: Returned string must be freed with g_free();
 */
gchar *thoth_regex_replace(const gchar *regexp, GRegexCompileFlags grcflags,
		const gchar *text, const gchar *replacement)
{
	grcflags = grcflags | G_REGEX_OPTIMIZE;
	GRegex *reg = g_regex_new(regexp, grcflags, 0, NULL);
	g_assert(reg);

	gchar *buf = g_regex_replace(reg, text, -1, 0, replacement, 0, NULL);
	g_regex_unref(reg);

	return buf;
}

/**
 * Apply formatting operations to selected text such as uppercase, lowercase,
 * removing line breaks, or trimming excessive spaces.
 */
static void thoth_format_selection(gint formattype)
{
	GtkTextIter startiter, enditer;
	if (!gtk_text_buffer_get_selection_bounds(thoth_editor->textbuffer, &startiter, &enditer))
		return; /* No selection has been made */

	gchar *text, *formatted;
	text = gtk_text_buffer_get_slice(thoth_editor->textbuffer, &startiter, &enditer, FALSE);
	if (G_LIKELY(text == NULL))
		return; /* No valid text to work with */

	switch (formattype) {
	case EDIT_UPPERCASE:
		formatted = g_utf8_strup(text, -1);
		break;

	case EDIT_LOWERCASE:
		formatted = g_utf8_strdown(text, -1);
		break;

	case EDIT_REMOVE_TRAILING_SPACES:
		/* Remove Tabs (11dec/0Bhex) and spaces (32dec/20hex) */
		formatted = thoth_regex_replace("[\\x20\\x0B]+$",
				G_REGEX_MULTILINE, text, "");
		break;

	default:
		printf("%s:%4d.%4d Invalid format type: %d.\n", __FILE__, __LINE__, dbg++, formattype);
	}

	if (G_LIKELY(formatted && g_strcmp0(text, formatted))) {
		gint offset = gtk_text_iter_get_offset(&startiter);
		gtk_text_buffer_begin_user_action(thoth_editor->textbuffer);
		thoth_set_undochain(TRUE);
		gtk_text_buffer_delete(thoth_editor->textbuffer, &startiter, &enditer);
		gtk_text_buffer_insert(thoth_editor->textbuffer, &enditer, formatted, -1);
		gtk_text_buffer_get_iter_at_offset(thoth_editor->textbuffer, &startiter, offset);
		gtk_text_buffer_select_range(thoth_editor->textbuffer, &startiter, &enditer);
		gtk_text_buffer_end_user_action(thoth_editor->textbuffer);
		thoth_set_undochain(FALSE);
	}

	g_free(text);
	g_free(formatted);
}

/**
 * Respond to user input on textview widget signals.
 */
static gboolean thoth_textview_key_press_event(GtkWidget *widget, GdkEventKey *evkey,
		gpointer data)
{
	switch(evkey->keyval) {
	case GDK_KEY_Return:
		if (thoth_autoindent) {
			thoth_autoindent_now(widget);
			return TRUE;
		}
		break;

	case GDK_KEY_Tab:
		/* Only indent if control not held; it's for the switcher */
		if (!(evkey->state & GDK_CONTROL_MASK)) {
			return thoth_indent(1);
		}
		break;
	case GDK_KEY_ISO_Left_Tab:
		/* Only unindent if control not held; it's for the switcher */
		if (!(evkey->state & GDK_CONTROL_MASK)) {
			return thoth_indent(-1);
		}
		break;
	}
	thoth_last_keyval = evkey->keyval;
	thoth_update_title();
	return FALSE;
}

static gboolean thoth_change_font_size(gint amount)
{
	PangoFontDescription *font_description;
	font_description = pango_font_description_from_string(thoth_textview_font);
	gint fontsize = pango_font_description_get_size(font_description) / PANGO_SCALE;
	if (fontsize < MIN_FONT_SIZE) {
		fontsize = MIN_FONT_SIZE;
	} else if (fontsize > MAX_FONT_SIZE) {
		fontsize = MAX_FONT_SIZE;
	}
	pango_font_description_set_size(font_description,
			(fontsize + amount) * PANGO_SCALE);

	g_free(thoth_textview_font);
	thoth_textview_font = pango_font_description_to_string(font_description);
	pango_font_description_free(font_description);

	thoth_set_textview_font(thoth_editor->textview, thoth_textview_font);
}

static gboolean thoth_textview_scroll_event(GtkWidget *widget, GdkEventScroll *evs,
		gpointer user_data)
{
	guint direction = evs->direction;
	guint state = evs->state;
	gint delta_x = evs->delta_x;
	gint delta_y = evs->delta_y;

	if (((direction == GDK_SCROLL_DOWN) || (delta_y == -1))
			&& (state & GDK_CONTROL_MASK)) {
		thoth_change_font_size(1);
		return TRUE;
	} else if (((direction == GDK_SCROLL_UP) || (delta_y == 1))
			&& (state & GDK_CONTROL_MASK)) {
		thoth_change_font_size(-1);
		return TRUE;
	}

	return FALSE;
}

static gint thoth_line_numbers_draw(GtkWidget *widget, cairo_t *cr)
{
	GdkWindow *gdkleft;
	PangoLayout *layout;
	gint layoutwidth;
	GtkTextIter iter, end_iter;
	gint currline, linecount;
	GtkStyleContext *context;
	GdkRGBA rgba_normal, rgba_faded;

	gdkleft = gtk_text_view_get_window(GTK_TEXT_VIEW(thoth_editor->textview),
			GTK_TEXT_WINDOW_LEFT);
	if (!gdkleft || (!gtk_cairo_should_draw_window(cr, gdkleft)))
		return FALSE;

	/* Get colours used for line numbers in side window */
	rgba_faded = (GdkRGBA) {0.5, 0.5, 0.5, 1}; /* Temporary, need to detect colour schemes */
	context = gtk_widget_get_style_context(GTK_WIDGET(thoth_editor->textview));
	gtk_style_context_get_color(context,
			gtk_widget_get_state_flags(thoth_editor->textview), &rgba_normal);

	layout = gtk_widget_create_pango_layout(GTK_WIDGET(thoth_editor->textview), "x");

	/* Get the current maximum number string width in pixels */
	gtk_text_buffer_get_end_iter(thoth_editor->textbuffer, &iter);
	linecount = gtk_text_iter_get_line(&iter) + 1;
	gchar *maxstring = g_strdup_printf("<b>%d</b>", linecount);
	pango_layout_set_markup(layout, maxstring, -1);
	g_free(maxstring);
	pango_layout_get_pixel_size(layout, &layoutwidth, NULL);
	gtk_text_view_set_border_window_size(
			GTK_TEXT_VIEW(thoth_editor->textview),
			GTK_TEXT_WINDOW_LEFT, layoutwidth+8);
	pango_layout_set_width(layout, layoutwidth+8);
	pango_layout_set_alignment(layout, PANGO_ALIGN_RIGHT);

	/* Get line number the cursor is on */
	gtk_text_buffer_get_iter_at_mark(thoth_editor->textbuffer, &iter,
			gtk_text_buffer_get_insert(thoth_editor->textbuffer));
	currline = gtk_text_iter_get_line(&iter);

	/* Get upper and lower visible text buffer iters */
	GdkRectangle rect;
	gtk_text_view_get_visible_rect(GTK_TEXT_VIEW(thoth_editor->textview), &rect);
	gtk_text_view_get_line_at_y(GTK_TEXT_VIEW(thoth_editor->textview),
			&iter, rect.y, NULL);
	gtk_text_view_get_line_at_y(GTK_TEXT_VIEW(thoth_editor->textview),
			&end_iter, rect.y + rect.height, NULL);

	gint linenumber = gtk_text_iter_get_line(&iter);
	gint lastlinenumber = gtk_text_iter_get_line(&end_iter);

	while (linenumber <= lastlinenumber) {
		gint y, ypos, ht;

		gtk_text_iter_set_line(&iter, linenumber);
		gtk_text_view_get_line_yrange(GTK_TEXT_VIEW(thoth_editor->textview),
				&iter, &y, &ht);
		gtk_text_view_buffer_to_window_coords(
				GTK_TEXT_VIEW(thoth_editor->textview),
				GTK_TEXT_WINDOW_LEFT, 0, y, NULL, &ypos);

		/* Change text, depending if number is line cursor is on */
		gchar *string;
		if (linenumber == currline) {
			gdk_cairo_set_source_rgba(cr, &rgba_normal);
			string = g_strdup_printf("<b>%d</b>", linenumber+1);
		} else {
			gdk_cairo_set_source_rgba(cr, &rgba_faded);
			string = g_strdup_printf("%d", linenumber+1);
		}
		pango_layout_set_markup(layout, string, -1);
		g_free(string);
		cairo_move_to(cr, layoutwidth + 4, ypos);
		pango_cairo_show_layout(cr, layout);

		linenumber++;
	}

	g_object_unref(G_OBJECT(layout));

	return TRUE;
}

static void thoth_set_textview_line_numbers(GtkWidget *textview,
		gboolean showlinenumbers)
{
	if (showlinenumbers) {
		gtk_text_view_set_border_window_size(GTK_TEXT_VIEW(textview),
				GTK_TEXT_WINDOW_LEFT, thoth_linenumberswidth);
		g_signal_connect_after(G_OBJECT(textview), "draw",
				G_CALLBACK(thoth_line_numbers_draw), NULL);
	} else {
		gtk_text_view_set_border_window_size(GTK_TEXT_VIEW(textview),
				GTK_TEXT_WINDOW_LEFT, 0);
		g_signal_handlers_disconnect_by_func(G_OBJECT(textview),
				G_CALLBACK(thoth_line_numbers_draw), NULL);
	}
}

static void thoth_set_textview_wrap_mode(GtkWidget *textview, gboolean wraplines)
{
	GtkWrapMode wrapmode;
	if (wraplines) {
		wrapmode = GTK_WRAP_WORD;
	} else {
		wrapmode = GTK_WRAP_NONE;
	}
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textview), wrapmode);
}

static void thoth_set_textview_font(GtkWidget *textview, const gchar *font_name)
{
	PangoFontDescription *font_description;

	font_description = pango_font_description_from_string(font_name);
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
	gtk_widget_override_font(textview, font_description); /* Deprecated */
G_GNUC_END_IGNORE_DEPRECATIONS
	pango_font_description_free(font_description);
}

static void thoth_set_textview_foreground_colour(GdkRGBA *rgba)
{
	/* COMMENTED: Changing this to CSS styling because this (deprecated)
	 * method causes the selection to go invisible.
	gtk_widget_override_cursor(thoth_editor->textview, rgba, rgba);
	gtk_widget_override_color(thoth_editor->textview, GTK_STATE_FLAG_NORMAL, rgba);
	// */
}

static void thoth_set_textview_background_colour(GdkRGBA *rgba)
{
	/* COMMENTED: Changing this to CSS styling because this (deprecated)
	 * method causes the selection to go invisible.
	gtk_widget_override_background_color(thoth_editor->textview, GTK_STATE_FLAG_NORMAL, rgba);
	// */
}

static void thoth_set_textview_tab_size(GtkWidget *textview, gint tabsize)
{
	PangoContext *context = gtk_widget_get_pango_context(textview);
	PangoFontMetrics *metrics = pango_context_get_metrics(context, NULL, NULL);
	gint width = pango_font_metrics_get_approximate_char_width(metrics);
	PangoTabArray *tabarray = pango_tab_array_new_with_positions(1, FALSE, PANGO_TAB_LEFT, width*tabsize);
	gtk_text_view_set_tabs(GTK_TEXT_VIEW(textview), tabarray);
	pango_tab_array_free(tabarray);
	pango_font_metrics_unref(metrics);
}

/**
 * Callbacks declarations.
 */
static void thoth_callback_open_recent(GtkWidget *widget, gpointer data);

/**
 * Following 3 func's add undo data from inserting/deleting text.
 */
static void thoth_add_undo_data(gint command, gint start, gint end, gchar *str)
{
	/* Crop any undo data that's in the redo area... */
	/* NOTE: GQueue is apparently faster at getting the list length. */
	while (g_list_length(thoth_editor->undolist) > thoth_editor->undoindex) {
		GList *last = g_list_last(thoth_editor->undolist);
		g_free(((ThothUndoStruct *)last->data)->str);
		g_free(last->data);
		thoth_editor->undolist = g_list_delete_link(thoth_editor->undolist, last);
	}
	gtk_widget_set_sensitive(GTK_WIDGET(thoth_redotoolitem), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(thoth_undotoolitem), TRUE);

	ThothUndoStruct *lastundo = NULL;
	if (g_list_length(thoth_editor->undolist)) {
		GList *lastitem = g_list_last(thoth_editor->undolist);
		lastundo = (ThothUndoStruct *)lastitem->data;
	}

	/* Check if can append to existing undo data */
	/* (Yes, the code for consecutive UNDO_INS and UNDO_BS/UNDO_DEL are
	 * identical, but they are kept apart to do other things later on) */
	if (lastundo) {
		/* Append consecutive inserts. "Pastes" are not appended here
		* and are given a separate undo action */
		if ((command == UNDO_INS) && (lastundo->command == command)
				&& (lastundo->end == start) && !thoth_new_undo_action) {
			gchar *newundostr = g_strconcat(lastundo->str, str, NULL);
			g_free(lastundo->str);
			lastundo->str = g_strdup(newundostr);
			g_free(newundostr);
			lastundo->end += (end - start);
			return;
		/* Append consecutive forward deletes */
		} else if (((lastundo->command == UNDO_BS) || (lastundo->command == UNDO_DEL))
				&& ((command == UNDO_BS) || (command == UNDO_DEL))
				&& (lastundo->start == start)) {
			gchar *newundostr = g_strconcat(lastundo->str, str, NULL);
			g_free(lastundo->str);
			lastundo->str = g_strdup(newundostr);
			g_free(newundostr);
			lastundo->end += (end - start);
			return;
		/* Prepend consecutive backspaces */
		} else if (((lastundo->command == UNDO_BS) || (lastundo->command == UNDO_DEL))
				&& ((command == UNDO_BS) || (command == UNDO_DEL))
				&& (lastundo->start == end)) {
			gchar *newundostr = g_strconcat(str, lastundo->str, NULL);
			g_free(lastundo->str);
			lastundo->str = g_strdup(newundostr);
			g_free(newundostr);
			lastundo->start = start;
			return;
		}
	}

	/* Reset any non-chainable undo actions */
	thoth_new_undo_action = FALSE;

	/* Not modifying existing undo data. Append a new entry */
	ThothUndoStruct *undo = g_malloc(sizeof(ThothUndoStruct));
	undo->command = command;
	undo->start = start;
	undo->end = end;
	undo->str = g_strdup(str);
	undo->chain = thoth_undo_chain_started;

	/* If "undo chain" is set, consecutive undo items will be chained */
	if (thoth_undo_chain)
		thoth_undo_chain_started = TRUE;

	thoth_editor->undolist = g_list_append(thoth_editor->undolist, undo);
	thoth_editor->undoindex++;
}

static void thoth_callback_inserttext(GtkTextBuffer *buffer,
		GtkTextIter *iter, gchar *str, gint len)
{
	gint start, end;

	end = gtk_text_iter_get_offset(iter);
	start = end - g_utf8_strlen(str, -1);
	thoth_add_undo_data(UNDO_INS, start, end, str);
	thoth_update_title();
	thoth_scroll_to_cursor(GTK_TEXT_VIEW(thoth_editor->textview), 0.05);
}

static void thoth_callback_deleterange(GtkTextBuffer *buffer,
		GtkTextIter *startiter, GtkTextIter *enditer)
{
	gint command, start, end;
	gchar *str;

	start = gtk_text_iter_get_offset(startiter);
	end = gtk_text_iter_get_offset(enditer);
	str = gtk_text_buffer_get_text(buffer, startiter, enditer, FALSE);
	command = (thoth_last_keyval == GDK_KEY_BackSpace) ? UNDO_BS : UNDO_DEL;
	thoth_add_undo_data(command, start, end, str);
	thoth_update_title();
	thoth_scroll_to_cursor(GTK_TEXT_VIEW(thoth_editor->textview), 0.05);
}

/**
 * Following 2 func's prevent excessive messages from buffer changes.
 */
static void thoth_callback_unblockuseraction(GtkTextBuffer *buffer)
{
	g_signal_handlers_unblock_by_func(G_OBJECT(buffer),
			G_CALLBACK(thoth_callback_inserttext), NULL);
	g_signal_handlers_unblock_by_func(G_OBJECT(buffer),
			G_CALLBACK(thoth_callback_deleterange), NULL);
}

static void thoth_callback_blockuseraction(GtkTextBuffer *buffer)
{
	g_signal_handlers_block_by_func(G_OBJECT(buffer),
			G_CALLBACK(thoth_callback_inserttext), NULL);
	g_signal_handlers_block_by_func(G_OBJECT(buffer),
			G_CALLBACK(thoth_callback_deleterange), NULL);
}

void thoth_init_undo(ThothEditorStruct *editor)
{
	editor->undoindex = 0;
	gtk_widget_set_sensitive(GTK_WIDGET(thoth_undotoolitem), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(thoth_redotoolitem), FALSE);
	while (g_list_length(editor->undolist)) {
		g_free(((ThothUndoStruct *)editor->undolist->data)->str);
		g_free(editor->undolist->data);
		editor->undolist = g_list_delete_link(editor->undolist, editor->undolist);
	}
}

/**
 * Generate a random base64 string len bytes long.
 * NOTE: len is length of byte data, not base64 string.
 */
static gchar *thoth_get_random_base64(gint len)
{
	if (len <= 0)
		return NULL;

	guchar *data = g_malloc0(len);
	int rc = RAND_bytes(data, len);
	if (rc != 1) {
		printf("%s:%4d.%4d ERROR: Can't get RAND_bytes\n",
				__FILE__, __LINE__, dbg++);
		return NULL;
	}
	gchar *b64data = g_base64_encode(data, len);
	g_free(data);

	return b64data;
}

static gchar *thoth_encrypt(gchar *plaintext, gchar *password, gchar *base64iv)
{
	gsize iv_size;
	guchar *iv = g_base64_decode(base64iv, &iv_size);

	/* Generate key from password. A regular string should NOT be used here
	 * so the key needs to be a byte array of fixed size. */
	guchar key[256];
	PKCS5_PBKDF2_HMAC_SHA1((const gchar *)password, g_utf8_strlen(password, -1),
			NULL, 0, 1000, 128, key);

	guchar enc_buf[1024];
	gint enc_len, tmp_len;
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv);

	if (EVP_EncryptUpdate(ctx, enc_buf, &enc_len, (guchar *)plaintext,
			g_utf8_strlen(plaintext, -1)) == 0) {
		printf("%s:%4d.%4d ERROR: EVP_EncryptUpdate returned 0\n",
				__FILE__, __LINE__, dbg++);
		return NULL;
	}

	if (EVP_EncryptFinal_ex(ctx, enc_buf+enc_len, &tmp_len) == 0) {
		printf("%s:%4d.%4d ERROR: EVP_EncryptFinal_ex returned 0\n",
				__FILE__, __LINE__, dbg++);
		return NULL;
	}

	enc_len += tmp_len;
	EVP_CIPHER_CTX_free(ctx);

	/* Need to encode as base64 since encrypted data is binary. */
	gchar *b64 = g_base64_encode(enc_buf, enc_len);
	g_free(iv);

	return b64;
}

static gchar *thoth_decrypt(gchar *base64data, gchar *password, gchar *base64iv)
{
	gsize iv_size;
	guchar *iv = g_base64_decode(base64iv, &iv_size);

	gsize bin_size;
	guchar *bin_data = g_base64_decode(base64data, &bin_size);

	/* Generate key from password. A regular string should NOT be used here
	 * so the key needs to be a byte array of fixed size. */
	guchar key[256];
	PKCS5_PBKDF2_HMAC_SHA1((const gchar *)password, g_utf8_strlen(password, -1),
			NULL, 0, 1000, 128, key);

	guchar dec_buf[1024];
	int dec_len, tmp_len;
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();

	EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv);

	if (EVP_DecryptUpdate(ctx, dec_buf, &dec_len, bin_data, bin_size) == 0) {
		printf("%s:%4d.%4d ERROR: EVP_DecryptUpdate returned 0\n",
				__FILE__, __LINE__, dbg++);
		return NULL;
	}

	if (EVP_DecryptFinal_ex(ctx, dec_buf+dec_len, &tmp_len) == 0) {
		printf("%s:%4d.%4d ERROR: EVP_DecryptFinal_ex returned 0\n",
				__FILE__, __LINE__, dbg++);
		return NULL;
	}
	dec_len += tmp_len;

	EVP_CIPHER_CTX_free(ctx);
	g_free(bin_data);
	g_free(iv);

	gchar *str = g_strndup(dec_buf, dec_len);

	return str;
}

static gchar *thoth_get_password_digest(gchar *salt, gchar *password)
{
	guchar sha1[EVP_MAX_MD_SIZE];
	guint len;

	EVP_MD_CTX *ctx = EVP_MD_CTX_create();
	EVP_DigestInit_ex(ctx, EVP_sha1(), NULL);
	EVP_DigestUpdate(ctx, salt, g_utf8_strlen(salt, -1));
	EVP_DigestUpdate(ctx, password, g_utf8_strlen(password, -1));
	EVP_DigestFinal_ex(ctx, sha1, &len);
	EVP_MD_CTX_free(ctx);

	gchar *b64sha1 = g_base64_encode(sha1, len);

	return b64sha1;
}

/**
 * Convenience function to set all margins of a widget.
 */
static void thoth_set_widget_margins(GtkWidget *widget,
		gint top, gint bottom, gint start, gint end)
{
	gtk_widget_set_margin_top(widget, top);
	gtk_widget_set_margin_bottom(widget, bottom);
	gtk_widget_set_margin_start(widget, start);
	gtk_widget_set_margin_end(widget, end);
}

/**
 * Set up callback to enable or disable keyboard shortcut handling, otherwise
 * the text edit window steals commands from other controls when not in focus.
 * (e.g. Ctrl+V for paste, which can be used in the "Find" dialog.)
 * - data: FALSE disables text edit keyboard shortcuts, TRUE to re-enable them.
 */
static gboolean thoth_callback_textview_focus_change(GtkWidget *widget, GdkEvent *event, gpointer data)
{
	thoth_buffertarget = (data) ? NULL : widget;

	return FALSE;
}

/**
 * Convenience function to add a text entry that accepts cut/copy/paste commands.
 */
static GtkWidget *thoth_entry_new_with_buffer(GtkEntryBuffer *buffer)
{
	GtkWidget *entry = gtk_entry_new_with_buffer(GTK_ENTRY_BUFFER(buffer));
	g_signal_connect(entry, "focus-in-event",
			G_CALLBACK(thoth_callback_textview_focus_change), (gpointer)FALSE);
	g_signal_connect(entry, "focus-out-event",
			G_CALLBACK(thoth_callback_textview_focus_change), (gpointer)TRUE);

	return entry;
}

/**
 * Shows password entry popup.
 * - if new_password is TRUE it shows both entry fields for new password
 *   and keeps looping until both passwords match, otherwise only the first
 *   password entry field is shown.
 * Returns the password on success, or NULL if the user cancels at any time.
 */
static gchar *thoth_master_password_dialog(gboolean new_password, GtkWidget *parent)
{
	GtkWidget *label, *passwordentry, *passwordentry2;
	GtkEntryBuffer *entrybuffer;
	gchar *uri = NULL;
	const gchar *get_password_markup = "Enter the master password"
			" to access saved logins.";
	const gchar *new_password_markup = "A master password is needed to save"
			" login details on this device.\n\n"
			"The same master password will be needed to load"
			" saved login passwords also.\n\n"
			"You can enter a master password here, or click cancel"
			" if you don't need to save login details now.\n\n";

	GtkWidget *master_password_dialog = gtk_dialog_new_with_buttons(
			"Enter the Master Password",
			GTK_WINDOW(parent), GTK_DIALOG_MODAL,
			"Cancel", GTK_RESPONSE_CANCEL,
			"Ok", GTK_RESPONSE_ACCEPT, NULL);
	gtk_dialog_set_default_response(GTK_DIALOG(master_password_dialog), GTK_RESPONSE_ACCEPT);

	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(master_password_dialog));
	GtkWidget *grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(content_area), grid);
	gtk_grid_set_column_homogeneous(GTK_GRID(grid), FALSE);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 4);
	gtk_grid_set_row_spacing(GTK_GRID(grid), 4);
	thoth_set_widget_margins(grid, 4, 4, 4, 4);
	gtk_widget_show(grid);

	GtkWidget *frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
	gtk_widget_show(frame);

	GtkWidget *frame_label = gtk_label_new(NULL);
	if (new_password) {
		gtk_label_set_markup(GTK_LABEL(frame_label), new_password_markup);
	} else {
		gtk_label_set_markup(GTK_LABEL(frame_label), get_password_markup);
	}
	gtk_label_set_justify(GTK_LABEL(frame_label), GTK_JUSTIFY_LEFT);
	gtk_label_set_yalign(GTK_LABEL(frame_label), 0);
	gtk_label_set_line_wrap(GTK_LABEL(frame_label), TRUE);
	gtk_label_set_max_width_chars(GTK_LABEL(frame_label), 24);
	gtk_label_set_width_chars(GTK_LABEL(frame_label), 24);
	thoth_set_widget_margins(frame_label, 8, 4, 4, 4);
	gtk_widget_show(frame_label);

	gtk_container_add(GTK_CONTAINER(frame), frame_label);
	gtk_grid_attach(GTK_GRID(grid), frame, 1, 1, 1, 5);

	label = gtk_label_new("Password");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_label_set_yalign(GTK_LABEL(label), 1);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 2, 1, 1, 1);
	entrybuffer = gtk_entry_buffer_new(NULL, -1);
	passwordentry = thoth_entry_new_with_buffer(GTK_ENTRY_BUFFER(entrybuffer));
	gtk_entry_set_activates_default(GTK_ENTRY(passwordentry), TRUE);
	gtk_entry_set_width_chars(GTK_ENTRY(passwordentry), 24);
	gtk_entry_set_visibility(GTK_ENTRY(passwordentry), FALSE);
	g_object_set(G_OBJECT(passwordentry),
			"input-purpose", GTK_INPUT_PURPOSE_PASSWORD, NULL);
	gtk_widget_set_hexpand(passwordentry, TRUE);
	gtk_widget_show(passwordentry);
	gtk_grid_attach(GTK_GRID(grid), passwordentry, 2, 2, 1, 1);

	if (new_password) {
		label = gtk_label_new("Retype Password");
		gtk_widget_set_halign(label, GTK_ALIGN_START);
		gtk_label_set_yalign(GTK_LABEL(label), 1);
		gtk_widget_show(label);
		gtk_grid_attach(GTK_GRID(grid), label, 2, 3, 1, 1);
		entrybuffer = gtk_entry_buffer_new(NULL, -1);
		passwordentry2 = thoth_entry_new_with_buffer(GTK_ENTRY_BUFFER(entrybuffer));
		gtk_entry_set_width_chars(GTK_ENTRY(passwordentry2), 24);
		gtk_entry_set_visibility(GTK_ENTRY(passwordentry2), FALSE);
		g_object_set(G_OBJECT(passwordentry2),
				"input-purpose", GTK_INPUT_PURPOSE_PASSWORD, NULL);
		gtk_widget_set_hexpand(passwordentry2, TRUE);
		gtk_widget_show(passwordentry2);
		gtk_grid_attach(GTK_GRID(grid), passwordentry2, 2, 4, 1, 1);
	}

	gboolean get_password = TRUE;
	gchar *password = NULL;
	while (get_password) {
		gint response = gtk_dialog_run(GTK_DIALOG(master_password_dialog));
		if (response != GTK_RESPONSE_ACCEPT) {
			gtk_widget_destroy(master_password_dialog);
			return NULL;
		}

		const gchar *pw = gtk_entry_get_text(GTK_ENTRY(passwordentry));
		if (new_password) {
			const gchar *pw2 = gtk_entry_get_text(GTK_ENTRY(passwordentry2));
			if (g_strcmp0(pw, pw2) == 0) {
				get_password = FALSE;
			}
		} else {
			get_password = FALSE;
		}

		if (!get_password) {
			password = g_strdup(pw);
		}
		gtk_widget_destroy(master_password_dialog);
	}

	return password;
}

/**
 * Prompts for a new password and saves its salt and digest to disk.
 */
static gboolean thoth_set_master_password()
{
	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	if (!loadstate) {
		keyfile = g_key_file_new();
		g_error_free(err);
		err = NULL;
	}

	gchar *password = thoth_master_password_dialog(TRUE, thoth_window);
	if (password == NULL) {
		g_key_file_free(keyfile);
		return FALSE;
	}
	thoth_master_password = g_strdup(password);

	gchar *salt = thoth_get_random_base64(16);
	gchar *digest = thoth_get_password_digest(salt, password);

	g_key_file_set_string(keyfile, "master password", "digest", digest);
	g_key_file_set_string(keyfile, "master password", "salt", salt);
	g_key_file_save_to_file(keyfile, path, &err);

	g_free(password);
	g_free(salt);
	g_free(digest);
	g_key_file_free(keyfile);

	return TRUE;
}

/**
 * Load master password from thoth-passwords.ini and set thoth_master_password
 *   on success. Keeps looping until success or cancel.
 * Returns TRUE if thoth_master_password set,
 * otherwise FALSE on cancel or if no master password has been saved before.
 */
static gboolean thoth_load_master_password()
{
	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	if (!loadstate) {
		keyfile = g_key_file_new();
		g_error_free(err);
		err = NULL;
	}

	gchar *master_password_digest = g_key_file_get_string(keyfile,
			"master password", "digest", NULL);
	gchar *master_password_salt = g_key_file_get_string(keyfile,
			"master password", "salt", NULL);
	if ((master_password_digest == NULL) || (master_password_salt == NULL)) {
		g_key_file_free(keyfile);
		return FALSE;
	}
	gchar *password = NULL;
	while (password == NULL) {
		password = thoth_master_password_dialog(FALSE, thoth_window);
		if (password == NULL) {
			g_key_file_free(keyfile);
			return FALSE;
		}

		gchar *digest = thoth_get_password_digest(master_password_salt,
				password);
		if (g_strcmp0(digest, master_password_digest) != 0) {
			g_free(password);
			password = NULL; /* Clear password, try again. */
		}
		g_free(digest);
	}

	thoth_master_password = g_strdup(password);
	g_free(password);
	g_key_file_free(keyfile);
}

/**
 * Show an editor by its ID.
 * - id: usually found in thoth_editor->id.
 * - selectiter: TRUE sets sidepanel tree active.
 */
static void thoth_show_editor_by_id(gint id)
{
	GList *list = thoth_editorlist_find_id(id);
	ThothEditorStruct *editor = (ThothEditorStruct *)list->data;
	thoth_editor = editor;
	gint num = gtk_notebook_page_num(GTK_NOTEBOOK(thoth_notebook),
			editor->scrolledwindow);
	gtk_notebook_set_current_page(GTK_NOTEBOOK(thoth_notebook), num);
}

/**
 * Select a treeview iter.
 */
static void thoth_select_sidepanel_iter(GtkTreeModel *treemodel, GtkTreeIter *target_iter,
		gboolean show_editor)
{
	GtkTreePath *targettreepath = gtk_tree_model_get_path(treemodel, target_iter);
	gtk_tree_view_expand_to_path(GTK_TREE_VIEW(thoth_treeview), targettreepath);
	gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(thoth_treeview), targettreepath,
			NULL, TRUE, 0.5, 0);
	gtk_tree_path_free(targettreepath);
	GtkTreeSelection *treesel = gtk_tree_view_get_selection(
			GTK_TREE_VIEW(thoth_treeview));
	gtk_tree_selection_select_iter(treesel, target_iter);

	if (show_editor) {
		gint id;
		gtk_tree_model_get(treemodel, target_iter, SPL_COL_ID, &id, -1);
		thoth_show_editor_by_id(id);
	}
}

/**
 * Process cURL read and write.
 */
static size_t thoth_curl_readfunction(gchar *ptr, size_t size,
		size_t nmemb, FILE *stream)
{
	return fread(ptr, size, nmemb, stream);
}

static size_t thoth_curl_writefunction(gchar *ptr, size_t size,
		size_t nmemb, FILE *stream)
{
	return fwrite(ptr, size, nmemb, stream);
}

/**
 * Returns string from cURL for the FTP functions below.
 */
static size_t thoth_curl_tostringfunction(void *chars, size_t size,
		size_t nmemb, void *data)
{
	size_t realsize = size * nmemb;
	ThothCharString *cs = (ThothCharString *)data;

	cs->chars = (char *)realloc(cs->chars, cs->len + realsize + 1);
	if (!cs->chars) {
		printf("%s:%4d.%4d ERROR: realloc returned NULL.\n",
				__FILE__, __LINE__, dbg++);

		return 0;
	}

	memcpy(&(cs->chars[cs->len]), chars, realsize);
	cs->len += realsize;
	cs->chars[cs->len] = 0; /* NULL terminator */

	return realsize;
}

static gint thoth_get_ftp_permissions(gchar *uri)
{
	/* Allocate memory string for cURL to write to */
	ThothCharString *charstring = g_malloc(sizeof(ThothCharString));
	charstring->chars = NULL; // g_malloc(1);
	charstring->len = 0;

	/* Check if cURL can be set up first */
	curl_global_init(CURL_GLOBAL_ALL); /* Inits winsock in Windows */
	CURL *curl = curl_easy_init();
	if (!curl) {
		printf("%s:%4d.%4d ERROR: curl_easy_init(CURL_GLOBAL_ALL) failed!\n",
				__FILE__, __LINE__, dbg++);
		curl_global_cleanup();
		return FALSE;
	}

	/* Get the URI's file name (basename) and matching FTP directory */
	gchar *basename = g_path_get_basename(uri);
	gchar *dirname = g_path_get_dirname(uri);
	gchar *dir_uri = g_strconcat(dirname, "/", NULL);
	g_free(dirname);

	/* Fetch directory listing through cURL */
	curl_easy_setopt(curl, CURLOPT_URL, dir_uri);
	curl_easy_setopt(curl, CURLOPT_USERNAME, thoth_username);
	curl_easy_setopt(curl, CURLOPT_PASSWORD, thoth_password);
	curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, charstring);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, thoth_curl_tostringfunction);

	curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	CURLcode res = curl_easy_perform(curl);
	curl_easy_cleanup(curl);

	if (res != CURLE_OK) {
		/* we failed */
		fprintf(stderr, "%s:%4d ERROR! curl told us %d\n",
				__FILE__, __LINE__, res);
		return FALSE;
	}

	/* Tokenize fetched data to list files and folders */
	gboolean firstentry = TRUE;
	gint permissionint = 0;
	gint i;
	gchar **lines = g_strsplit(charstring->chars, "\n", 1024);
	for (i = 0; lines[i] != NULL; i++) {
		g_strstrip(lines[i]);
		if (!strlen(lines[i])) {
			continue; /* Skip blank lines */
		}

		/* Tokenize the string for permissions, size, date, filename, etc. */
		gint ct;
		gboolean isDirectory = FALSE;
		gchar *filename, *permissions = NULL;
		gchar **tokens = g_strsplit(lines[i], " ", 30);
		for (ct = 0; tokens[ct] != NULL; ct++) {
			g_strstrip(tokens[ct]);
			if (!strlen(tokens[ct])) {
				continue; /* Blank token */
			}

			/* First entry should be permissions */
			if (!permissions) {
				permissions = g_strdup(tokens[ct]);
				if (permissions[0] == 'd')
					isDirectory = TRUE;
			}
		}
		/* Skip dot entries */
		if (g_strcmp0(tokens[ct-1], ".") == 0) {
			continue;
		}

		/* Last valid entry should be file name */
		filename = g_strdup(tokens[ct-1]);

		/* Compare and extract permission from matching entry */
		if (g_strcmp0(basename, filename) == 0) {
			permissionint = thoth_filepermissions_listingtoint(permissions);
		}

		g_free(permissions);
		g_free(filename);
		g_strfreev(tokens);
	}

	g_free(dir_uri);
	g_strfreev(lines);

	return permissionint;
}

/**
 * Send an FTP request to a ftp server with (optional) username and password.
 * - Returns the FTP uri on success, or FALSE otherwise.
 */
static gchar *thoth_web_address_ftp(gint command, gchar *uri,
		const gchar *username, const gchar *password,
		gchar *localfilepath, gint permissions)
{
	if ((command != THOTH_WEB_OPEN) && (command != THOTH_WEB_SAVE)) {
		return FALSE;
	}

	curl_global_init(CURL_GLOBAL_ALL); /* Inits winsock in Windows */
	CURL *curl = curl_easy_init();
	if (!curl) {
		printf("%s:%4d.%4d ERROR: curl_easy_init(CURL_GLOBAL_ALL) failed!\n",
				__FILE__, __LINE__, dbg++);
		curl_global_cleanup();
		return FALSE;
	}

	/* Setup local environment first. */
	FILE *localfile;
	gchar *upload_as = NULL;
	gchar *rename_to = NULL;
	gchar *ftp_permissions = NULL;
	if (command == THOTH_WEB_OPEN) {
		/* Setup FTP Download */
		gchar *filepath = thoth_web_address_to_local_page_file(uri);
		localfile = fopen(filepath, "wb");
		g_free(filepath);
		if (!localfile) {
			printf("%s:%4d.%4d ERROR: fopen() localfile for web address failed!\n",
					__FILE__, __LINE__, dbg++);
			return FALSE;
		}
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, localfile);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, thoth_curl_writefunction);
		curl_easy_setopt(curl, CURLOPT_URL, uri);
	} else if (command == THOTH_WEB_SAVE) {
		/* Setup FTP Upload */
		gchar *urifilename = g_path_get_basename(uri);
		gchar *uriuploadpath = g_strconcat(uri, ".tmp", NULL);
		localfile = fopen(localfilepath, "rb");
		if (!localfile) {
			printf("%s:%4d.%4d ERROR: fopen() localfile for web address failed!\n",
					__FILE__, __LINE__, dbg++);
			return FALSE;
		}
		curl_easy_setopt(curl, CURLOPT_READDATA, localfile);
		curl_easy_setopt(curl, CURLOPT_READFUNCTION, thoth_curl_readfunction);
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L); /* enable uploading */
		curl_easy_setopt(curl, CURLOPT_URL, uriuploadpath);

		/* FTP commands come last */
		struct curl_slist *headerlist = NULL;
		upload_as = g_strconcat("RNFR ", urifilename, ".tmp", NULL);
		rename_to = g_strconcat("RNTO ", urifilename, NULL);

		headerlist = curl_slist_append(headerlist, upload_as);
		headerlist = curl_slist_append(headerlist, rename_to);

		/* Fetch permissions via FTP if not set */
		if (thoth_editor->unixpermissions == -1) {
			permissions = thoth_get_ftp_permissions(thoth_editor->uri);
			thoth_editor->unixpermissions = permissions;
		}

		/* Only set permissions if it's been modified/fetched */
		if (thoth_editor->unixpermissions != -1) {
			gchar *permstr = thoth_filepermissions_inttooctstr(permissions);
			ftp_permissions = g_strconcat("SITE CHMOD ", permstr, " ", urifilename, NULL);
			g_free(permstr);

			headerlist = curl_slist_append(headerlist, ftp_permissions);
		}

		curl_easy_setopt(curl, CURLOPT_POSTQUOTE, headerlist);

		g_free(urifilename);
		g_free(uriuploadpath);
	}

	/* Process global options second */
	curl_easy_setopt(curl, CURLOPT_USERNAME, username);
	curl_easy_setopt(curl, CURLOPT_PASSWORD, password);
	curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL);

	/* Switch on full protocol/debug output */
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

	curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	CURLcode res = curl_easy_perform(curl);
	fclose(localfile);
	curl_easy_cleanup(curl);

	if (res != CURLE_OK) {
		/* we failed */
		fprintf(stderr, "%s:%4d ERROR! curl told us %d\n",
				__FILE__, __LINE__, res);
		return FALSE;
	}

	g_free(upload_as); /* Ignored if NULL */
	g_free(rename_to); /* Ignored if NULL */
	g_free(ftp_permissions); /* Ignored if NULL */

	gchar *ret_uri = g_strdup(uri);
	return ret_uri;
}

/**
 * Send a post request to a web address with (optional) username and password.
 * - Returns a copy of the URI on success, or FALSE otherwise.
 */
static gchar *thoth_web_address_post(gint command, const gchar *address,
		const gchar *protocol, const gchar *username,
		const gchar *password, gchar *description)
{
	gchar *post_command;
	if (command == THOTH_WEB_OPEN) {
		post_command = g_strdup("md");
	} else if (command == THOTH_WEB_SAVE) {
		post_command = g_strdup("save");
	} else {
		return FALSE;
	}

	/* Before opening, ensure the config dir exists for pages... */
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland", "pages", NULL);
	if (!g_file_test(path, G_FILE_TEST_IS_DIR))
		g_mkdir_with_parents(path, 0700);
	g_free(path);

	gchar *uri = g_strdup(address);
	gchar *filepath = thoth_web_address_to_local_page_file(uri);
	FILE *destfile = fopen(filepath, "wb");
	g_free(filepath);
	if (!destfile) {
		printf("%s:%4d.%4d ERROR: fopen() destfile for web address failed!\n",
				__FILE__, __LINE__, dbg++);
		return FALSE;
	}

	/* Download local copy by cURL */
	curl_global_init(CURL_GLOBAL_ALL); /* Inits winsock in Windows */
	CURL *curl = curl_easy_init();
	if (!curl) {
		printf("%s:%4d.%4d ERROR: curl_easy_init(CURL_GLOBAL_ALL) failed!\n",
				__FILE__, __LINE__, dbg++);
		curl_global_cleanup();
		return FALSE;
	}

	gchar *username_esc = NULL;
	if (username)
		username_esc = g_uri_escape_string(username, NULL, FALSE);

	gchar *password_esc = NULL;
	if (password)
		password_esc = g_uri_escape_string(password, NULL, FALSE);

	gchar *description_esc = NULL;
	if (description)
		description_esc = g_uri_escape_string(description, NULL, FALSE);

	gchar *postfields = g_strdup_printf("pageopt=%s"
			"&username=%s&password=%s&description=%s",
			post_command, username_esc, password_esc, description_esc);
	g_free(username_esc);
	g_free(password_esc);
	g_free(description_esc);

	curl_easy_setopt(curl, CURLOPT_URL, uri);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postfields);
	if (command == THOTH_WEB_OPEN) {
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, destfile);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, thoth_curl_writefunction);
	}
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	CURLcode res = curl_easy_perform(curl);

	g_free(post_command);
	g_free(postfields);

	if (command == THOTH_WEB_OPEN) {
		fclose(destfile);
	}

	if (res != CURLE_OK) {
		printf("%s:%4d.%4d ERROR: curl_easy_perform failed: %s\n",
				__FILE__, __LINE__, dbg++, curl_easy_strerror(res));
		return FALSE;
	}

	long response_code;
	res = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
	if (command == THOTH_WEB_OPEN) {
		if (response_code != 200) {
			printf("%s:%4d.%4d WARNING: Could not open uri '%s'\n",
					__FILE__, __LINE__, dbg++, uri);
			g_free(uri);
			uri = NULL;
		}
	} else if (command == THOTH_WEB_SAVE) {
		if ((res != CURLE_OK) || ((response_code != 301) && (response_code != 302))) {
			printf("%s:%4d.%4d ERROR: Could not save to web (%s) response_code=%d\n",
					__FILE__, __LINE__, dbg++, uri, response_code);
			curl_easy_cleanup(curl);
			curl_global_cleanup();
			return FALSE;
		}
		gchar *location = NULL; /* Do NOT free this! */
		res = curl_easy_getinfo(curl, CURLINFO_REDIRECT_URL, &location);
		if (!location) {
			printf("%s:%4d.%4d ERROR: Could not get redirect URL (%s) \n",
					__FILE__, __LINE__, dbg++, uri);
			curl_easy_cleanup(curl);
			curl_global_cleanup();
			return FALSE;
		}

		g_free(uri);
		uri = g_strdup(location);
	}
	curl_easy_cleanup(curl);
	curl_global_cleanup();

	return uri;
}

/**
 * Save username, password and base64iv to match uri. If uri already exists,
 * update the existing username/password combination for that uri.
 * Needs master_password to encrypt (and later decrypt).
 * Chooses a new master_password if one can't be found on disk.
 * If the user cancels at any time, the passwords for the URI aren't saved.
 * - uri: full address (e.g. "ftp://server.com/filename.php")
 * - protocol: usually "ftp"
 * - username/password: login details for the server at uri.
 * - base64iv: if left blank, a new one will be generated.
 * - savetoeditor: TRUE saves login details to the current thoth_editor instance,
 *   FALSE to not save (e.g. modifying logins in the password manager).
 * Returns TRUE on success, or FALSE on error or user cancel.
 */
static gboolean thoth_save_passwords(gchar *uri, gchar *protocol,
		gchar *username, gchar *password,
		guchar *base64iv, gboolean savetoeditor)
{
	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	if (!loadstate) {
		keyfile = g_key_file_new();
		g_error_free(err);
		err = NULL;
	}

	if (thoth_master_password == NULL) {
		/* If no master password has been saved, get one now. */
		gboolean hasdigest = g_key_file_has_key(keyfile,
				"master password", "digest", &err);
		if (err) {
			g_error_free(err);
			err = NULL;
		}
		gboolean hassalt = g_key_file_has_key(keyfile,
				"master password", "salt", &err);
		if (err) {
			g_error_free(err);
			err = NULL;
		}
		if (!hasdigest || !hassalt) {
			if (!thoth_set_master_password())
				return FALSE; /* No master password set */

			g_key_file_load_from_file(keyfile, path, 0, NULL); /* Reload keyfile */
		} else {
			thoth_load_master_password();
		}

		/* At this stage we _should_ have a master password. */
		if (!thoth_master_password)
			return FALSE; /* Can't save passwords without master password. */
	}

	if (!base64iv) {
		if (!(base64iv = thoth_get_random_base64(16))) {
			printf("%s:%4d.%4d ERROR: Can't generate initial vector.\n",
					__FILE__, __LINE__, dbg++);
			return FALSE;
		}
	}

	gchar *pw = thoth_encrypt(password, thoth_master_password, base64iv);
	gchar *pageid = g_base64_encode(uri, g_utf8_strlen(uri, -1));

	g_key_file_set_string(keyfile, pageid, "protocol", protocol);
	g_key_file_set_string(keyfile, pageid, "username", username);
	g_key_file_set_string(keyfile, pageid, "password", pw);
	g_key_file_set_string(keyfile, pageid, "base64iv", base64iv);

	g_key_file_save_to_file(keyfile, path, &err);
	g_key_file_free(keyfile);

	if (savetoeditor) {
		g_free(thoth_editor->username);
		g_free(thoth_editor->password);

		thoth_editor->protocol = g_strdup(thoth_protocol);
		thoth_editor->username = g_strdup(thoth_username);
		thoth_editor->password = g_strdup(thoth_password);

		if (!thoth_editor->base64iv) {
			thoth_editor->base64iv = g_strdup(base64iv);
		}
	}

	g_free(base64iv);
	g_free(pageid);
	g_free(path);
	g_free(pw);

	return TRUE;
}

/**
 * Get the password for a particular site.
 * Returns TRUE on success. Return FALSE if the user cancels at any time.
 */
static gboolean thoth_load_password(gchar *uri)
{
	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	g_free(path);
	if (!loadstate) {
		return FALSE;
	}

	if (!thoth_master_password) {
		if (!thoth_load_master_password()) {
			return FALSE;
		}
	}

	gchar *pageid = g_base64_encode(uri, g_utf8_strlen(uri, -1));
	gchar *protocol = NULL;
	gchar *username = NULL;
	gchar *password = NULL;
	gchar *encrypted_password = NULL;
	gchar *base64iv = NULL;

	if (g_key_file_has_group(keyfile, pageid))
		protocol = g_key_file_get_string(keyfile, pageid, "protocol", NULL);

	if (g_key_file_has_group(keyfile, pageid))
		username = g_key_file_get_string(keyfile, pageid, "username", NULL);

	if (username)
		password = g_key_file_get_string(keyfile, pageid, "password", NULL);

	if (password)
		base64iv = g_key_file_get_string(keyfile, pageid, "base64iv", NULL);

	if (base64iv) {
		g_free(thoth_protocol);
		g_free(thoth_username);
		g_free(thoth_password);
		g_free(thoth_base64iv);

		gchar *pw = thoth_decrypt(password, thoth_master_password, base64iv);

		thoth_protocol = g_strdup(protocol);
		thoth_username = g_strdup(username);
		thoth_password = g_strdup(pw);
		thoth_base64iv = g_strdup(base64iv);

		g_free(pw);
	}

	g_free(username);
	g_free(password);
	g_free(base64iv);
	g_key_file_free(keyfile);
}

static void thoth_callback_web_recent_combo(GtkComboBox *combo, gpointer data)
{
	ThothWebLoginStruct *weblogin = (ThothWebLoginStruct *)data;
	GtkTreeIter iter;
	gchar *uri, *protocol, *username, *password, *base64iv;
	gtk_combo_box_get_active_iter(GTK_COMBO_BOX(combo), &iter);
	gtk_tree_model_get(GTK_TREE_MODEL(weblogin->webrecentliststore), &iter,
			WRC_COL_URI, &uri, WRC_COL_PROTOCOL, &protocol,
			WRC_COL_USERNAME, &username, WRC_COL_PASSWORD, &password,
			WRC_COL_BASE64IV, &base64iv, -1);

	gtk_entry_set_text(GTK_ENTRY(weblogin->addressentry), uri);
	gtk_entry_set_text(GTK_ENTRY(weblogin->usernameentry), username);
	gtk_entry_set_text(GTK_ENTRY(weblogin->passwordentry), "");
	gtk_combo_box_set_active_id(GTK_COMBO_BOX(weblogin->protocolcombo), protocol);

	/* Attemp to get the master password ... */
	if (!thoth_master_password && password && base64iv)
		thoth_load_master_password();

	/* ... and decode login password if master password set */
	if (thoth_master_password && password && base64iv) {
		gchar *pw = thoth_decrypt(password, thoth_master_password, base64iv);
		gtk_entry_set_text(GTK_ENTRY(weblogin->passwordentry), pw);
		g_free(pw);
	}

	g_free(uri);
	g_free(protocol);
	g_free(username);
	g_free(password);
	g_free(base64iv);
}

/**
 * Populate the "Recent" drop-down box in Web Open/Save dialog
 * with entries from saved logins.
 */
static void thoth_web_address_add_recent_entries(GtkListStore *liststore)
{
	GtkTreeIter iter;

	gtk_list_store_append(liststore, &iter);
	gtk_list_store_set(liststore, &iter,
			WRC_COL_TITLE, "", WRC_COL_URI, "", -1);

	/* Load logins from file */
	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	if (!loadstate) {
		keyfile = g_key_file_new();
		g_error_free(err);
		err = NULL;
	}

	/* Add each login to the combobox list store */
	gsize groupct;
	gchar **groups = g_key_file_get_groups(keyfile, &groupct);
	int i;
	for (i = 0; i < groupct; i++) {
		if (g_strcmp0(groups[i], "master password") == 0)
			continue;

		gsize urilen;
		guchar *uri = g_base64_decode(groups[i], &urilen);
		gchar *protocol = g_key_file_get_string(keyfile, groups[i], "protocol", NULL);
		gchar *username = g_key_file_get_string(keyfile, groups[i], "username", NULL);
		gchar *password = g_key_file_get_string(keyfile, groups[i], "password", NULL);
		gchar *base64iv = g_key_file_get_string(keyfile, groups[i], "base64iv", NULL);
		gchar *title;
		if (g_utf8_strlen(username, 1)) {
			title = g_strconcat(username, "@", uri, NULL);
		} else {
			title = g_strdup(uri);
		}

		gtk_list_store_append(liststore, &iter);
		gtk_list_store_set(liststore, &iter,
				WRC_COL_PAGEID, groups[i], WRC_COL_TITLE, title,
				WRC_COL_URI, uri, WRC_COL_PROTOCOL, protocol,
				WRC_COL_USERNAME, username, WRC_COL_PASSWORD, password,
				WRC_COL_BASE64IV, base64iv, -1);

		g_free(title);
		g_free(uri);
		g_free(protocol);
		g_free(username);
		g_free(password);
		g_free(base64iv);
	}
}

/**
 * Fetch details to access a web page and (optionally) log in.
 * command is either THOTH_WEB_OPEN or THOTH_WEB_SAVE.
 */
static gchar *thoth_web_address_dialog(gint command, gchar *dest_uri)
{
	GtkWidget *label, *recentcombo, *protocolcombo;
	GtkWidget *addressentry, *usernameentry, *passwordentry;
	GtkEntryBuffer *entrybuffer;
	gchar *uri = NULL;
	const gchar *open_markup = "You can read any web page or text file"
			" you have network access to, however"
			" only Mirror Island posts and FTP files can"
			" be uploaded.\n\n"
			"Editing files over FTP use the full file URI\n"
			"<i>(e.g. ftp://ftp.host.com/index.php)</i>\n\n"
			"or a directory to browse an FTP server\n"
			"<i>(e.g. ftp://ftp.host.com/public_html/)</i>\n\n"
			"Mirror Island uses the post's URI\n"
			"<i>(e.g. https://www.mirrorisland.com/123)</i>";
	const gchar *save_markup = "Saving files over FTP use the full file URI\n"
			"<i>(e.g. ftp://ftp.host.com/index.php)</i>\n\n"
			"To update an existing Mirror Island post,"
			" provide its full address\n"
			"<i>(e.g. https://www.mirrorisland.com/123)</i>\n\n"
			"while saving without specifying a post will save a new one\n"
			"<i>(e.g. https://www.mirrorisland.com)</i>";

	gchar *dialog_title;
	gchar *dialog_accept;
	if (command == THOTH_WEB_OPEN) {
		dialog_title = g_strdup("Open Web Address");
		dialog_accept = g_strdup("Open Web Address");
	} else {
		dialog_title = g_strdup("Save to Web");
		dialog_accept = g_strdup("Save to Web");
	}
	GtkWidget *location_dialog = gtk_dialog_new_with_buttons(dialog_title,
			GTK_WINDOW(thoth_window), GTK_DIALOG_MODAL,
			"Cancel", GTK_RESPONSE_CANCEL,
			dialog_accept, GTK_RESPONSE_ACCEPT, NULL);
	g_free(dialog_title);
	g_free(dialog_accept);

	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(location_dialog));
	GtkWidget *content_frame = gtk_frame_new(NULL);
	gtk_container_add(GTK_CONTAINER(content_area), content_frame);
	gtk_widget_show(content_frame);

	/* Set up the recent combo box */
	GtkWidget *recent_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_show(recent_hbox);
	gtk_frame_set_label_widget(GTK_FRAME(content_frame), recent_hbox);
	gtk_frame_set_label_align(GTK_FRAME(content_frame), 0.5, 0.5);

	label = gtk_label_new("Recent:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_label_set_yalign(GTK_LABEL(label), 1);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(recent_hbox), label, TRUE, TRUE, 0);
	GtkListStore *webrecentliststore = gtk_list_store_new(WRC_NUM_COLS,
			G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
			G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	recentcombo = gtk_combo_box_new_with_model(GTK_TREE_MODEL(webrecentliststore));
	gtk_combo_box_set_entry_text_column(GTK_COMBO_BOX(recentcombo), WRC_COL_TITLE);
	gtk_combo_box_set_id_column(GTK_COMBO_BOX(recentcombo), WRC_COL_TITLE);

	GtkCellRenderer *recentcell = gtk_cell_renderer_text_new();
	gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(recentcombo), recentcell, FALSE);
	gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(recentcombo), recentcell, "text", 1, NULL);
	thoth_web_address_add_recent_entries(webrecentliststore);
	gtk_widget_set_hexpand(recentcombo, TRUE);
	gtk_widget_show(recentcombo);
	gtk_box_pack_start(GTK_BOX(recent_hbox), recentcombo, TRUE, TRUE, 0);

	/* Main dialog goes under here */
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_container_add(GTK_CONTAINER(content_frame), hbox);
	thoth_set_widget_margins(hbox, 4, 4, 4, 4);
	gtk_widget_show(hbox);

	GtkWidget *frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
	gtk_widget_show(frame);

	GtkWidget *frame_label = gtk_label_new(NULL);
	if (command == THOTH_WEB_OPEN) {
		gtk_label_set_markup(GTK_LABEL(frame_label), open_markup);
	} else {
		gtk_label_set_markup(GTK_LABEL(frame_label), save_markup);
	}
	gtk_label_set_justify(GTK_LABEL(frame_label), GTK_JUSTIFY_LEFT);
	gtk_label_set_yalign(GTK_LABEL(frame_label), 0);
	gtk_label_set_line_wrap(GTK_LABEL(frame_label), TRUE);
	gtk_label_set_max_width_chars(GTK_LABEL(frame_label), 22);
	gtk_label_set_width_chars(GTK_LABEL(frame_label), 32);
	thoth_set_widget_margins(frame_label, 8, 4, 4, 4);
	gtk_widget_show(frame_label);

	gtk_container_add(GTK_CONTAINER(frame), frame_label);
	gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 0);

	/* The controls go in here (the vbox) */
	GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show(vbox);


	label = gtk_label_new("Protocol:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_label_set_yalign(GTK_LABEL(label), 1);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	protocolcombo = gtk_combo_box_text_new();
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(protocolcombo),
			"http", "Mirror Island (HTTP)");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(protocolcombo),
			"ftps", "FTP over SSL");
	gtk_widget_set_hexpand(protocolcombo, TRUE);
	gtk_widget_show(protocolcombo);
	gtk_box_pack_start(GTK_BOX(vbox), protocolcombo, TRUE, TRUE, 0);

	label = gtk_label_new("Address/URI:");
	gtk_widget_set_margin_top(label, 8);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_label_set_yalign(GTK_LABEL(label), 1);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);

	entrybuffer = gtk_entry_buffer_new(NULL, -1);
	addressentry = thoth_entry_new_with_buffer(GTK_ENTRY_BUFFER(entrybuffer));
	gtk_entry_set_width_chars(GTK_ENTRY(addressentry), 32);
	gtk_widget_set_hexpand(addressentry, TRUE);
	gtk_widget_show(addressentry);
	gtk_box_pack_start(GTK_BOX(vbox), addressentry, TRUE, TRUE, 0);
	if (dest_uri) {
		gtk_entry_set_text(GTK_ENTRY(addressentry), dest_uri);
		g_object_set(G_OBJECT(addressentry),
				"editable", FALSE, NULL);
		gtk_widget_set_sensitive(GTK_WIDGET(addressentry), FALSE);
	}

	label = gtk_label_new("Username: (optional)");
	gtk_widget_set_margin_top(label, 8);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_label_set_yalign(GTK_LABEL(label), 1);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	entrybuffer = gtk_entry_buffer_new(NULL, -1);
	usernameentry = thoth_entry_new_with_buffer(GTK_ENTRY_BUFFER(entrybuffer));
	gtk_entry_set_width_chars(GTK_ENTRY(usernameentry), 32);
	gtk_widget_set_hexpand(usernameentry, TRUE);
	gtk_widget_show(usernameentry);
	gtk_box_pack_start(GTK_BOX(vbox), usernameentry, TRUE, TRUE, 0);

	label = gtk_label_new("Password: (optional)");
	gtk_widget_set_margin_top(label, 8);
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_label_set_yalign(GTK_LABEL(label), 1);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	entrybuffer = gtk_entry_buffer_new(NULL, -1);
	passwordentry = thoth_entry_new_with_buffer(GTK_ENTRY_BUFFER(entrybuffer));
	gtk_entry_set_width_chars(GTK_ENTRY(passwordentry), 32);
	gtk_entry_set_visibility(GTK_ENTRY(passwordentry), FALSE);
	g_object_set(G_OBJECT(passwordentry),
			"input-purpose", GTK_INPUT_PURPOSE_PASSWORD, NULL);
	gtk_widget_set_hexpand(passwordentry, TRUE);
	gtk_widget_show(passwordentry);
	gtk_box_pack_start(GTK_BOX(vbox), passwordentry, TRUE, TRUE, 0);

	GtkWidget *savepasswordcheckbutton = gtk_check_button_new_with_mnemonic(
			"Save _Password");
	gtk_widget_set_margin_bottom(savepasswordcheckbutton, 8);
	gtk_widget_show(savepasswordcheckbutton);
	gtk_box_pack_start(GTK_BOX(vbox), savepasswordcheckbutton, TRUE, TRUE, 0);

	/* Save widget details to update the protocol, address, username,
	 * and password when changing the "Recent" dropdown box */
	ThothWebLoginStruct *weblogin = g_malloc(sizeof(ThothWebLoginStruct));
	weblogin->webrecentliststore = webrecentliststore;
	weblogin->protocolcombo = protocolcombo;
	weblogin->addressentry = addressentry;
	weblogin->usernameentry = usernameentry;
	weblogin->passwordentry = passwordentry;
	g_signal_connect(G_OBJECT(recentcombo), "changed",
			G_CALLBACK(thoth_callback_web_recent_combo), weblogin);

	thoth_savepassword = FALSE; /* Reset this */
	gint response = gtk_dialog_run(GTK_DIALOG(location_dialog));
	if (response == GTK_RESPONSE_ACCEPT) {
		/* Get location path */
		uri = g_strdup(gtk_entry_get_text(GTK_ENTRY(addressentry)));
		thoth_protocol = g_strdup(gtk_combo_box_get_active_id(
				GTK_COMBO_BOX(protocolcombo)));
		thoth_username = g_strdup(gtk_entry_get_text(GTK_ENTRY(usernameentry)));
		thoth_password = g_strdup(gtk_entry_get_text(GTK_ENTRY(passwordentry)));
		thoth_savepassword = gtk_toggle_button_get_active(
				GTK_TOGGLE_BUTTON(savepasswordcheckbutton));
	}
	gtk_widget_destroy(location_dialog);
	g_free(weblogin);

	return uri;
}

static gchar thoth_open_file_from_web(gchar *uri)
{
	/* Handle post request through cURL */
	gboolean try_again = TRUE;
	gboolean load_password = FALSE;
	gboolean show_login = FALSE;
	gchar *ret_uri = NULL;
	while (try_again) {
		if (load_password && !show_login) {
			thoth_load_password(uri); /* Load login details to thoth_editor. */
			load_password = FALSE;
			show_login = TRUE;
		}

		if (g_strcmp0(thoth_protocol, "ftps") == 0) {
			/* Before opening, ensure the config dir exists for pages... */
			gchar *path = g_build_filename(g_get_user_config_dir(),
					"mirrorisland", "pages", NULL);
			if (!g_file_test(path, G_FILE_TEST_IS_DIR))
				g_mkdir_with_parents(path, 0700);
			g_free(path);

			/* Download the file */
			ret_uri = thoth_web_address_ftp(THOTH_WEB_OPEN, uri,
					thoth_username, thoth_password,
					NULL, 0);

			/* Get file permissions from ftp directory listing info */
			if (ret_uri) {
				thoth_editor->unixpermissions = thoth_get_ftp_permissions(ret_uri);
			}
		} else if (g_strcmp0(thoth_protocol, "http") == 0) {
			ret_uri = thoth_web_address_post(THOTH_WEB_OPEN, uri,
					thoth_protocol, thoth_username, thoth_password,
					NULL);
		}

		if (ret_uri) {
			try_again = FALSE;
		} else if (!load_password && !show_login) {
			load_password = TRUE;
		} else {
			printf("%s:%4d.%4d ERROR: thoth_web_address_post could not open %s\n",
					__FILE__, __LINE__, dbg++, uri);
			ret_uri = thoth_web_address_dialog(THOTH_WEB_SAVE, uri);
			if (!ret_uri)
				return FALSE;
		}

		g_free(ret_uri);
		ret_uri = NULL;
	}

	if (thoth_savepassword) {
		thoth_save_passwords(thoth_editor->uri, thoth_protocol,
				thoth_username, thoth_password,
				thoth_editor->base64iv, TRUE);
	}

	return TRUE;
}

/**
 * Callback used for browsing FTP directories below.
 */
static gchar *thoth_show_ftp_browser_cursorchanged(GtkWidget *treeview, gpointer data)
{
	GtkTreeSelection *treesel = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreeIter iter;
	gchar *filename = NULL;
	if (gtk_tree_selection_get_selected(treesel, &model, &iter)) {
		gtk_tree_model_get(model, &iter,
				FTP_COL_FILENAME, &filename, -1);
		gtk_entry_set_text((GtkEntry *)data, filename);
	}
}

/**
 * Update various information about the currently opened file.
 * Returns true on success, false on error.
 */
static gboolean thoth_update_file_stats()
{
	int permissions = 0;

	if ((thoth_editor->category == THOTH_FILE)
			|| (thoth_editor->category == THOTH_NOTE)) {
		GStatBuf statresult;
		int ret = g_stat(thoth_editor->filepath, &statresult);
		if (ret != 0) {
			return FALSE;
		}
		int mode = statresult.st_mode;
		if (mode & S_IRUSR) permissions |= THOTH_OWNERR; /* Owner(user) Read */
		if (mode & S_IWUSR) permissions |= THOTH_OWNERW; /* Owner(user) Write */
		if (mode & S_IXUSR) permissions |= THOTH_OWNERX; /* Owner(user) Execute */
		if (mode & S_IRGRP) permissions |= THOTH_GROUPR; /* Group Read */
		if (mode & S_IWGRP) permissions |= THOTH_GROUPW; /* Group Write */
		if (mode & S_IXGRP) permissions |= THOTH_GROUPX; /* Group Execute */
		if (mode & S_IROTH) permissions |= THOTH_WORLDR; /* World(other) Read */
		if (mode & S_IWOTH) permissions |= THOTH_WORLDW; /* World(other) Write */
		if (mode & S_IXOTH) permissions |= THOTH_WORLDX; /* World(other) Execute */

		thoth_editor->unixpermissions = permissions;
	} else if (thoth_editor->category == THOTH_PAGE) {
		if ((thoth_editor->unixpermissions == -1) && (thoth_loaded)) {
			if (thoth_master_password == NULL) {
				thoth_load_master_password();
			}

			if (thoth_master_password) {
				thoth_load_password(thoth_editor->uri);
				permissions = thoth_get_ftp_permissions(thoth_editor->uri);
				thoth_editor->unixpermissions = permissions;
			}
		}
	}
}

/**
 * Shows a basic FTP browser for a given URI.
 * Can be used to choose an FTP file to either open or save.
 * Returns FTP URI of file on success, or false on cancel or error.
 * TODO: Add FTP features to add subdirectories and delete/rename files.
 */
static gchar *thoth_show_ftp_browser(gchar *uri_in)
{
	gchar *uri = g_strdup(uri_in);

	/* Set up the FTP browser window.
	 * TODO: Change between Open or Save */
	GtkWidget *ftp_browser_dialog = gtk_dialog_new_with_buttons(
			"FTP Browser",
			GTK_WINDOW(thoth_window), GTK_DIALOG_MODAL,
			"Cancel", GTK_RESPONSE_CANCEL,
			"Open", GTK_RESPONSE_ACCEPT, NULL);

	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(ftp_browser_dialog));
	gtk_window_set_default_size(GTK_WINDOW(ftp_browser_dialog), 480, 480);

	GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show(vbox);
	gtk_container_add(GTK_CONTAINER(content_area), vbox);

	GtkWidget *ftpdirentry = gtk_entry_new();
	g_object_set(G_OBJECT(ftpdirentry),
			"editable", FALSE, NULL);
	gtk_widget_show(ftpdirentry);
	gtk_box_pack_start(GTK_BOX(vbox), ftpdirentry, FALSE, TRUE, 0);

	GtkWidget *ftpfileentry = gtk_entry_new();
	gtk_widget_show(ftpfileentry);
	gtk_box_pack_end(GTK_BOX(vbox), ftpfileentry, FALSE, TRUE, 0);

	GtkWidget *scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(scrolledwindow);
	gtk_box_pack_start(GTK_BOX(vbox), scrolledwindow, TRUE, TRUE, 0);

	GtkListStore *liststore = gtk_list_store_new(FTP_NUM_COLS,
			G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN,
			G_TYPE_INT, G_TYPE_INT, G_TYPE_STRING);
	GtkWidget *treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(liststore));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(treeview), TRUE);
	gtk_tree_view_set_tooltip_column(GTK_TREE_VIEW(treeview), FTP_COL_TOOLTIP);
	g_signal_connect(treeview, "cursor-changed",
			G_CALLBACK(thoth_show_ftp_browser_cursorchanged), ftpfileentry);
	gtk_container_add(GTK_CONTAINER(scrolledwindow), treeview);
	gtk_widget_set_hexpand(scrolledwindow, TRUE);
	gtk_widget_set_vexpand(scrolledwindow, TRUE);
	gtk_widget_show(treeview);

	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Permissions", renderer,
			"text", FTP_COL_PERMISSIONS, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Filename", renderer,
			"text", FTP_COL_FILENAME, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);

	/* Now keep browsing the FTP directory until a file is chosen */
	gchar *returi = NULL;
	while (!returi) {
		/* Allocate memory string for cURL to write to */
		ThothCharString *charstring = g_malloc(sizeof(ThothCharString));
		charstring->chars = NULL; // g_malloc(1);
		charstring->len = 0;

		/* Check if cURL can be set up first */
		curl_global_init(CURL_GLOBAL_ALL); /* Inits winsock in Windows */
		CURL *curl = curl_easy_init();
		if (!curl) {
			printf("%s:%4d.%4d ERROR: curl_easy_init(CURL_GLOBAL_ALL) failed!\n",
					__FILE__, __LINE__, dbg++);
			curl_global_cleanup();
			return FALSE;
		}

		/* Fetch directory listing through cURL */
		curl_easy_setopt(curl, CURLOPT_URL, uri);
		curl_easy_setopt(curl, CURLOPT_USERNAME, thoth_username);
		curl_easy_setopt(curl, CURLOPT_PASSWORD, thoth_password);
		curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, charstring);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, thoth_curl_tostringfunction);

		curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
		CURLcode res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);

		if (res != CURLE_OK) {
			/* we failed */
			fprintf(stderr, "%s:%4d ERROR! curl told us %d\n",
					__FILE__, __LINE__, res);
			return FALSE;
		}

		/* Tokenize fetched data to list files and folders */
		gboolean firstentry = TRUE;
		gint i;
		gchar **lines = g_strsplit(charstring->chars, "\n", 1024);
		for (i = 0; lines[i] != NULL; i++) {
			g_strstrip(lines[i]);
			if (!strlen(lines[i])) {
				continue; /* Skip blank lines */
			}

			/* Tokenize the string for permissions, size, date, filename, etc. */
			gint ct;
			gboolean isDirectory = FALSE;
			gchar *filename, *permissions = NULL;
			gchar **tokens = g_strsplit(lines[i], " ", 30);
			for (ct = 0; tokens[ct] != NULL; ct++) {
				g_strstrip(tokens[ct]);
				if (!strlen(tokens[ct])) {
					continue; /* Blank token */
				}

				/* First entry should be permissions */
				if (!permissions) {
					permissions = g_strdup(tokens[ct]);
					if (permissions[0] == 'd')
						isDirectory = TRUE;
				}
			}
			/* Skip dot entries */
			if (g_strcmp0(tokens[ct-1], ".") == 0) {
				continue;
			}

			/* Last valid entry should be file name */
			filename = g_strdup(tokens[ct-1]);

			GtkTreeIter iter;
			gtk_list_store_append(liststore, &iter);
			gtk_list_store_set(liststore, &iter,
					FTP_COL_TOOLTIP, lines[i], /* Raw string is tooltip */
					FTP_COL_PERMISSIONS, permissions,
					FTP_COL_ISDIRECTORY, isDirectory,
					FTP_COL_FILENAME, filename, -1);

			if (firstentry) {
				GtkTreeSelection *treesel = gtk_tree_view_get_selection(
						GTK_TREE_VIEW(treeview));
				gtk_tree_selection_select_iter(treesel, &iter);
				firstentry = FALSE;
			}

			g_free(permissions);
			g_free(filename);
			g_strfreev(tokens);
		}
		g_strfreev(lines);
		gtk_widget_grab_focus(treeview);
		gtk_entry_set_text(GTK_ENTRY(ftpdirentry), uri);

		/* Display the listing */
		gint response = gtk_dialog_run(GTK_DIALOG(ftp_browser_dialog));

		if (response != GTK_RESPONSE_ACCEPT) {
			gtk_widget_destroy(ftp_browser_dialog);
			return NULL;
		}

		/* Check if entering subdirectory or opening a file here,
		 * and update uri string on subdirectory */
		GtkTreeSelection *treesel = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
		GtkTreeIter iter;
		gchar *filename = NULL;
		if (gtk_tree_selection_get_selected(treesel, &model, &iter)) {
			gboolean isdir;

			gtk_tree_model_get(model, &iter,
					FTP_COL_ISDIRECTORY, &isdir,
					FTP_COL_FILENAME, &filename, -1);

			if (isdir) {
				/* Navigate to another directory */
				gchar *newuri;
				if (g_strcmp0(filename, "..") == 0) {
					newuri = thoth_get_uri_parent(uri);
				} else {
					newuri = g_strconcat(uri, filename, "/", NULL);
				}
				g_free(uri);
				uri = g_strdup(newuri);
				g_free(newuri);
			} else {
				/* Return uri with selected file, using text in entry if possible */
				gchar *newuri;
				const gchar *entrytext = gtk_entry_get_text(GTK_ENTRY(ftpfileentry));
				if (strlen(entrytext)) {
					newuri = g_strconcat(uri, entrytext, NULL);
				} else {
					newuri = g_strconcat(uri, filename, NULL);
				}
				returi = g_strdup(newuri);
				g_free(newuri);
			}

			g_free(filename);
		}

		/* All done! Free resource from here on */
		g_free(charstring->chars);
		g_free(charstring);
		gtk_list_store_clear(liststore);
	}

	gtk_widget_destroy(ftp_browser_dialog);
	g_free(uri);

	return returi; /* Force FALSE for now... */
}

/**
 * Open file functions.
 * - category is THOTH_PAGE, THOTH_FILE or THOTH_NOTE.
 * Sets thoth_editor on load success.
 * Returns editor struct id.
 * Special condition: If an FTP directory is provided in uri_open, it opens
 * the FTP browser and returns the URI for a file on that server.
 */
static gint thoth_open_file(gchar *uri_open, gint category, gboolean connect_undo,
		gboolean force_reload)
{
	if (!G_LIKELY(uri_open))
		return 0;

	gchar *uri = g_strdup(uri_open);

	if (!force_reload) {
		GtkTreeIter *treeiter = thoth_treestore_iter_find_uri(thoth_treemodel, uri);
		if (treeiter) {
			gint id;
			gtk_tree_model_get(thoth_treemodel, treeiter,
					SPL_COL_ID, &id, -1);
			/* Focus on already opened file instead of reopening it. */
			thoth_select_sidepanel_iter(thoth_treemodel, treeiter, TRUE);
			return id;
		}
	}

	gchar *filepath;
	if ((category == THOTH_FILE) || (category == THOTH_NOTE)) {
		filepath = thoth_filename_from_uri(uri);
	} else if (category == THOTH_PAGE) {
		char ch = uri[strlen(uri) - 1];
		if (ch == '/') {
			/* Show FTP browser */
			gchar *new_uri = thoth_show_ftp_browser(uri);
			if (!new_uri) {
				return FALSE;
			}

			g_free(uri);
			uri = g_strdup(new_uri);
			g_free(new_uri);
		}

		filepath = thoth_web_address_to_local_page_file(uri);
		gboolean loadwebpage = TRUE;
		if ((g_file_test(filepath, G_FILE_TEST_EXISTS))
				&& (g_file_test(filepath, G_FILE_TEST_IS_REGULAR))) {
			loadwebpage = FALSE;
		}
		if ((force_reload) || (loadwebpage)) {
			if (!thoth_open_file_from_web(uri)) {
				g_free(filepath);
				return FALSE;
			}
		}
	}

	/* Test that the file name is valid, and load the file if it is. */
	if (!g_file_test(filepath, G_FILE_TEST_EXISTS)
			|| !g_file_test(filepath, G_FILE_TEST_IS_REGULAR)
			|| g_file_test(filepath, G_FILE_TEST_IS_SYMLINK)) {
		g_free(filepath);
		return FALSE;
	}

	gchar *file_contents;
	gsize file_length;
	GError *err = NULL;
	gboolean read_success = g_file_get_contents(filepath, &file_contents,
			&file_length, &err);
	if (!read_success) {
		printf("%s:%4d.%4d ERROR: %s\n", __FILE__, __LINE__, dbg++, err->message);
		g_error_free(err);
		g_free(filepath);
		return 0;
	}

	if (!force_reload) {
		thoth_editor = thoth_new_window_add_editor(category);
		thoth_editor->uri = g_strdup(uri);
		thoth_editor->filepath = g_strdup(filepath);
		thoth_editor->category = category;
	}

	if (thoth_editor->category == THOTH_PAGE) {
		if (thoth_savepassword) {
			thoth_save_passwords(thoth_editor->uri, thoth_protocol,
					thoth_username, thoth_password,
					thoth_editor->base64iv, TRUE);
			thoth_savepassword = FALSE;
		}

		/* Freeing and setting these global values as NULL
		 * shows the web page login dialog (if needed) */
		g_free(thoth_protocol);
		g_free(thoth_username);
		g_free(thoth_password);
		g_free(thoth_base64iv);
		thoth_protocol = NULL;
		thoth_username = NULL;
		thoth_password = NULL;
		thoth_base64iv = NULL;
	}

	/* Get file permissions, and possibly Mirror Island permissions */
	thoth_update_file_stats();

	/* TODO: Detect the text file's character set encoding and line endings
	 * (CR, LF or both) and convert file_contents if necessary. */

	GtkTextIter iter;
	thoth_callback_blockuseraction(thoth_editor->textbuffer);
	gtk_text_buffer_set_text(thoth_editor->textbuffer, file_contents, file_length);
	thoth_callback_unblockuseraction(thoth_editor->textbuffer);
	gtk_text_buffer_set_modified(thoth_editor->textbuffer, FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(thoth_savetoolitem), FALSE);
	thoth_init_undo(thoth_editor);
	gtk_text_buffer_get_start_iter(thoth_editor->textbuffer, &iter);
	gtk_text_buffer_place_cursor(thoth_editor->textbuffer, &iter);
	gtk_widget_grab_focus(thoth_editor->textview);

	thoth_set_current_uri(TRUE);

	g_free(file_contents);
	g_free(filepath);
	g_free(uri);

	return thoth_editor->id;
}

/**
 * Setup and call undo/redo functions.
 */
gboolean thoth_command_undo()
{
	GtkTextIter startiter, enditer;

	gint length = g_list_length(thoth_editor->undolist);
	if ((!g_list_length(thoth_editor->undolist)) || (thoth_editor->undoindex == 0)) {
		return FALSE;
	}

	gboolean chain = FALSE;
	do {
		ThothUndoStruct *undo = g_list_nth_data(thoth_editor->undolist, thoth_editor->undoindex-1);
		gtk_text_buffer_get_iter_at_offset(thoth_editor->textbuffer, &startiter, undo->start);
		chain = undo->chain;

		switch (undo->command) {
		case UNDO_INS:
			gtk_text_buffer_get_iter_at_offset(thoth_editor->textbuffer, &enditer, undo->end);
			gtk_text_buffer_delete(thoth_editor->textbuffer, &startiter, &enditer);
			break;
		default:
			gtk_text_buffer_insert(thoth_editor->textbuffer, &startiter, undo->str, -1);
		}
		if (thoth_editor->undoindex > 0) {
			thoth_editor->undoindex--;
			gtk_widget_set_sensitive(GTK_WIDGET(thoth_redotoolitem), TRUE);
		}
		if (thoth_editor->undoindex <= 0) {
			thoth_editor->undoindex = 0;
			gtk_widget_set_sensitive(GTK_WIDGET(thoth_undotoolitem), FALSE);
			chain = FALSE;
		}
	} while (chain);

	gtk_widget_set_sensitive(GTK_WIDGET(thoth_redotoolitem), TRUE);
	gtk_text_buffer_place_cursor(thoth_editor->textbuffer, &startiter);
	thoth_scroll_to_cursor(GTK_TEXT_VIEW(thoth_editor->textview), 0.05);
}

void thoth_command_redo()
{
	GtkTextIter startiter, enditer;

	gint listlength = g_list_length(thoth_editor->undolist);
	if ((listlength == 0) || (thoth_editor->undoindex >= listlength)) {
		return;
	}

	gboolean chain = FALSE;
	do {
		ThothUndoStruct *undo = g_list_nth_data(thoth_editor->undolist, thoth_editor->undoindex);
		gtk_text_buffer_get_iter_at_offset(thoth_editor->textbuffer, &startiter, undo->start);
		if (thoth_editor->undoindex+1 < listlength) {
			ThothUndoStruct *nextundo = g_list_nth_data(thoth_editor->undolist, thoth_editor->undoindex+1);
			chain = nextundo->chain;
		}

		switch (undo->command) {
		case UNDO_INS:
			gtk_text_buffer_insert(thoth_editor->textbuffer, &startiter, undo->str, -1);
			break;
		default:
			gtk_text_buffer_get_iter_at_offset(thoth_editor->textbuffer, &enditer, undo->end);
			gtk_text_buffer_delete(thoth_editor->textbuffer, &startiter, &enditer);
		}

		if (thoth_editor->undoindex < listlength) {
			thoth_editor->undoindex++;
			gtk_widget_set_sensitive(GTK_WIDGET(thoth_undotoolitem), TRUE);
		}
		if (thoth_editor->undoindex >= listlength) {
			thoth_editor->undoindex = listlength;
			gtk_widget_set_sensitive(GTK_WIDGET(thoth_redotoolitem), FALSE);
			chain = FALSE;
		}
	} while (chain);

	gtk_text_buffer_place_cursor(thoth_editor->textbuffer, &startiter);
	thoth_scroll_to_cursor(GTK_TEXT_VIEW(thoth_editor->textview), 0.05);
}

/**
 * Edit commands
 */
void thoth_command_edit_cut()
{
	GtkWidget *widget = thoth_editor->textview;
	if (thoth_buffertarget) {
		widget = thoth_buffertarget;
	}
	g_signal_emit_by_name(G_OBJECT(widget), "cut-clipboard");
}

void thoth_command_edit_copy()
{
	GtkWidget *widget = thoth_editor->textview;
	if (thoth_buffertarget) {
		widget = thoth_buffertarget;
	}
	g_signal_emit_by_name(G_OBJECT(widget), "copy-clipboard");
}

void thoth_command_edit_paste()
{
	GtkWidget *widget = thoth_editor->textview;
	if (thoth_buffertarget) {
		widget = thoth_buffertarget;
	}
	thoth_new_undo_action = TRUE; /* Pastes have their own undo action */
	thoth_set_undochain(TRUE);
	g_signal_emit_by_name(G_OBJECT(widget), "paste-clipboard");
	thoth_set_undochain(FALSE);
}

void thoth_command_edit_selectall()
{
	GtkWidget *widget = thoth_editor->textview;
	if (thoth_buffertarget) {
		/* Text entries don't accept "select-all" signals by default.
		 * Handle Ctrl+A ourselves instead */
		gtk_editable_select_region(GTK_EDITABLE(thoth_buffertarget), 0, -1);
		return;
	}
	g_signal_emit_by_name(G_OBJECT(widget), "select-all");
}

gboolean thoth_command_find_next(gint searchdirection, gboolean loop_search)
{
	GtkTextIter itercurr, startiter, enditer;
	gboolean found;

	const gchar *findtext = gtk_entry_get_text(GTK_ENTRY(thoth_findentry));
	gtk_text_mark_set_visible(
			gtk_text_buffer_get_selection_bound(thoth_editor->textbuffer), FALSE);
	GtkTextMark *mark = gtk_text_buffer_get_insert(thoth_editor->textbuffer);
	gtk_text_buffer_get_iter_at_mark(thoth_editor->textbuffer, &itercurr, mark);
	GtkTextSearchFlags flags = GTK_TEXT_SEARCH_CASE_INSENSITIVE;
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(thoth_casesensitivecheck)))
		flags = 0;

	found = FALSE;
	if (searchdirection > 0) {
		found = gtk_text_iter_forward_search(&itercurr, findtext, flags,
				&startiter, &enditer, NULL);
	} else if (searchdirection < 0) {
		found = gtk_text_iter_backward_search(&itercurr, findtext, flags,
				&startiter, &enditer, NULL);
		/**
		 * Sometimes backwards search loops on the same text,
		 * so do another search if the iter hasn't moved.
		 * It could be more efficient, but at least it works.
		 */
		if (gtk_text_iter_equal(&itercurr, &enditer)) {
			found = gtk_text_iter_backward_search(&startiter, findtext, flags,
					&startiter, &enditer, NULL);
		}
	}

	/* No Match? Try wrap around. */
	if (!found && loop_search) {
		if (searchdirection > 0) {
			gtk_text_buffer_get_start_iter(thoth_editor->textbuffer, &itercurr);
			found = gtk_text_iter_forward_search(&itercurr, findtext, flags,
					&startiter, &enditer, NULL);
		} else if (searchdirection < 0) {
			gtk_text_buffer_get_end_iter(thoth_editor->textbuffer, &itercurr);
			found = gtk_text_iter_backward_search(&itercurr, findtext, flags,
					&startiter, &enditer, NULL);
		}
	}

	if (found) {
		gtk_text_buffer_place_cursor(thoth_editor->textbuffer, &startiter);
		gtk_text_buffer_move_mark_by_name(thoth_editor->textbuffer, "insert", &enditer);
		thoth_scroll_to_cursor(GTK_TEXT_VIEW(thoth_editor->textview), 0.05);
		/* gtk_widget_grab_focus(thoth_editor->textview); */
	} else {
		/* Alert user that nothing has been found! */
		return FALSE;
	}
	return TRUE;
}

void thoth_command_find_replace(gboolean replace_all)
{
	GtkTextIter startiter, enditer;
	gboolean replace = FALSE;

	/* Compare current selection to find text. */
	const gchar *findtext = gtk_entry_get_text(GTK_ENTRY(thoth_findentry));
	if (g_utf8_strlen(findtext, -1) == 0) {
		return; /* Can't proceed: Zero length find string. */
	}
	gchar *str_tofind = g_utf8_casefold(findtext, -1);

	/* IF replace_all, start from the beginning */
	if (replace_all) {
		GtkTextIter iter;
		gtk_text_buffer_get_start_iter(thoth_editor->textbuffer, &iter);
		gtk_text_buffer_place_cursor(thoth_editor->textbuffer, &iter);
	}

	/* This is a loop, but only replace_all causes it to cycle. */
	do {
		gchar *str_found = NULL;
		gtk_text_buffer_get_selection_bounds(thoth_editor->textbuffer,
				&startiter, &enditer);
		if (gtk_text_iter_equal(&startiter, &enditer)) {
			/* Find next if not replacing. */
			replace = FALSE; /* Can't check for replace: No selected text. */
			gboolean loopsearch = !replace_all;
			gboolean found = thoth_command_find_next(1, loopsearch);
			if (!found) {
				replace_all = FALSE;
			}
		} else {
			replace = TRUE;
			gchar *str = gtk_text_iter_get_slice(&startiter, &enditer);
			if (g_utf8_strlen(str, -1) != 0) {
				str_found = g_utf8_casefold(
						gtk_text_iter_get_slice(&startiter, &enditer), -1);
			} else {
				replace = FALSE;
			}
			g_free(str);

		}

		/* If it matches, replace the current selection. */
		if (replace && (g_strcmp0(str_tofind, str_found) == 0)) {
			GtkTextMark *mark = gtk_text_buffer_create_mark(thoth_editor->textbuffer,
					NULL, &startiter, FALSE);
			const gchar *findtext = gtk_entry_get_text(GTK_ENTRY(thoth_replaceentry));
			g_signal_emit_by_name(G_OBJECT(thoth_editor->textbuffer),
					"begin-user-action");
			gtk_text_buffer_delete(thoth_editor->textbuffer, &startiter, &enditer);
			gtk_text_buffer_get_iter_at_mark(thoth_editor->textbuffer, &startiter, mark);
			gtk_text_buffer_insert(thoth_editor->textbuffer, &startiter, findtext, -1);
			g_signal_emit_by_name(G_OBJECT(thoth_editor->textbuffer), "end-user-action");
			gtk_text_buffer_delete_mark(thoth_editor->textbuffer, mark);
		}
		g_free(str_found);
	} while (replace_all);
	g_free(str_tofind);
}

/**
 * Functions to handle "Jump to..." commands.
 */
static void thoth_jumpto_close()
{
	gtk_widget_destroy(thoth_jumptohbox);
	thoth_jumptohbox = NULL;
	thoth_jumptospinentry = NULL;
}

static void thoth_command_jumpto()
{
	GtkTextIter iter;

	gint line = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(thoth_jumptospinentry));
	gtk_text_buffer_get_iter_at_line_offset(thoth_editor->textbuffer, &iter,
			line - 1, 0);
	gtk_text_buffer_place_cursor(thoth_editor->textbuffer, &iter);
	thoth_scroll_to_cursor(GTK_TEXT_VIEW(thoth_editor->textview), 0.05);
	gtk_widget_grab_focus(thoth_editor->textview);

	thoth_jumpto_close();
}

static gboolean thoth_callback_jumpto_keypress(GtkWidget *widget,
		GdkEventKey *event, gpointer data)
{
	if (event->keyval == GDK_KEY_Escape) {
		thoth_jumpto_close();
		return TRUE;
	} else {
		gtk_spin_button_update(GTK_SPIN_BUTTON(widget));
	}

	return FALSE;
}

static void thoth_command_edit_jumpto()
{
	GtkAdjustment *adjustment;
	GtkWidget *button;
	GtkTextIter iter;
	gint linenum, maxnum;

	/* Get current and last line numbers */
	gtk_text_buffer_get_iter_at_mark(thoth_editor->textbuffer, &iter,
			gtk_text_buffer_get_insert(thoth_editor->textbuffer));
	linenum = gtk_text_iter_get_line(&iter) + 1;

	gtk_text_buffer_get_end_iter(thoth_editor->textbuffer, &iter);
	maxnum = gtk_text_iter_get_line(&iter) + 1;

	adjustment = gtk_adjustment_new(linenum, 1, maxnum, 1, 1, 0);

	if (thoth_jumptohbox) {
		gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(thoth_jumptospinentry), adjustment);
	} else {
		/* Generate the widgets and add it to the overlay layer */
		thoth_jumptohbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

		thoth_jumptospinentry = gtk_spin_button_new(adjustment, 1, 0);
		gtk_entry_set_width_chars(GTK_ENTRY(thoth_jumptospinentry), 8);
		gtk_box_pack_start(GTK_BOX(thoth_jumptohbox), thoth_jumptospinentry, TRUE, TRUE, 0);
		g_signal_connect(thoth_jumptospinentry, "key-press-event",
				G_CALLBACK(thoth_callback_jumpto_keypress), NULL);
		g_signal_connect(thoth_jumptospinentry, "activate",
				G_CALLBACK(thoth_command_activate), GINT_TO_POINTER(COMMAND_JUMPTO));

		button = gtk_button_new_with_label("Jump");
		gtk_box_pack_end(GTK_BOX(thoth_jumptohbox), button, TRUE, TRUE, 0);
		g_signal_connect(button, "clicked",
				G_CALLBACK(thoth_command_activate), GINT_TO_POINTER(COMMAND_JUMPTO));

		gtk_widget_set_halign(thoth_jumptohbox, GTK_ALIGN_END);
		gtk_widget_set_valign(thoth_jumptohbox, GTK_ALIGN_END);
		gtk_widget_show_all(thoth_jumptohbox);
		gtk_overlay_add_overlay(GTK_OVERLAY(thoth_editoroverlay), thoth_jumptohbox);
	}

	gtk_widget_grab_focus(thoth_jumptospinentry);
}

static gint thoth_confirm_dialog(gchar *title, gchar *message, gchar *cancel, gchar *accept)
{
	GtkWidget *label, *dialog = gtk_dialog_new_with_buttons(title,
			GTK_WINDOW(thoth_window), GTK_DIALOG_MODAL,
			cancel, GTK_RESPONSE_CANCEL,
			accept, GTK_RESPONSE_ACCEPT, NULL);
	label = gtk_label_new(message);
	gtk_widget_set_margin_top(GTK_WIDGET(label), 16);
	gtk_widget_set_margin_bottom(GTK_WIDGET(label), 16);
	gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), label);
	gtk_widget_show(label);

	gint response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return response;
}

/**
 * NEXTINLINE Called by following function.
 * [select_treeview] focuses on the newly added item if TRUE.
 */
void thoth_add_sidepanel_item(GtkTreeIter *parentiter, gint category, gint editorid,
		gchar *uri, gchar *tooltip, gboolean select_treeview)
{
	gchar *title;
	if (category == THOTH_NOTE) {
		title = thoth_get_snippet(255, TRUE);
	} else if (category == THOTH_PAGE) {
		title = thoth_get_snippet(255, TRUE);
	} else if (uri) {
		title = g_path_get_basename(uri);
	}
	if ((title == NULL) || (g_utf8_strlen(title, -1) == 0)) {
		title = g_strdup("(Untitled)");
	}
	gchar *tooltip_esc = g_markup_escape_text(tooltip, -1);
	GtkTreeIter treeiter;
	gtk_tree_store_append(GTK_TREE_STORE(thoth_treemodel), &treeiter, parentiter);
	gtk_tree_store_set(GTK_TREE_STORE(thoth_treemodel), &treeiter,
			SPL_COL_ID, editorid, SPL_COL_CATEGORY, category,
			SPL_COL_TITLE, title, SPL_COL_URI, uri, SPL_COL_TOOLTIP, tooltip_esc, -1);
	g_free(title);
	g_free(tooltip_esc);

	/* Select newly added item, if requested. */
	if (select_treeview)
		thoth_select_sidepanel_iter(thoth_treemodel, &treeiter, TRUE);
}

static void thoth_remove_recent_uri(gchar *uri)
{
	/* Cycle through existing recent menu items, remove if already there. */
	GList *list = thoth_recentfilelist;
	while (list != NULL) {
		if (g_strcmp0(((ThothRecentStruct *)list->data)->uri, uri) == 0) {
			gtk_widget_destroy(((ThothRecentStruct *)list->data)->menuitem);
			g_free(((ThothRecentStruct *)list->data)->uri);
			g_free((ThothRecentStruct *)list->data);
			thoth_recentfilelist = g_list_delete_link(thoth_recentfilelist, list);
		}
		list = list->next;
	}
}

static void thoth_prune_recent_uri_list()
{
	GList *list = thoth_recentfilelist;
	gint i = 1;
	while (list != NULL) {
		GList *list_next = list->next;
		if (i > thoth_recentfilemax) {
			gtk_widget_destroy(((ThothRecentStruct *)list->data)->menuitem);
			g_free(((ThothRecentStruct *)list->data)->uri);
			g_free((ThothRecentStruct *)list->data);
			thoth_recentfilelist = g_list_delete_link(thoth_recentfilelist, list);
		}
		i++;
		list = list_next;
	}
}

/**
 * Save/load settings on exit/startup.
 */
static void thoth_save_config()
{
	GError *err = NULL;
	gboolean state;
	gint left, top, width, height;
	gchar *path;
	GKeyFile *keyfile = g_key_file_new();

	/* Save sidepanel state. */
	state = thoth_sidepanelvisible;
	g_key_file_set_boolean(keyfile, "Settings", "SidePanelVisible", state);

	/* Window size and position */
	gtk_window_get_size(GTK_WINDOW(thoth_window), &width, &height);
	g_key_file_set_integer(keyfile, "Settings", "WindowWidth", width);
	g_key_file_set_integer(keyfile, "Settings", "WindowHeight", height);
	gtk_window_get_position(GTK_WINDOW(thoth_window), &left, &top);
	g_key_file_set_integer(keyfile, "Settings", "WindowLeft", left);
	g_key_file_set_integer(keyfile, "Settings", "WindowTop", top);

	/* Sidepanel size and position. */
	gint editorpanedpos = gtk_paned_get_position(GTK_PANED(thoth_editorpaned));
	g_key_file_set_integer(keyfile, "Settings", "EditorPanedPosition", editorpanedpos);

	/* Save options */
	g_key_file_set_boolean(keyfile, "Settings", "OptionsWrapLines", thoth_wraplines);
	g_key_file_set_boolean(keyfile, "Settings", "OptionsShowLineNumbers",
			thoth_showlinenumbers);
	g_key_file_set_boolean(keyfile, "Settings", "OptionsAutoindent", thoth_autoindent);
	g_key_file_set_boolean(keyfile, "Settings", "OptionsKeepAbove", thoth_keepabove);
	g_key_file_set_boolean(keyfile, "Settings", "OptionsReopenFilesOnStartup",
			thoth_reopenfilesonstartup);

	g_key_file_set_integer(keyfile, "Settings", "RecentFilesMax", thoth_recentfilemax);
	g_key_file_set_integer(keyfile, "Settings", "TabSize", thoth_tabsize);
	g_key_file_set_integer(keyfile, "Settings", "CloseButtonAction", thoth_closebuttonaction);
	g_key_file_set_string(keyfile, "Settings", "FontName", thoth_textview_font);
	g_key_file_set_string(keyfile, "Settings", "TextColour",
			gdk_rgba_to_string(&thoth_textview_fg_rgba));
	g_key_file_set_string(keyfile, "Settings", "BackgroundColour",
			gdk_rgba_to_string(&thoth_textview_bg_rgba));

	/* Before saving, ensure the config dir exists... */
	path = g_build_filename(g_get_user_config_dir(), "mirrorisland", NULL);
	if (!g_file_test(path, G_FILE_TEST_IS_DIR))
		g_mkdir_with_parents(path, 0700);
	g_free(path);

	/* ...now save the config file. */
	path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-settings.ini", NULL);
	g_key_file_save_to_file(keyfile, path, &err);
	g_free(path);

	g_key_file_free(keyfile);
}

static void thoth_load_config()
{
	GError *err = NULL;
	gchar *str;
	gint width, height;
	gboolean state;
	GdkRGBA rgba;

	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-settings.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	g_free(path);
	if (!loadstate) {
		return; /* Can't load config - nothing more to do. */
	}

	/* Show/hide side panel and restore positions for paned handles. */
	state = g_key_file_get_boolean(keyfile, "Settings", "SidePanelVisible", &err);
	gtk_toggle_tool_button_set_active(
			GTK_TOGGLE_TOOL_BUTTON(thoth_sidepaneltoolitem), state);
	gtk_widget_set_visible(thoth_sidepanelframe, state);
	thoth_sidepanelvisible = state;
	gint editorpanedpos = g_key_file_get_integer(keyfile, "Settings",
			"EditorPanedPosition", &err);
	if (editorpanedpos) {
		gtk_paned_set_position(GTK_PANED(thoth_editorpaned), editorpanedpos);
	}

	/* Setup size and position of main window. */
	width = g_key_file_get_integer(keyfile, "Settings", "WindowWidth", &err);
	height = g_key_file_get_integer(keyfile, "Settings", "WindowHeight", &err);
	if ((width > 1) && (height > 1)) {
		gtk_window_resize(GTK_WINDOW(thoth_window), width, height);
		thoth_width = width;
		thoth_height = height;
	}
	thoth_left = g_key_file_get_integer(keyfile, "Settings", "WindowLeft", &err);
	thoth_top = g_key_file_get_integer(keyfile, "Settings", "WindowTop", &err);
	gtk_window_move(GTK_WINDOW(thoth_window), thoth_left, thoth_top);

	/* Restore options from here on. */

	state = g_key_file_get_boolean(keyfile, "Settings", "OptionsWrapLines", &err);
	gtk_check_menu_item_set_active(
			GTK_CHECK_MENU_ITEM(thoth_menuitem[OPTIONS_WRAP_LINES]), state);
	thoth_wraplines = state;

	state = g_key_file_get_boolean(keyfile, "Settings", "OptionsShowLineNumbers", &err);
	gtk_check_menu_item_set_active(
			GTK_CHECK_MENU_ITEM(thoth_menuitem[OPTIONS_SHOW_LINE_NUMBERS]), state);
	thoth_showlinenumbers = state;

	/* Autoindent */
	state = g_key_file_get_boolean(keyfile, "Settings", "OptionsAutoindent", &err);
	if (err) {
		state = FALSE;
		g_error_free(err);
		err = NULL;
	} else {
		gtk_check_menu_item_set_active(
				GTK_CHECK_MENU_ITEM(thoth_menuitem[OPTIONS_AUTOINDENT]), state);
	}
	thoth_autoindent = state;

	/* Keep Thoth window above others. */
	state = g_key_file_get_boolean(keyfile, "Settings", "OptionsKeepAbove", &err);
	gtk_check_menu_item_set_active(
			GTK_CHECK_MENU_ITEM(thoth_menuitem[OPTIONS_KEEP_ABOVE]),
			state);
	thoth_keepabove = state;
	gtk_window_set_keep_above(GTK_WINDOW(thoth_window), thoth_keepabove);

	/* Reopen files on Startup */
	state = g_key_file_get_boolean(keyfile, "Settings", "OptionsReopenFilesOnStartup", &err);
	gtk_check_menu_item_set_active(
			GTK_CHECK_MENU_ITEM(thoth_menuitem[OPTIONS_REOPEN_FILES_ON_STARTUP]),
			state);
	thoth_reopenfilesonstartup = state;

	/* Set editor font, text colour, and background colour. */
	str = g_key_file_get_string(keyfile, "Settings", "FontName", &err);
	if (str) {
		/* Filter out any invalid fonts or sizes */
		PangoFontDescription *font_description;
		font_description = pango_font_description_from_string(str);

		gint fontsize = pango_font_description_get_size(font_description) / PANGO_SCALE;
		if (fontsize < MIN_FONT_SIZE) {
			fontsize = MIN_FONT_SIZE;
		} else if (fontsize > MAX_FONT_SIZE) {
			fontsize = MAX_FONT_SIZE;
		}
		pango_font_description_set_size(font_description,
				fontsize * PANGO_SCALE);

		thoth_textview_font = pango_font_description_to_string(font_description);
		pango_font_description_free(font_description);
	}

	str = g_key_file_get_string(keyfile, "Settings", "TextColour", &err);
	if (str) {
		if (gdk_rgba_parse(&rgba, str)) {
			thoth_textview_fg_rgba = rgba;
		} else {
			thoth_textview_fg_rgba = (GdkRGBA) {0.5, 0, 0, 1};
		}
	}
	thoth_set_textview_foreground_colour(&thoth_textview_fg_rgba);

	str = g_key_file_get_string(keyfile, "Settings", "BackgroundColour", &err);
	if (str) {
		if (gdk_rgba_parse(&rgba, str)) {
			thoth_textview_bg_rgba = rgba;
		} else {
			thoth_textview_bg_rgba = (GdkRGBA) {1, 0.9, 0.7, 1};
		}
	}
	thoth_set_textview_background_colour(&thoth_textview_bg_rgba);

	gint recentfilemax = g_key_file_get_integer(keyfile, "Settings", "RecentFilesMax", &err);
	if (recentfilemax)
		thoth_recentfilemax = recentfilemax;

	gint tabsize = g_key_file_get_integer(keyfile, "Settings", "TabSize", &err);
	if (tabsize)
		thoth_tabsize = tabsize;

	gint closebuttonaction = g_key_file_get_integer(keyfile, "Settings", "CloseButtonAction", &err);
	if (closebuttonaction)
		thoth_closebuttonaction = closebuttonaction;

	g_key_file_free(keyfile);
}

static GtkWidget* thoth_add_recent_menu_item(gchar *uri, gboolean top)
{
	GtkWidget *menuitem = gtk_menu_item_new_with_label(uri);
	gtk_widget_show(menuitem);
	gtk_menu_shell_append(GTK_MENU_SHELL(thoth_filesmenu), menuitem);
	g_signal_connect(G_OBJECT(menuitem), "activate",
			G_CALLBACK(thoth_callback_open_recent), NULL);

	if (top) {
		gtk_menu_reorder_child(GTK_MENU(thoth_filesmenu), menuitem, RECENT_MENU_INSERT);
	}

	return menuitem;
}

static void thoth_add_recent_uri(gchar *uri, gboolean top)
{
	thoth_remove_recent_uri(uri);

	/* Add to top of recent queue and menu item list. */
	ThothRecentStruct *recent = g_malloc(sizeof(ThothRecentStruct));
	GtkWidget *menuitem = thoth_add_recent_menu_item(uri, top);
	recent->uri = g_strdup(uri);
	recent->menuitem = menuitem;
	if (top) {
		thoth_recentfilelist = g_list_prepend(thoth_recentfilelist, recent);
	} else {
		thoth_recentfilelist = g_list_append(thoth_recentfilelist, recent);
	}
}

/**
 * Save/Load open files and locations on exit/startup.
 * location is optionally used for exporting uri lists.
 */
static void thoth_save_uris(gchar *location)
{
	GError *err = NULL;
	GKeyFile *keyfile = g_key_file_new();
	gint i, len;
	GtkTreeIter iter;
	gboolean valid;

	/* Save Recent URIs from the file menu. */
	i = 0;
	len = g_list_length(thoth_recentfilelist);
	gchar **recenturis = g_new0(gchar*, len + 1);
	GList *list = thoth_recentfilelist;
	while (list) {
		recenturis[i] = g_strdup(((ThothRecentStruct *)list->data)->uri);
		list = list->next;
		i++;
	}
	recenturis[i] = NULL;
	g_key_file_set_string_list(keyfile, "RecentURIs", "RecentURIList",
			(const gchar**)recenturis, len);
	g_strfreev(recenturis);

	/* All open files are listed in FileURIList - FileURI points to displayed file. */
	if (thoth_editor->uri) {
		g_key_file_set_string(keyfile, "FileURIs", "FileURI", thoth_editor->uri);
	}

	/* Save web-based pages, if any. */
	i = 0;
	len = gtk_tree_model_iter_n_children(thoth_treemodel, &thoth_pagesiter);
	gchar **pageuris = g_new0(gchar*, len + 1);
	valid = gtk_tree_model_iter_children(thoth_treemodel, &iter, &thoth_pagesiter);
	while (valid) {
		gchar *uri;
		gint id;
		gtk_tree_model_get(thoth_treemodel, &iter,
				SPL_COL_URI, &uri, SPL_COL_ID, &id, -1);

		GList *list = thoth_editorlist_find_id(id);
		ThothEditorStruct *editor = (ThothEditorStruct *)list->data;
		gint64 accesstime = editor->accesstime;

		pageuris[i] = g_strdup_printf("%ld::%s", accesstime, uri);
		g_free(uri);

		valid = gtk_tree_model_iter_next(thoth_treemodel, &iter);
		i++;
	}
	pageuris[i] = NULL;
	g_key_file_set_string_list(keyfile, "PageURIs", "PageURIList",
			(const gchar**)pageuris, len);
	g_strfreev(pageuris);

	/* Save notes, if any. */
	i = 0;
	len = gtk_tree_model_iter_n_children(thoth_treemodel, &thoth_notesiter);
	gchar **noteuris = g_new0(gchar*, len + 1);
	valid = gtk_tree_model_iter_children(thoth_treemodel, &iter, &thoth_notesiter);
	while (valid) {
		gchar *uri;
		gint id;
		gtk_tree_model_get(thoth_treemodel, &iter,
				SPL_COL_URI, &uri, SPL_COL_ID, &id, -1);

		GList *list = thoth_editorlist_find_id(id);
		ThothEditorStruct *editor = (ThothEditorStruct *)list->data;
		gint64 accesstime = editor->accesstime;

		noteuris[i] = g_strdup_printf("%ld::%s", accesstime, uri);
		g_free(uri);

		valid = gtk_tree_model_iter_next(thoth_treemodel, &iter);
		i++;
	}
	noteuris[i] = NULL;
	g_key_file_set_string_list(keyfile, "NoteURIs", "NoteURIList",
			(const gchar**)noteuris, len);
	g_strfreev(noteuris);

	/* Save currently open URIs, if requested. */
	i = 0;
	len = gtk_tree_model_iter_n_children(thoth_treemodel, &thoth_openfilesiter);
	gchar **fileuris = g_new0(gchar*, len + 1);
	valid = gtk_tree_model_iter_children(thoth_treemodel, &iter, &thoth_openfilesiter);
	while (valid) {
		gchar *uri;
		gint id;
		gtk_tree_model_get(thoth_treemodel, &iter,
				SPL_COL_URI, &uri, SPL_COL_ID, &id, -1);

		GList *list = thoth_editorlist_find_id(id);
		ThothEditorStruct *editor = (ThothEditorStruct *)list->data;
		gint64 accesstime = editor->accesstime;

		fileuris[i] = g_strdup_printf("%ld::%s", accesstime, uri);
		g_free(uri);

		valid = gtk_tree_model_iter_next(thoth_treemodel, &iter);
		i++;
	}
	fileuris[i] = NULL;
	g_key_file_set_string_list(keyfile, "FileURIs", "FileURIList",
			(const gchar**)fileuris, len);
	g_strfreev(fileuris);

	/* ...now save the URIs file. */
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-uris.ini", NULL);
	g_key_file_save_to_file(keyfile, path, &err);
	g_free(path);

	g_key_file_free(keyfile);
}

/**
 * Show/hide the sidepanel and update toggle button.
 */
static void thoth_command_sidepanel()
{
	gboolean state = gtk_toggle_tool_button_get_active(
			GTK_TOGGLE_TOOL_BUTTON(thoth_sidepaneltoolitem));

	gtk_widget_set_visible(thoth_sidepanelframe, state);
	thoth_sidepanelvisible = state;
}

static void thoth_command_file_open()
{
	GtkWidget *dialog = gtk_file_chooser_dialog_new("Open text file",
			GTK_WINDOW(thoth_window), GTK_FILE_CHOOSER_ACTION_OPEN,
			"_Cancel", GTK_RESPONSE_CANCEL,
			"_Open", GTK_RESPONSE_ACCEPT, NULL);

	/* Ask for a valid file to open. */
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		gchar *uri = thoth_uri_from_filename(filename);

		/* Show file if already opened, otherwise try opening it. */
		gint editorid = thoth_open_file(uri, THOTH_FILE, TRUE, FALSE);

		if (editorid) {
			thoth_add_recent_uri(uri, TRUE);
			thoth_prune_recent_uri_list();
			thoth_add_sidepanel_item(&thoth_openfilesiter, THOTH_FILE, editorid,
					uri, filename, TRUE);
			thoth_set_current_uri(TRUE);
			thoth_save_uris(NULL);
		}
		g_free(uri);
		g_free(filename);
	}

	gtk_widget_destroy(dialog);
}

static void thoth_command_file_open_web_address()
{
	gchar *uri = thoth_web_address_dialog(THOTH_WEB_OPEN, NULL);
	if (!uri)
		return;

	gint editorid = thoth_open_file(uri, THOTH_PAGE, TRUE, FALSE);
	if (editorid) {
		thoth_add_sidepanel_item(&thoth_pagesiter, THOTH_PAGE,
				editorid, uri, uri, TRUE);
		thoth_set_current_uri(TRUE);
	}

	g_free(uri);
}

/**
 * Move a note or file to the page category (if needed).
 */
static void thoth_command_file_save_to_web_finalise(gchar *uri)
{
	/* Remove note attributes, including sidepanel entry. */
	/* Parts of this code is copied from thoth_close_editor. Merge? */
	GtkTreeIter *treeiter = thoth_treemodel_iter_find_id(
			thoth_treemodel, thoth_editor->id);
	if (treeiter) {
		gtk_tree_store_remove(GTK_TREE_STORE(thoth_treemodel), treeiter);
	}

	g_free(thoth_editor->title);
	thoth_editor->title = g_strdup(uri);
	thoth_editor->category = THOTH_PAGE;
	thoth_editor->uri = g_strdup(uri);

	/* The following line updates if already open, otherwise it adds. */
	thoth_add_sidepanel_item(&thoth_pagesiter, THOTH_PAGE,
			thoth_editor->id, uri, uri, TRUE);
	thoth_set_current_uri(TRUE);
}

/**
 * Show the web location popup, post the data, and retrieve the URI.
 * - dest_uri points to an existing URI, or NULL if uploading new post.
 * Sometimes the returned URI is different, such as when saving a new post
 * instead of updating an existing one.
 */
static void thoth_command_file_save_to_web(gchar *dest_uri)
{
	gchar *uri = g_strdup(dest_uri);

	gboolean load_password = (dest_uri) ? TRUE : FALSE;

	gchar *ret_uri = NULL;
	while (ret_uri == NULL) {
		if (load_password) {
			thoth_load_password(uri); /* Fill login details in thoth_editor. */
			load_password = FALSE;
		}

		if (!uri) {
			uri = thoth_web_address_dialog(THOTH_WEB_SAVE, dest_uri);
			char ch = uri[strlen(uri) - 1];
			if (ch == '/') {
				/* Show FTP browser */
				gchar *new_uri = thoth_show_ftp_browser(uri);
				if (!new_uri)
					return;

				g_free(uri);
				uri = g_strdup(new_uri);
				g_free(new_uri);
			}
		}

		if (!uri)
			return; /* Cancel pressed. No need to continue. */

		if (g_strcmp0(thoth_protocol, "ftps") == 0) {
			thoth_save_file(thoth_editor, thoth_editor->filepath);
			ret_uri = thoth_web_address_ftp(THOTH_WEB_SAVE, uri,
					thoth_username, thoth_password,
					thoth_editor->filepath, thoth_editor->unixpermissions);
			if (ret_uri && ((thoth_editor->category == THOTH_PAGE)
						|| (thoth_editor->category == THOTH_NOTE))) {
				/* Move existing file if successful. */
				if (g_strcmp0(thoth_editor->uri, uri) != 0) {
					gchar *filepath = thoth_web_address_to_local_page_file(uri);
					g_rename(thoth_editor->filepath, filepath);
					g_free(thoth_editor->filepath);
					thoth_editor->filepath = filepath;
				}
			}
		} else if (g_strcmp0(thoth_protocol, "http") == 0) {
			thoth_save_file(thoth_editor, thoth_editor->filepath);
			GtkTextIter start_iter, end_iter;
			gtk_text_buffer_get_bounds(thoth_editor->textbuffer, &start_iter, &end_iter);
			gchar *description = gtk_text_buffer_get_text(thoth_editor->textbuffer,
					&start_iter, &end_iter, FALSE);
			ret_uri = thoth_web_address_post(THOTH_WEB_SAVE, uri,
					thoth_protocol, thoth_username, thoth_password,
					description);
			g_free(description);
		}
		g_free(uri);
		uri = NULL;
	}

	/* Page saved. Complete saving process from here on. */
	if (thoth_savepassword) {
		thoth_save_passwords(thoth_editor->uri, thoth_protocol,
				thoth_username, thoth_password,
				thoth_editor->base64iv, TRUE);
		thoth_savepassword = FALSE;
	}

	// Commented, can't identify HTTP: thoth_add_recent_uri(dest_uri, TRUE);
	// Comment this too: thoth_prune_recent_uri_list();

	if (thoth_editor->category == THOTH_NOTE) {
		thoth_command_file_save_to_web_finalise(ret_uri);
	}

	gtk_text_buffer_set_modified(thoth_editor->textbuffer, FALSE);
	thoth_callback_modified_changed(thoth_editor->textbuffer, NULL);

	g_free(ret_uri);
}

static void thoth_command_file_reload()
{
	if (thoth_editor->modified) {
		gint confirm = thoth_confirm_dialog("Reload and lose all changes?",
				"A file has been modified.\n\n"
				"Do you want to go back to save the file,"
				" or reload it to undo changes?",
				"Go _Back", "_Reload File");

		if (confirm != GTK_RESPONSE_ACCEPT)
			return; /* Don't exit yet. */
	} else {
		gint confirm = thoth_confirm_dialog("Reload file?",
				"Do you want to reload the file?",
				"Go _Back", "_Reload File");

		if (confirm != GTK_RESPONSE_ACCEPT)
			return; /* Don't exit yet. */
	}

	if (thoth_open_file(thoth_editor->uri, thoth_editor->category, FALSE, TRUE) == 0) {
		GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(thoth_window),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
				"[%d] Error reloading \"%s\"", __LINE__, thoth_editor->uri);
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
	}
}

/**
 * Save file on a local device. Takes local filename, not URI.
 */
static gboolean thoth_save_file(ThothEditorStruct *editor, gchar *filename)
{
	gchar *file_text;
	GtkTextIter start_iter, end_iter;
	GError *err = NULL;

	/* Save file contents first... */
	gtk_text_buffer_get_bounds(editor->textbuffer, &start_iter, &end_iter);
	file_text = gtk_text_buffer_get_text(editor->textbuffer, &start_iter, &end_iter, FALSE);
	if (!g_file_set_contents(filename, file_text, -1, &err)) {
		printf("%s:%4d.%4d %s\n", __FILE__, __LINE__, dbg++, err->message);
		g_error_free(err);
		return FALSE;
	}
	gtk_text_buffer_set_modified(editor->textbuffer, FALSE);
	thoth_callback_modified_changed(editor->textbuffer, NULL);

	/* ...then set saved permissions */
	if (thoth_editor->category == THOTH_FILE) {
		g_chmod(thoth_editor->filepath, thoth_editor->unixpermissions);
	}

	return TRUE;
}

/**
 * Called by thoth_command_file_save_as() below.
 */
static void thoth_command_file_save_as_finalise(gchar *filename)
{
	gchar *uri = thoth_uri_from_filename(filename);

	if ((thoth_editor->category == THOTH_PAGE)
			|| (thoth_editor->category == THOTH_NOTE)) {
		/* Remove note attributes, including sidepanel entry. */
		/* Parts of this code is copied from thoth_close_editor. Merge? */
		GtkTreeIter *treeiter = thoth_treemodel_iter_find_id(
				thoth_treemodel, thoth_editor->id);
		if (treeiter) {
			gtk_tree_store_remove(GTK_TREE_STORE(thoth_treemodel), treeiter);
		}

		/* Web pages and Note files are immediately deleted. */
		g_remove(thoth_editor->filepath);

		g_free(thoth_editor->title);
		g_free(thoth_editor->uri);
		g_free(thoth_editor->filepath);
		g_free(thoth_editor->protocol);

		thoth_editor->title = g_path_get_basename(filename);
		thoth_editor->category = THOTH_FILE;
		thoth_editor->uri = g_strdup(uri);
		thoth_editor->filepath = thoth_filename_from_uri(uri);
		thoth_editor->protocol = NULL;
	} else {
		/* Make a copy if original was a web page or file */
		thoth_open_file(uri, THOTH_FILE, TRUE, FALSE);
	}
	thoth_add_recent_uri(uri, TRUE);
	thoth_prune_recent_uri_list();

	/* The following line updates if already open, otherwise it adds. */
	thoth_add_sidepanel_item(&thoth_openfilesiter, THOTH_FILE,
			thoth_editor->id, uri, filename, TRUE);
	thoth_set_current_uri(TRUE);
	g_free(uri);
}

static void thoth_command_file_save_as()
{
	GtkWidget *dialog = gtk_file_chooser_dialog_new("Save file as...",
			GTK_WINDOW(thoth_window), GTK_FILE_CHOOSER_ACTION_SAVE,
			"_Cancel", GTK_RESPONSE_CANCEL,
			"_Save", GTK_RESPONSE_ACCEPT, NULL);
	gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
	/* Point to existing file, if set. */
	if (thoth_editor->uri) {
		gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(dialog), thoth_editor->uri);
	}

	/* Ask for a valid file to save. */
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));

		if (thoth_save_file(thoth_editor, filename)) {
			thoth_command_file_save_as_finalise(filename);
		}
		g_free(filename);
	}

	gtk_widget_destroy(dialog);
}

static void thoth_command_file_new()
{
	thoth_editor = thoth_new_window_add_editor(THOTH_NOTE);
	thoth_init_undo(thoth_editor);
	thoth_callback_blockuseraction(thoth_editor->textbuffer);
	gtk_text_buffer_set_text(thoth_editor->textbuffer, "", 0);
	thoth_callback_unblockuseraction(thoth_editor->textbuffer);
	gtk_text_buffer_set_modified(thoth_editor->textbuffer, FALSE);

	gint64 getrealtime = g_get_real_time();
	while (getrealtime == thoth_lastgetrealtime) {
		getrealtime++;
	}
	thoth_lastgetrealtime = getrealtime;
	GString *newidstring = g_string_new(NULL);
	g_string_printf(newidstring, "%x", getrealtime);
	gchar *newid = g_string_free(newidstring, FALSE);
	gchar *filepath = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"notes", newid, NULL);
	gchar *uri = thoth_uri_from_filename(filepath);

	/* Before saving, ensure the config dir exists for notes... */
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland", "notes", NULL);
	if (!g_file_test(path, G_FILE_TEST_IS_DIR))
		g_mkdir_with_parents(path, 0700);
	g_free(path);

	thoth_save_file(thoth_editor, filepath);
	thoth_editor->uri = g_strdup(uri);
	thoth_editor->filepath = filepath;
	gchar *tooltip = thoth_get_snippet(255, FALSE);
	thoth_add_sidepanel_item(&thoth_notesiter, THOTH_NOTE, thoth_editor->id,
			uri, tooltip, TRUE);
	g_free(tooltip);
	thoth_set_current_uri(FALSE);
	thoth_save_uris(NULL);

	g_free(newid);
	g_free(uri);
}

static void thoth_command_file_save()
{
	if (thoth_editor->uri == NULL) {
		thoth_command_file_save_as();
	} else if (thoth_editor->category == THOTH_PAGE) {
		thoth_command_file_save_to_web(thoth_editor->uri);
	} else {
		thoth_save_file(thoth_editor, thoth_editor->filepath);
	}
}

/**
 * Handle print operations.
 */
static void thoth_print_begin_print(GtkPrintOperation *operation,
		GtkPrintContext *context, gpointer user_data)
{
	GtkTextIter start_iter, end_iter;
	GtkTextBuffer *buffer = thoth_editor->textbuffer;
	PangoFontDescription *font_description;

	gtk_text_buffer_get_bounds(buffer, &start_iter, &end_iter);
	gchar *text = g_strchomp(gtk_text_buffer_get_text(buffer,
			&start_iter, &end_iter, FALSE));

	thoth_print_page_width = gtk_print_context_get_width(context);
	thoth_print_page_height = gtk_print_context_get_height(context);
	font_description = pango_font_description_from_string(thoth_textview_font);
	thoth_print_layout = gtk_print_context_create_pango_layout(context);
	pango_layout_set_width(thoth_print_layout, thoth_print_page_width * PANGO_SCALE);
	pango_layout_set_font_description(thoth_print_layout, font_description);
	pango_layout_set_text(thoth_print_layout, text, -1);

	/* Set tabs sizes for printing */
	PangoContext *ctx = gtk_widget_get_pango_context(thoth_editor->textview);
	PangoFontMetrics *metrics = pango_context_get_metrics(ctx, NULL, NULL);
	gint width = pango_font_metrics_get_approximate_char_width(metrics);
	PangoTabArray *tabarray = pango_tab_array_new_with_positions(1, FALSE,
			PANGO_TAB_LEFT, width*thoth_tabsize);
	pango_layout_set_tabs(thoth_print_layout, tabarray);
	pango_tab_array_free(tabarray);
	pango_font_metrics_unref(metrics);

	thoth_print_line_count = pango_layout_get_line_count(thoth_print_layout);
	thoth_print_text_height = pango_font_description_get_size(font_description) / PANGO_SCALE;
	thoth_print_lines_per_page = thoth_print_page_height / thoth_print_text_height;

	thoth_print_n_pages = (thoth_print_line_count - 1) / thoth_print_lines_per_page + 1;
	gtk_print_operation_set_n_pages(operation, thoth_print_n_pages);
}

static void thoth_print_draw_page(GtkPrintOperation *operation,
		GtkPrintContext *context, gint page_number, gpointer user_data)
{
	PangoFontDescription *font_description;
	font_description = pango_font_description_from_string(thoth_textview_font);

	cairo_t *cr = gtk_print_context_get_cairo_context(context);

	/* Draw print page headers */
	gint layout_width, layout_height;
	gchar *title = NULL;
	gchar *snippet = NULL;
	switch (thoth_editor->category) {
	case THOTH_NOTE:
		if (g_utf8_validate(thoth_editor->title, -1, NULL)) {
			snippet = g_malloc0(41);
			g_utf8_strncpy(snippet, thoth_editor->title, 40);
		} else {
			snippet = g_strndup(thoth_editor->title, 40);
		}
		title = g_strdup_printf("(Note) %s...", snippet);
		g_free(snippet);
		break;

	case THOTH_PAGE:
		title = g_strdup(thoth_editor->uri);
		break;

	case THOTH_FILE:
		title = g_strdup(thoth_editor->filepath);
	}

	if (title) {
		PangoLayout *title_layout = gtk_print_context_create_pango_layout(context);
		pango_layout_set_font_description(title_layout, font_description);
		pango_layout_set_text(title_layout, title, -1);
		cairo_move_to(cr, 0, -72 / 25.4 * 10);
		pango_cairo_show_layout(cr, title_layout);
		g_free(title);
	}

	gchar *text = g_strdup_printf("%d / %d", page_number + 1, thoth_print_n_pages);
	PangoLayout *num_layout = gtk_print_context_create_pango_layout(context);
	pango_layout_set_font_description(num_layout, font_description);
	pango_layout_set_text(num_layout, text, -1);
	pango_layout_get_size(num_layout, &layout_width, &layout_height);
	cairo_move_to(cr, thoth_print_page_width - layout_width / PANGO_SCALE,
			-72 / 25.4 * 10);
	pango_cairo_show_layout(cr, num_layout);
	g_free(text);

	cairo_move_to(cr, 0, -25.4 + layout_height / PANGO_SCALE);
	cairo_line_to(cr, thoth_print_page_width, -25.4 + layout_height / PANGO_SCALE);
	cairo_set_line_width(cr, 0.1);
	cairo_stroke(cr);

	/* Calculate and draw lines of text on this page */
	gint n_line, i, ct = 0;

	if (thoth_print_line_count > thoth_print_lines_per_page * (page_number + 1)) {
		n_line = thoth_print_lines_per_page * (page_number + 1);
	} else {
		n_line = thoth_print_line_count;
	}

	for (i = thoth_print_lines_per_page * page_number; i < n_line; i++) {
		PangoLayoutLine *line = pango_layout_get_line(thoth_print_layout, i);
		cairo_move_to(cr, 0, thoth_print_text_height * (ct + 1));
		pango_cairo_show_layout_line(cr, line);
		ct++;
	}

	pango_font_description_free(font_description);
}

static void thoth_print_end_print(GtkPrintOperation *operation,
		GtkPrintContext *context, gpointer user_data)
{
	g_object_unref(thoth_print_layout);
}

static GtkPrintOperation *thoth_create_print_operation()
{
	GtkPrintOperation *print = gtk_print_operation_new();

	if (thoth_printsettings != NULL) {
		gtk_print_operation_set_print_settings(print, thoth_printsettings);
	}

	/* TODO: Make print margins user-configurable */
	static GtkPageSetup *page_setup = NULL;
	page_setup = gtk_page_setup_new();
	gtk_page_setup_set_top_margin(page_setup, 20.0, GTK_UNIT_MM);
	gtk_page_setup_set_bottom_margin(page_setup, 15.0, GTK_UNIT_MM);
	gtk_page_setup_set_left_margin(page_setup, 15.0, GTK_UNIT_MM);
	gtk_page_setup_set_right_margin(page_setup, 15.0, GTK_UNIT_MM);
	gtk_print_operation_set_default_page_setup(print, page_setup);

	g_signal_connect(print, "begin_print", G_CALLBACK(thoth_print_begin_print), NULL);
	g_signal_connect(print, "draw_page", G_CALLBACK(thoth_print_draw_page), NULL);
	g_signal_connect(print, "end_print", G_CALLBACK(thoth_print_end_print), NULL);

	return print;
}

static void thoth_command_file_print()
{
	GtkPrintOperation *print = thoth_create_print_operation();
	GError *err = NULL;
	GtkPrintOperationResult res = gtk_print_operation_run(print,
			GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG,
			GTK_WINDOW(thoth_window), &err);

	if (res == GTK_PRINT_OPERATION_RESULT_APPLY) {
		if (thoth_printsettings)
			g_object_unref(thoth_printsettings);

		thoth_printsettings = g_object_ref(
				gtk_print_operation_get_print_settings(print));
	} else if (res == GTK_PRINT_OPERATION_RESULT_ERROR) {
		GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(thoth_window),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
				err->message);
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		g_error_free(err);
	}

	g_object_unref(print);
}

static void thoth_command_file_print_preview()
{
	GtkPrintOperation *print = thoth_create_print_operation();
	GError *err = NULL;
	GtkPrintOperationResult res = gtk_print_operation_run(print,
			GTK_PRINT_OPERATION_ACTION_PREVIEW,
			GTK_WINDOW(thoth_window), &err);

	if (res == GTK_PRINT_OPERATION_RESULT_ERROR) {
		GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(thoth_window),
				GTK_DIALOG_DESTROY_WITH_PARENT,
				GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
				err->message);
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		g_error_free(err);
	}

	g_object_unref(print);
}

/**
 *
 */
static void thoth_filepermissions_setcheckboxes(gint permissions)
{
	thoth_permissionsentrychanging = TRUE; /* Prevent checkbox-entrybox-checkbox update loop */
	GList *listitem = thoth_permissionscheckboxlist;
	while (listitem != NULL) {
		GtkWidget *checkbox = GTK_WIDGET(listitem->data);
		const gchar *name = gtk_widget_get_name(checkbox);

		if (g_strcmp0(name, "or") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_OWNERR);
		} else if (g_strcmp0(name, "ow") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_OWNERW);
		} else if (g_strcmp0(name, "ox") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_OWNERX);
		} else if (g_strcmp0(name, "gr") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_GROUPR);
		} else if (g_strcmp0(name, "gw") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_GROUPW);
		} else if (g_strcmp0(name, "gx") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_GROUPX);
		} else if (g_strcmp0(name, "wr") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_WORLDR);
		} else if (g_strcmp0(name, "ww") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_WORLDW);
		} else if (g_strcmp0(name, "wx") == 0) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), permissions & THOTH_WORLDX);
		}

		listitem = listitem->next;
	}
	thoth_permissionsentrychanging = FALSE;
}

/**
 *
 */
static void thoth_filepermissionsentrychanged(GtkEntry *entry, gchar *string,
		gpointer data)
{
	if (thoth_permissionsentrychanging)
		return; /* Something is already changing this for us */

	const gchar *entrytext = gtk_entry_get_text(GTK_ENTRY(entry));

	gint permissions = 0;
	GMatchInfo *matchinfo;
	GRegex *listingregex = g_regex_new("([0-7][0-7][0-7])",
			0, 0, NULL);
	g_regex_match(listingregex, entrytext, 0, &matchinfo);
	if (g_match_info_matches(matchinfo)) {
		gchar *matchstr = g_match_info_fetch(matchinfo, 0);
		gint matchint = g_ascii_strtoll(matchstr, 0, 8); /* Base-8, octal */

		thoth_permissionsentrychanging = TRUE;
		thoth_filepermissions_setcheckboxes(matchint);
		thoth_editor->unixpermissions = matchint;
		thoth_permissionsentrychanging = FALSE;

		g_free(matchstr);
	}

	g_match_info_free(matchinfo);
	g_regex_unref(listingregex);
}

static void thoth_filepermissionstoggle(GtkToggleButton *toggle,
		gpointer data)
{
	if (thoth_permissionsentrychanging)
		return; /* Prevent checkbox-entrybox-checkbox update loop */

	thoth_permissionsentrychanging = TRUE;
	int permissions = 0;
	GList *listitem = data;
	while (listitem != NULL) {
		GtkWidget *checkbox = GTK_WIDGET(listitem->data);
		const gchar *name = gtk_widget_get_name(checkbox);
		gboolean active = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbox));

		if ((g_strcmp0(name, "or") == 0) && active) {
			permissions |= (1 << 8); /* Owner Read */
		} else if ((g_strcmp0(name, "ow") == 0) && active) {
			permissions |= (1 << 7); /* Owner Write */
		} else if ((g_strcmp0(name, "ox") == 0) && active) {
			permissions |= (1 << 6); /* Owner Execute */
		} else if ((g_strcmp0(name, "gr") == 0) && active) {
			permissions |= (1 << 5); /* Group Read */
		} else if ((g_strcmp0(name, "gw") == 0) && active) {
			permissions |= (1 << 4); /* Group Write */
		} else if ((g_strcmp0(name, "gx") == 0) && active) {
			permissions |= (1 << 3); /* Group Execute */
		} else if ((g_strcmp0(name, "wr") == 0) && active) {
			permissions |= (1 << 2); /* World Read */
		} else if ((g_strcmp0(name, "ww") == 0) && active) {
			permissions |= (1 << 1); /* World Write */
		} else if ((g_strcmp0(name, "wx") == 0) && active) {
			permissions |= (1 << 0); /* World Execute */
		}

		listitem = listitem->next;
	}

	thoth_editor->unixpermissions = permissions;
	gchar *pstr = thoth_filepermissions_inttooctstr(permissions);
	gtk_entry_set_text(GTK_ENTRY(thoth_permissionsentry), pstr);
	g_free(pstr);

	thoth_permissionsentrychanging = FALSE;
}

static void thoth_command_file_properties()
{
	thoth_update_file_stats();

	GtkWidget *label, *entry, *separator;
	GtkWidget *properties_dialog = gtk_dialog_new_with_buttons(
			"File Properties",
			GTK_WINDOW(thoth_window), GTK_DIALOG_MODAL,
			"Cancel", GTK_RESPONSE_CANCEL,
			"Ok", GTK_RESPONSE_ACCEPT, NULL);
	//gtk_window_set_default_size(GTK_WINDOW(properties_dialog), 480, -1);
	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(properties_dialog));

	GtkWidget *grid = gtk_grid_new();
	gtk_grid_set_column_homogeneous(GTK_GRID(grid), FALSE);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 8);
	gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
	gtk_widget_set_margin_top(GTK_WIDGET(grid), 16);
	gtk_widget_set_margin_bottom(GTK_WIDGET(grid), 16);
	gtk_widget_set_margin_start(GTK_WIDGET(grid), 16);
	gtk_widget_set_margin_end(GTK_WIDGET(grid), 8);
	gtk_widget_show(grid);
	gtk_container_add(GTK_CONTAINER(content_area), grid);
	gtk_widget_set_hexpand(grid, TRUE);
	gtk_widget_set_vexpand(grid, TRUE);

	label = thoth_create_left_label("File name:");
	gtk_grid_attach(GTK_GRID(grid), label, 1, 1, 1, 1);
	GtkEntryBuffer *filenamebuffer = gtk_entry_buffer_new(NULL, -1);
	GtkWidget *filenameentry = gtk_entry_new_with_buffer(GTK_ENTRY_BUFFER(filenamebuffer));
	gtk_widget_set_hexpand(filenameentry, TRUE);
	gtk_widget_show(filenameentry);
	gtk_grid_attach(GTK_GRID(grid), filenameentry, 2, 1, 1, 1);

	label = thoth_create_left_label("Location:");
	gtk_grid_attach(GTK_GRID(grid), label, 1, 2, 1, 1);
	GtkWidget *locationlabel = thoth_create_left_label("./");
	gtk_label_set_selectable(GTK_LABEL(locationlabel), TRUE);
	gtk_grid_attach(GTK_GRID(grid), locationlabel, 2, 2, 1, 1);

	if ((thoth_editor->category == THOTH_FILE) || (thoth_editor->category == THOTH_NOTE)) {
		gtk_entry_set_text(GTK_ENTRY(filenameentry),
				g_path_get_basename(thoth_editor->filepath));
		gtk_label_set_text(GTK_LABEL(locationlabel),
				g_path_get_dirname(thoth_editor->filepath));

		if (thoth_editor->category == THOTH_NOTE) {
			g_object_set(G_OBJECT(filenameentry), "editable", FALSE, NULL);
			gtk_widget_set_sensitive(GTK_WIDGET(filenameentry), FALSE);
		}
	} else if (thoth_editor->category == THOTH_PAGE) {
		gtk_entry_set_text(GTK_ENTRY(filenameentry),
				g_path_get_basename(thoth_editor->uri));
		gtk_label_set_text(GTK_LABEL(locationlabel),
				g_path_get_dirname(thoth_editor->uri));
	}

	separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_widget_show(separator);
	gtk_grid_attach(GTK_GRID(grid), separator, 1, 3, 2, 1);

	label = thoth_create_left_label("Size:");
	gtk_grid_attach(GTK_GRID(grid), label, 1, 4, 1, 1);
	label = thoth_create_left_label("- bytes");
	gtk_grid_attach(GTK_GRID(grid), label, 2, 4, 1, 1);

	label = thoth_create_left_label("Count:");
	gtk_grid_attach(GTK_GRID(grid), label, 1, 5, 1, 1);
	label = thoth_create_left_label("- Characters, - Words, - Lines");
	gtk_grid_attach(GTK_GRID(grid), label, 2, 5, 1, 1);

	label = thoth_create_left_label("Owner:");
	gtk_grid_attach(GTK_GRID(grid), label, 1, 6, 1, 1);
	label = thoth_create_left_label("-");
	gtk_grid_attach(GTK_GRID(grid), label, 2, 6, 1, 1);

	label = thoth_create_left_label("Created:");
	gtk_grid_attach(GTK_GRID(grid), label, 1, 7, 1, 1);
	label = thoth_create_left_label("-");
	gtk_grid_attach(GTK_GRID(grid), label, 2, 7, 1, 1);

	separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_widget_show(separator);
	gtk_grid_attach(GTK_GRID(grid), separator, 1, 8, 2, 1);

	label = thoth_create_left_label("Permissions:");
	gtk_grid_attach(GTK_GRID(grid), label, 1, 9, 1, 1);

	GtkEntryBuffer *permissionsbuffer = gtk_entry_buffer_new(NULL, -1);
	thoth_permissionsentry = gtk_entry_new_with_buffer(GTK_ENTRY_BUFFER(permissionsbuffer));
	gtk_widget_set_hexpand(thoth_permissionsentry, TRUE);
	gtk_entry_set_max_length(GTK_ENTRY(thoth_permissionsentry), 3);
	gtk_widget_show(thoth_permissionsentry);
	gtk_grid_attach(GTK_GRID(grid), thoth_permissionsentry, 2, 9, 1, 1);
	g_object_set(G_OBJECT(thoth_permissionsentry),
			"input-purpose", GTK_INPUT_PURPOSE_NUMBER, NULL);

	/* Add the permissions grid */
	GtkWidget *permissiongrid = gtk_grid_new();
	gtk_grid_set_column_homogeneous(GTK_GRID(permissiongrid), FALSE);
	gtk_grid_set_column_spacing(GTK_GRID(permissiongrid), 8);
	gtk_grid_set_row_spacing(GTK_GRID(permissiongrid), 8);
	gtk_widget_show(permissiongrid);
	gtk_widget_set_hexpand(permissiongrid, TRUE);
	gtk_widget_set_vexpand(permissiongrid, TRUE);
	gtk_grid_attach(GTK_GRID(grid), permissiongrid, 2, 10, 1, 1);

	label = thoth_create_left_label("Read");
	gtk_grid_attach(GTK_GRID(permissiongrid), label, 2, 1, 1, 1);
	label = thoth_create_left_label("Write");
	gtk_grid_attach(GTK_GRID(permissiongrid), label, 3, 1, 1, 1);
	label = thoth_create_left_label("Exec");
	gtk_grid_attach(GTK_GRID(permissiongrid), label, 4, 1, 1, 1);

	label = thoth_create_left_label("Owner");
	gtk_grid_attach(GTK_GRID(permissiongrid), label, 1, 2, 1, 1);
	label = thoth_create_left_label("Group");
	gtk_grid_attach(GTK_GRID(permissiongrid), label, 1, 3, 1, 1);
	label = thoth_create_left_label("World");
	gtk_grid_attach(GTK_GRID(permissiongrid), label, 1, 4, 1, 1);

	GtkWidget *checkbox;
	thoth_permissionscheckboxlist = NULL;
	gint unixpermissions = thoth_editor->unixpermissions;

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "or"); /* Owner Read */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_OWNERR);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 2, 2, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "ow"); /* Owner Write */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_OWNERW);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 3, 2, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "ox"); /* Owner Execute */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_OWNERX);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 4, 2, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "gr"); /* Group Read */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_GROUPR);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 2, 3, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "gw"); /* Group Write */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_GROUPW);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 3, 3, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "gx"); /* Group Execute */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_GROUPX);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 4, 3, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "wr"); /* World Read */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_WORLDR);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 2, 4, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "ww"); /* World Write */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_WORLDW);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 3, 4, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	checkbox = gtk_check_button_new();
	gtk_widget_show(checkbox);
	gtk_widget_set_name(checkbox, "wx"); /* World Execute */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), unixpermissions & THOTH_WORLDX);
	gtk_grid_attach(GTK_GRID(permissiongrid), checkbox, 4, 4, 1, 1);
	thoth_permissionscheckboxlist = g_list_append(thoth_permissionscheckboxlist, checkbox);
	g_signal_connect(checkbox, "toggled", G_CALLBACK(thoth_filepermissionstoggle), thoth_permissionscheckboxlist);

	/* Set the permissions text entry according to the checkboxes,
	 * then attach signal handlers for that text entry's changes */
	thoth_filepermissionstoggle(NULL, thoth_permissionscheckboxlist);

	g_signal_connect_after(G_OBJECT(thoth_permissionsentry), "insert-text",
			G_CALLBACK(thoth_filepermissionsentrychanged), NULL);
	g_signal_connect_after(G_OBJECT(thoth_permissionsentry), "backspace",
			G_CALLBACK(thoth_filepermissionsentrychanged), NULL);
	g_signal_connect_after(G_OBJECT(thoth_permissionsentry), "key-press-event",
			G_CALLBACK(thoth_filepermissionsentrychanged), NULL);
	g_signal_connect_after(G_OBJECT(thoth_permissionsentry), "delete-from-cursor",
			G_CALLBACK(thoth_filepermissionsentrychanged), NULL);
	g_signal_connect_after(G_OBJECT(thoth_permissionsentry), "cut-clipboard",
			G_CALLBACK(thoth_filepermissionsentrychanged), NULL);
	g_signal_connect_after(G_OBJECT(thoth_permissionsentry), "paste-clipboard",
			G_CALLBACK(thoth_filepermissionsentrychanged), NULL);

	/* Now run the preferences dialog */
	gint response = gtk_dialog_run(GTK_DIALOG(properties_dialog));
	if (response == GTK_RESPONSE_ACCEPT) {
		if (thoth_editor->category == THOTH_FILE) {
			/* Modify local files */
			g_chmod(thoth_editor->filepath, thoth_editor->unixpermissions);
		} else if (thoth_editor->category == THOTH_PAGE) {
			thoth_editor->modified = TRUE; /* Mark modified to upload changes */
		}
	} else {
		/* Restore previous properties */
		thoth_editor->unixpermissions = unixpermissions;
	}
	g_list_free(thoth_permissionscheckboxlist);
	gtk_widget_destroy(properties_dialog);
}

static void thoth_command_options_wrap_lines()
{
	GtkWidget *menuitem = thoth_menuitem[OPTIONS_WRAP_LINES];

	thoth_wraplines = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem));
	if (thoth_editor)
		thoth_set_textview_wrap_mode(thoth_editor->textview, thoth_wraplines);
}

static void thoth_command_options_show_line_numbers()
{
	GtkWidget *menuitem = thoth_menuitem[OPTIONS_SHOW_LINE_NUMBERS];

	thoth_showlinenumbers = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem));
	if (thoth_editor)
		thoth_set_textview_line_numbers(thoth_editor->textview,
				thoth_showlinenumbers);
}

static void thoth_command_options_autoindent()
{
	GtkWidget *menuitem = thoth_menuitem[OPTIONS_AUTOINDENT];

	thoth_autoindent = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem));
}

static void thoth_command_options_keepabove()
{
	GtkWidget *menuitem = thoth_menuitem[OPTIONS_KEEP_ABOVE];

	thoth_keepabove = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem));
	gtk_window_set_keep_above(GTK_WINDOW(thoth_window), thoth_keepabove);
}

static void thoth_command_options_reopen_files_on_startup()
{
	GtkWidget *menuitem = thoth_menuitem[OPTIONS_REOPEN_FILES_ON_STARTUP];

	thoth_reopenfilesonstartup = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menuitem));
}

void thoth_callback_select_font(GtkWidget *widget, gpointer data)
{
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
	const gchar *font_name = gtk_font_button_get_font_name(GTK_FONT_BUTTON(widget));
G_GNUC_END_IGNORE_DEPRECATIONS
	thoth_set_textview_font(thoth_editor->textview, font_name);
	if (thoth_textview_font) {
		g_free(thoth_textview_font);
		thoth_textview_font = NULL;
	}
	thoth_textview_font = g_strdup(font_name);
}

void thoth_callback_select_foreground_colour(GtkWidget *widget, gpointer data)
{
	GdkRGBA rgba;

	gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(widget), &rgba);
	thoth_set_textview_foreground_colour(&rgba);
	thoth_textview_fg_rgba = rgba;
}

void thoth_callback_select_background_colour(GtkWidget *widget, gpointer data)
{
	GdkRGBA rgba;

	gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(widget), &rgba);
	thoth_set_textview_background_colour(&rgba);
	thoth_textview_bg_rgba = rgba;
}

void thoth_callback_recent_files_spin_button(GtkSpinButton *spin_button, gpointer data)
{
	thoth_recentfilemax = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spin_button));
}

void thoth_callback_tab_size_spin_button(GtkSpinButton *spin_button, gpointer data)
{
	thoth_tabsize = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spin_button));
	thoth_set_textview_tab_size(thoth_editor->textview, thoth_tabsize);
}

void thoth_callback_close_button_combobox(GtkComboBox *combo, gpointer data)
{
	thoth_closebuttonaction = gtk_combo_box_get_active(GTK_COMBO_BOX(combo));
}

/**
 * Reload password list dialog.
 * - show_passwords: TRUE show decoded passwords, FALSE hide with dots.
 */
static gboolean thoth_preferences_load_saved_logins(gboolean show_passwords)
{
	/* Load logins from file */
	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	if (!loadstate) {
		keyfile = g_key_file_new();
		g_error_free(err);
		err = NULL;
	}

	/* Get and check master password if showing passwords */
	if (show_passwords) {

		while (!thoth_master_password) {
			gchar *master_password_digest = g_key_file_get_string(keyfile,
					"master password", "digest", NULL);
			gchar *master_password_salt = g_key_file_get_string(keyfile,
					"master password", "salt", NULL);
			if ((master_password_digest == NULL) || (master_password_salt == NULL)) {
				g_key_file_free(keyfile);
				return FALSE;
			}

			thoth_master_password = thoth_master_password_dialog(FALSE, thoth_window);
			if (!thoth_master_password) {
				g_key_file_free(keyfile);
				g_free(master_password_digest);
				g_free(master_password_salt);
				return FALSE;
			}

			gchar *digest = thoth_get_password_digest(master_password_salt,
					thoth_master_password);
			if (g_strcmp0(digest, master_password_digest) != 0) {
				g_free(thoth_master_password);
				thoth_master_password = NULL; /* Clear password, try again. */
			}
			g_free(digest);
			g_free(master_password_digest);
			g_free(master_password_salt);
		}
		gtk_widget_grab_focus(thoth_preferences_dialog);
	}

	gtk_list_store_clear(thoth_passwordliststore);

	/* Add each login to the treeview */
	gsize groupct;
	gchar **groups = g_key_file_get_groups(keyfile, &groupct);
	int i;
	for (i = 0; i < groupct; i++) {
		if (g_strcmp0(groups[i], "master password") == 0)
			continue;

		gsize urilen;
		guchar *uri = g_base64_decode(groups[i], &urilen);
		gchar *protocol = g_key_file_get_string(keyfile, groups[i], "protocol", NULL);
		gchar *username = g_key_file_get_string(keyfile, groups[i], "username", NULL);
		gchar *password = g_key_file_get_string(keyfile, groups[i], "password", NULL);
		gchar *base64iv = g_key_file_get_string(keyfile, groups[i], "base64iv", NULL);

		/* Only show passwords if master password given */
		gchar *pw;
		if (show_passwords) {
			pw = thoth_decrypt(password, thoth_master_password, base64iv);
		} else {
			pw = g_strdup("∙∙∙∙∙∙∙∙∙");
		}

		GtkTreeIter iter;
		gtk_list_store_append(thoth_passwordliststore, &iter);
		gtk_list_store_set(thoth_passwordliststore, &iter,
				PWD_COL_PAGEID, groups[i],
				PWD_COL_URI, uri, PWD_COL_PROTOCOL, protocol,
				PWD_COL_USERNAME, username, PWD_COL_PASSWORD, pw, -1);

		g_free(pw);
		g_free(uri);
		g_free(protocol);
		g_free(username);
		g_free(base64iv);
	}
}

/**
 * Only load password list on page load.
 */
static gboolean thoth_preferences_switch_page(GtkNotebook *notebook, GtkWidget *page,
		guint page_num, gpointer data)
{
	if (page_num == 1) {
		thoth_preferences_load_saved_logins(FALSE);
	}

	return FALSE;
}

static void thoth_passwordtreeviewtoggle(GtkCellRendererToggle *toggle,
		gchar *path_string, gpointer data)
{
	gboolean toggled;
	GtkTreeIter iter;

	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
	gtk_tree_model_get_iter(GTK_TREE_MODEL(thoth_passwordliststore), &iter, path);
	gtk_tree_model_get(GTK_TREE_MODEL(thoth_passwordliststore), &iter,
			PWD_COL_CHECKBOX, &toggled, -1);

	toggled = !toggled;
	gtk_list_store_set(thoth_passwordliststore, &iter,
			PWD_COL_CHECKBOX, toggled, -1);

	gtk_tree_path_free(path);
}

static void thoth_preferences_show_passwords()
{
	thoth_preferences_load_saved_logins(TRUE);
}

static gboolean thoth_preferences_delete_password()
{
	gint confirm = thoth_confirm_dialog("Delete selected logins?",
			"Delete the selected logins?\n\n"
			"This is permanent, and can not be undone.",
			"Go _Back", "_Delete Selected");

	if (confirm != GTK_RESPONSE_ACCEPT)
		return FALSE;

	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loaded = g_key_file_load_from_file(keyfile, path, 0, &err);
	if (!loaded) {
		g_free(path);
		return FALSE;
	}

	GtkTreeIter iter;
	if (!gtk_tree_model_get_iter_first(GTK_TREE_MODEL(thoth_passwordliststore), &iter)) {
		g_key_file_free(keyfile);
		return FALSE;
	}

	/* The double-loop below is needed since gtk_tree_model_foreach() will
	 * accidentally skip iters during deletion. The skipping happens since:
	 * (1) the deleted iter's path is replaced by the next sibling, and
	 * (2) GTK's *_foreach() function goes to the next iter, skipping the
	 * replacement sibling iter in the process. */
	gboolean keyfilemodified = FALSE;
	do {
		gboolean checked;
		do {
			gchar *pageid;
			gtk_tree_model_get(GTK_TREE_MODEL(thoth_passwordliststore), &iter,
					PWD_COL_CHECKBOX, &checked,
					PWD_COL_PAGEID, &pageid, -1);

			if (checked) {
				if (g_key_file_remove_group(keyfile, pageid, NULL)) {
					gtk_list_store_remove(GTK_LIST_STORE(thoth_passwordliststore), &iter);
					keyfilemodified = TRUE;
				}
			}
		} while (checked);
	} while (gtk_tree_model_iter_next(GTK_TREE_MODEL(thoth_passwordliststore), &iter));

	if (keyfilemodified) {
		g_key_file_save_to_file(keyfile, path, &err);
	}

	g_free(path);
	g_key_file_free(keyfile);

	return TRUE;
}

static gboolean thoth_passwordmanager_update(gchar *pageid,
		gint column_id, gchar *strval)
{
	/* Load the password key file */
	GError *err = NULL;
	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-passwords.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	gboolean loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	if (!loadstate) {
		keyfile = g_key_file_new();
		g_error_free(err);
		err = NULL;
	}

	gchar *keyname;
	gchar *keyval;
	if (column_id == PWD_COL_URI) {
		gchar *keyval = g_base64_encode(strval, g_utf8_strlen(strval, -1));
		if (g_key_file_has_group(keyfile, keyval)) {
			GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(thoth_window),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
					"Error updating login URI.\nA URI already exists.");
			gtk_dialog_run(GTK_DIALOG(dialog));
			gtk_widget_destroy(dialog);
			g_free(keyval);

			return FALSE; /* Can not update duplicate URI */
		}

		keyname = g_strdup("uri");
	} else if (column_id == PWD_COL_USERNAME) {
		keyname = g_strdup("username");
		keyval = g_strdup(strval);
	} else if (column_id == PWD_COL_PASSWORD) {
		if (!thoth_master_password) {
			GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(thoth_window),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
					"Can not update login passwords that aren't shown.\nClick \"Show passwords\" before trying again.");
			gtk_dialog_run(GTK_DIALOG(dialog));
			gtk_widget_destroy(dialog);

			return FALSE; /* Can not update "hidden" password */
		}

		gchar *base64iv = g_key_file_get_string(keyfile, pageid, "base64iv", NULL);
		if (!base64iv) {
			if (!(base64iv = thoth_get_random_base64(16)))
				return FALSE; /* Can't generate initial vector. */
		}

		keyname = g_strdup("password");
		keyval = thoth_encrypt(strval, thoth_master_password, base64iv);
	} else {
		return FALSE; /* Invalid column */
	}

	if (!g_key_file_has_key(keyfile, pageid, keyname, &err))
		return FALSE;

	/* Make changes to an existing login */
	g_key_file_set_value(keyfile, pageid, keyname, keyval);
	g_key_file_save_to_file(keyfile, path, &err);
	g_key_file_free(keyfile);

	g_free(keyname);
	g_free(keyval);

	return TRUE;
}

static void thoth_preferences_passwordedited(GtkCellRenderer *renderer,
		gchar *path_string, gchar *new_text, gpointer data)
{
	gchar *pageid;
	GtkTreeIter iter;
	gint column_id = GPOINTER_TO_INT(data);

	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
	gtk_tree_model_get_iter(GTK_TREE_MODEL(thoth_passwordliststore), &iter, path);
	gtk_tree_model_get(GTK_TREE_MODEL(thoth_passwordliststore), &iter,
			PWD_COL_PAGEID, &pageid, -1);

	if (!thoth_passwordmanager_update(pageid, column_id, new_text))
		return; /* Didn't save to file. Don't update table either */

	gtk_list_store_set(thoth_passwordliststore, &iter,
			column_id, new_text, -1);
}

static void thoth_command_options_preferences()
{
	GtkWidget *label, *button, *combobox;
	GtkAdjustment *adjustment;

	thoth_preferences_dialog = gtk_dialog_new_with_buttons("Preferences",
			GTK_WINDOW(thoth_window), GTK_DIALOG_MODAL,
			"Close", GTK_RESPONSE_CLOSE, NULL);
	gtk_window_set_default_size(GTK_WINDOW(thoth_preferences_dialog), 480, -1);
	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(thoth_preferences_dialog));
	GtkWidget *notebook = gtk_notebook_new();
	gtk_widget_show(notebook);
	gtk_container_add(GTK_CONTAINER(content_area), notebook);
	gtk_widget_set_hexpand(notebook, TRUE);
	gtk_widget_set_vexpand(notebook, TRUE);
	g_signal_connect(G_OBJECT(notebook), "switch-page",
			G_CALLBACK(thoth_preferences_switch_page), NULL);

	GtkWidget *grid = gtk_grid_new();
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid, NULL);
	gtk_notebook_set_tab_label_text(GTK_NOTEBOOK(notebook), grid, "Options");
	gtk_grid_set_column_homogeneous(GTK_GRID(grid), FALSE);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 4);
	gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
	gtk_widget_set_margin_top(GTK_WIDGET(grid), 16);
	gtk_widget_set_margin_bottom(GTK_WIDGET(grid), 16);
	gtk_widget_set_margin_start(GTK_WIDGET(grid), 16);
	gtk_widget_show(grid);

	label = gtk_label_new("Editor Font:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 1, 1, 1);
	button = gtk_font_button_new();
	if (thoth_textview_font) {
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
		gtk_font_button_set_font_name(GTK_FONT_BUTTON(button), thoth_textview_font);
G_GNUC_END_IGNORE_DEPRECATIONS
	}
	gtk_widget_show(button);
	gtk_grid_attach(GTK_GRID(grid), button, 2, 1, 1, 1);
	g_signal_connect(G_OBJECT(button), "font-set",
			G_CALLBACK(thoth_callback_select_font), NULL);

	label = gtk_label_new("Text Colour:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 2, 1, 1);
	button = gtk_color_button_new();
	gtk_widget_show(button);
	gtk_color_chooser_set_use_alpha(GTK_COLOR_CHOOSER(button), FALSE);
	gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(button), &thoth_textview_fg_rgba);
	gtk_grid_attach(GTK_GRID(grid), button, 2, 2, 1, 1);
	g_signal_connect(G_OBJECT(button), "color-set",
			G_CALLBACK(thoth_callback_select_foreground_colour), NULL);

	label = gtk_label_new("Background Colour:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 3, 1, 1);
	button = gtk_color_button_new();
	gtk_widget_show(button);
	gtk_color_chooser_set_use_alpha(GTK_COLOR_CHOOSER(button), FALSE);
	gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(button), &thoth_textview_bg_rgba);
	gtk_grid_attach(GTK_GRID(grid), button, 2, 3, 1, 1);
	g_signal_connect(G_OBJECT(button), "color-set",
			G_CALLBACK(thoth_callback_select_background_colour), NULL);

	label = gtk_label_new("Recent Files:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 4, 1, 1);
	adjustment = gtk_adjustment_new(thoth_recentfilemax, 0, 30, 1, 5, 0);
	button = gtk_spin_button_new(adjustment, 1, 0);
	gtk_widget_show(button);
	gtk_grid_attach(GTK_GRID(grid), button, 2, 4, 1, 1);
	g_signal_connect(G_OBJECT(button), "value-changed",
			G_CALLBACK(thoth_callback_recent_files_spin_button), NULL);

	label = gtk_label_new("Tab Size:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 5, 1, 1);
	adjustment = gtk_adjustment_new(thoth_tabsize, 1, 32, 1, 1, 0);
	button = gtk_spin_button_new(adjustment, 1, 0);
	gtk_widget_show(button);
	gtk_grid_attach(GTK_GRID(grid), button, 2, 5, 1, 1);
	g_signal_connect(G_OBJECT(button), "value-changed",
			G_CALLBACK(thoth_callback_tab_size_spin_button), NULL);

	label = gtk_label_new("Close Button:");
	gtk_widget_set_halign(label, GTK_ALIGN_START);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 6, 1, 1);
	combobox = gtk_combo_box_text_new();
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox),
			"quit", "Quit");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox),
			"minimise", "Minimise");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox),
			"closeandkeepopen", "Close current file");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox),
			"closeandminimise", "Close current file and minimise");
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox),
			"closeandquit", "Close current file and quit");
	gtk_combo_box_set_active(GTK_COMBO_BOX(combobox), thoth_closebuttonaction);
	gtk_widget_set_hexpand(combobox, TRUE);
	gtk_widget_show(combobox);
	gtk_grid_attach(GTK_GRID(grid), combobox, 2, 6, 1, 1);
	g_signal_connect(G_OBJECT(combobox), "changed",
			G_CALLBACK(thoth_callback_close_button_combobox), NULL);

	/* Backup variables if needed before running dialog. */
	gchar *textview_font = g_strdup(thoth_textview_font);
	GdkRGBA textview_fg_rgba = thoth_textview_fg_rgba;
	GdkRGBA textview_bg_rgba = thoth_textview_bg_rgba;
	gint recentfilemax = thoth_recentfilemax;
	gint tabsize = thoth_tabsize;
	gint closebuttonaction = thoth_closebuttonaction;

	/* Prepare the password management tab */
	GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show(vbox);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vbox, NULL);
	gtk_notebook_set_tab_label_text(GTK_NOTEBOOK(notebook), vbox, "Passwords");

	GtkWidget *scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(scrolledwindow);
	gtk_box_pack_start(GTK_BOX(vbox), scrolledwindow, TRUE, TRUE, 0);

	thoth_passwordliststore = gtk_list_store_new(PWD_NUM_COLS,
			G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_STRING,
			G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	thoth_passwordtreeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(thoth_passwordliststore));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(thoth_passwordtreeview), TRUE);
	gtk_container_add(GTK_CONTAINER(scrolledwindow), thoth_passwordtreeview);
	gtk_widget_show(thoth_passwordtreeview);

	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	renderer = gtk_cell_renderer_toggle_new();
	column = gtk_tree_view_column_new_with_attributes("#", renderer,
			"active", PWD_COL_CHECKBOX, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(thoth_passwordtreeview), column);
	g_signal_connect(renderer, "toggled", G_CALLBACK(thoth_passwordtreeviewtoggle), NULL);

	renderer = gtk_cell_renderer_text_new();
	g_object_set(G_OBJECT(GTK_CELL_RENDERER_TEXT(renderer)), "editable", TRUE, NULL);
	g_object_set(G_OBJECT(renderer), "mode", GTK_CELL_RENDERER_MODE_EDITABLE,
			"ellipsize-set", TRUE, "ellipsize", PANGO_ELLIPSIZE_MIDDLE, NULL);
	g_signal_connect(G_OBJECT(renderer), "edited", G_CALLBACK(thoth_preferences_passwordedited),
			GINT_TO_POINTER(PWD_COL_URI));
	column = gtk_tree_view_column_new_with_attributes("Address", renderer,
			"text", PWD_COL_URI, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(thoth_passwordtreeview), column);
	gtk_tree_view_column_set_expand(column, TRUE);
	gtk_tree_view_column_set_resizable(column, TRUE);
	gtk_tree_view_column_set_min_width(column, 100);

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Type", renderer,
			"text", PWD_COL_PROTOCOL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(thoth_passwordtreeview), column);

	renderer = gtk_cell_renderer_text_new();
	g_object_set(G_OBJECT(renderer), "mode", GTK_CELL_RENDERER_MODE_EDITABLE, NULL);
	g_object_set(G_OBJECT(GTK_CELL_RENDERER_TEXT(renderer)), "editable", TRUE, NULL);
	g_signal_connect(renderer, "edited", G_CALLBACK(thoth_preferences_passwordedited),
			GINT_TO_POINTER(PWD_COL_USERNAME));
	column = gtk_tree_view_column_new_with_attributes("Username", renderer,
			"text", PWD_COL_USERNAME, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(thoth_passwordtreeview), column);

	renderer = gtk_cell_renderer_text_new();
	g_object_set(G_OBJECT(renderer), "mode", GTK_CELL_RENDERER_MODE_EDITABLE, NULL);
	g_object_set(G_OBJECT(GTK_CELL_RENDERER_TEXT(renderer)), "editable", TRUE, NULL);
	g_signal_connect(renderer, "edited", G_CALLBACK(thoth_preferences_passwordedited),
			GINT_TO_POINTER(PWD_COL_PASSWORD));
	column = gtk_tree_view_column_new_with_attributes("Password", renderer,
			"text", PWD_COL_PASSWORD, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(thoth_passwordtreeview), column);

	GtkWidget *passwordbuttonbox = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(passwordbuttonbox), GTK_BUTTONBOX_START);
	gtk_widget_show(passwordbuttonbox);
	gtk_box_pack_start(GTK_BOX(vbox), passwordbuttonbox, FALSE, FALSE, 0);

	GtkWidget *passwordbutton;

	passwordbutton = gtk_button_new_with_label("Delete");
	gtk_widget_show(passwordbutton);
	gtk_box_pack_start(GTK_BOX(passwordbuttonbox), passwordbutton, FALSE, FALSE, 0);
	g_signal_connect(passwordbutton, "clicked",
			G_CALLBACK(thoth_preferences_delete_password), NULL);

	passwordbutton = gtk_button_new_with_label("Show Passwords");
	gtk_widget_show(passwordbutton);
	gtk_box_pack_start(GTK_BOX(passwordbuttonbox), passwordbutton, FALSE, FALSE, 0);
	g_signal_connect(passwordbutton, "clicked",
			G_CALLBACK(thoth_preferences_show_passwords), NULL);

	/* Now run the preferences dialog */
	gint response = gtk_dialog_run(GTK_DIALOG(thoth_preferences_dialog));
	if (response == GTK_RESPONSE_CLOSE) {
		thoth_prune_recent_uri_list();
		g_free(textview_font);
	}
	gtk_widget_destroy(thoth_preferences_dialog);
}

static void thoth_command_options_about()
{
	GtkWidget *label, *entry;

	GtkWidget *about_dialog = gtk_dialog_new_with_buttons("About Thoth",
			GTK_WINDOW(thoth_window), GTK_DIALOG_MODAL,
			"Close", GTK_RESPONSE_CLOSE, NULL);
	gtk_window_set_default_size(GTK_WINDOW(about_dialog), 480, -1);
	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(about_dialog));
	GtkWidget *notebook = gtk_notebook_new();
	gtk_widget_show(notebook);
	gtk_container_add(GTK_CONTAINER(content_area), notebook);
	gtk_widget_set_hexpand(notebook, TRUE);
	gtk_widget_set_vexpand(notebook, TRUE);

	GtkWidget *grid = gtk_grid_new();
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), grid, NULL);
	gtk_notebook_set_tab_label_text(GTK_NOTEBOOK(notebook), grid, "About Thoth");
	gtk_grid_set_column_homogeneous(GTK_GRID(grid), FALSE);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 4);
	gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
	gtk_widget_set_margin_top(GTK_WIDGET(grid), 16);
	gtk_widget_set_margin_bottom(GTK_WIDGET(grid), 16);
	gtk_widget_show(grid);

	gchar *text_ctr = g_strdup_printf("<big><b>Thoth</b></big>\n"
			"The note taker and text editor.\n"
			"Version %d.%d.%d\n"
			"<a href=\"https://www.mirrorisland.com/thoth\">www.mirrorisland.com/thoth</a>",
			THOTH_VERSION_MAJOR, THOTH_VERSION_MINOR, THOTH_VERSION_PATCH);
	label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(label), text_ctr);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 1, 3, 1);
	g_free(text_ctr);

	gchar *text_lft = g_strdup_printf("Copyright (c) %d Janis and Sam Allen\n\n"
			"This version is available under the GNU GPL version 3\n"
			"The full license text can be read under the \"License\" tab.",
			THOTH_COPYRIGHT_YEAR);
	label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(label), text_lft);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 2, 3, 1);
	g_free(text_lft);

	GtkWidget *scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(scrolledwindow);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scrolledwindow, NULL);
	gtk_notebook_set_tab_label_text(GTK_NOTEBOOK(notebook), scrolledwindow, "License");
	GtkWidget *textview = gtk_text_view_new();
	gtk_widget_show(textview);
	gtk_container_add(GTK_CONTAINER(scrolledwindow), textview);
	gtk_widget_set_margin_top(textview, 12);
	gtk_widget_set_margin_bottom(textview, 12);
	gtk_widget_set_margin_start(textview, 12);
	gtk_widget_set_margin_end(textview, 12);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(textview), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textview), GTK_WRAP_WORD);
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	gchar *licensetext = NULL;
	gchar *licensefile = g_strdup("LICENSE.md");
	gboolean read_success = g_file_get_contents(licensefile, &licensetext, NULL, NULL);
	g_free(licensefile);
	if (!read_success) {
		licensetext = g_strdup("Released under the GNU GPL Version 3.0");
	}
	gtk_text_buffer_set_text(buffer, licensetext, -1);
	g_free(licensetext);

	gtk_dialog_run(GTK_DIALOG(about_dialog));
	gtk_widget_destroy(about_dialog);
}

static gboolean thoth_autosave_timeout(gpointer data)
{
	if (thoth_editor->category == THOTH_NOTE) {
		if (thoth_editor->modified) {
			thoth_save_file(thoth_editor, thoth_editor->filepath);
			thoth_editor->modified = FALSE;
		}
	}

	return TRUE;
}

/**
 * Shut down the program entirely.
 */
void thoth_exit()
{
	thoth_autosave_timeout(NULL);
	thoth_save_config();
	thoth_save_uris(NULL);
	printf("%s:%4d.%4d ========= g_application_quit ==========\n", __FILE__, __LINE__, dbg++);
	g_application_quit(G_APPLICATION(thoth_app));
}

/**
 * TODO: Show modified files in listview.
 */
static void thoth_command_quit()
{
	gboolean modified = FALSE;
	GList *listitem = thoth_editorlist;
	while ((listitem != NULL) && !modified) {
		ThothEditorStruct *editor = (ThothEditorStruct *)listitem->data;
		if (editor->category != THOTH_NOTE) {
			modified = gtk_text_buffer_get_modified(editor->textbuffer);
		}
		listitem = listitem->next;
	}

	if (modified) {
		gint confirm = thoth_confirm_dialog("Save before exiting?",
				"One or more files have been modified.\n\n"
				"You you want to go back to save before closing?",
				"Go _Back", "_Close Without Saving");
		if (confirm == GTK_RESPONSE_CANCEL) {
			return;
		}
	}
	thoth_exit();
}

/**
 * Closing editor from this func does not check if buffer is modified/unsaved.
 */
static gboolean thoth_close_editor(ThothEditorStruct *editor)
{
	/* Disconnect signals. */
	g_signal_handler_disconnect(G_OBJECT(editor->textview), editor->keypresseventid);
	g_signal_handler_disconnect(G_OBJECT(editor->textview), editor->scrolleventid);
	g_signal_handler_disconnect(G_OBJECT(editor->textbuffer), editor->modifiedchangedid);
	g_signal_handler_disconnect(G_OBJECT(editor->textbuffer), editor->inserttextid);
	g_signal_handler_disconnect(G_OBJECT(editor->textbuffer), editor->deleterangeid);
	g_signal_handler_disconnect(G_OBJECT(editor->textbuffer), editor->beginuseractionid);
	g_signal_handler_disconnect(G_OBJECT(editor->textbuffer), editor->enduseractionid);

	/* Free widget resources. */
	gint num = gtk_notebook_page_num(GTK_NOTEBOOK(thoth_notebook),
			editor->scrolledwindow);
	gtk_notebook_remove_page(GTK_NOTEBOOK(thoth_notebook), num);
	thoth_init_undo(editor);

	GtkTreeIter *treeiter = thoth_treemodel_iter_find_id(
			thoth_treemodel, editor->id);
	if (treeiter) {
		gtk_tree_store_remove(GTK_TREE_STORE(thoth_treemodel), treeiter);
	}

	/* Free memory allocations. */
	g_free(editor->uri);
	g_free(editor->title);

	/* Select the next available editor, or close if there aren't any.. */
	GList *listitem = thoth_editorlist_find_id(editor->id);
	GList *nextitem = NULL;

	/* First search forwards for next item in category */
	GList *ln = listitem->next;
	while (ln && !nextitem) {
		gint category = ((ThothEditorStruct *)ln->data)->category;
		if (category == editor->category)
			nextitem = ln;
		else
			ln = ln->next;
	}

	/* ...then backwards... */
	if (!nextitem) {
		GList *lp = listitem->prev;
		while (lp && !nextitem) {
			gint category = ((ThothEditorStruct *)lp->data)->category;
			if (category == editor->category)
				nextitem = lp;
			else
				lp = lp->prev;
		}
	}

	/* Otherwise, select whatever else is available */
	if (!nextitem) {
		if (listitem->next) {
			nextitem = listitem->next;
		} else if (listitem->prev) {
			nextitem = listitem->prev;
		}
	}
	thoth_editorlist = g_list_delete_link(thoth_editorlist, listitem);
	g_free(editor);

	if (nextitem) {
		thoth_editor = (ThothEditorStruct *)nextitem->data;
		thoth_set_current_uri(TRUE);
		thoth_update_title();
		thoth_update_undo_buttons();
		GtkTreeIter *iter = thoth_treemodel_iter_find_id(
				thoth_treemodel, thoth_editor->id);
		if (iter) {
			thoth_select_sidepanel_iter(thoth_treemodel, iter, TRUE);
		}

		thoth_save_uris(NULL);
	} else {
		/* No list item? Close the app. */
		thoth_exit();
	}
}

/**
 * Returns TRUE if file close, FALSE if user cancelled or error.
 */
static gboolean thoth_command_file_close()
{
	gboolean modified = thoth_editor->modified;
	if (thoth_editor->category == THOTH_NOTE) {
		if (gtk_text_buffer_get_char_count(thoth_editor->textbuffer)) {
			modified = TRUE;
		}
	}

	if (modified) {
		gint confirm = thoth_confirm_dialog("Save file before closing?",
				"A modified file is about to be closed.\n\n"
				"Do you want to go back and save it?",
				"Go _Back", "_Close File");

		if (confirm != GTK_RESPONSE_ACCEPT) {
			return FALSE; /* Don't exit yet. */
		}
	}
	if ((thoth_editor->category == THOTH_PAGE)
			|| (thoth_editor->category == THOTH_NOTE)) {
		/* Web pages and Note files are immediately deleted. */
		g_remove(thoth_editor->filepath);
	}
	thoth_close_editor(thoth_editor);

	return TRUE;
}

/**
 * Automatically shows/hides/raises window
 * if both force_visible and force_hide are FALSE.
 */
static void thoth_showhide(gboolean force_visible, gboolean force_hide)
{
	if ((force_hide) || (gtk_window_is_active(GTK_WINDOW(thoth_window)) && !force_visible)) {
		gtk_window_get_size(GTK_WINDOW(thoth_window), &thoth_width, &thoth_height);
		gtk_window_get_position(GTK_WINDOW(thoth_window), &thoth_left, &thoth_top);
		gtk_widget_hide(thoth_window);
	} else {
		if (!gtk_widget_get_visible(thoth_window)) {
			gtk_window_resize(GTK_WINDOW(thoth_window), thoth_width, thoth_height);
			gtk_window_move(GTK_WINDOW(thoth_window), thoth_left, thoth_top);
		}
		gtk_window_present(GTK_WINDOW(thoth_window));
	}
}

/**
 * Handle what the "Close" button does. This is user-configurable.
 */
static void thoth_command_close_window()
{
	switch(thoth_closebuttonaction) {
	case 2: /* Close current file */
		thoth_command_file_close();
		break;

	case 3: /* Close current file and minimise to tray */
		if (!thoth_command_file_close())
			return; /* Do not fall through if no file closed */
	case 1: /* Minimise to tray icon */
		thoth_showhide(0, TRUE);
		break;

	case 4: /* Close current file and quit */
		if (!thoth_command_file_close())
			return; /* Do not fall through if no file closed */
	case 0: /* Quit */
		thoth_command_quit();
		break;
	default:
		printf("%s:%4d.%4d Unknown action. thoth_closebuttonaction=%d.\n",
				__FILE__, __LINE__, dbg++, thoth_closebuttonaction);
	}
}

/**
 * Respond to buttonts and menu items.
 */
static void thoth_command_activate(GtkWidget *widget, gpointer data)
{
	switch(GPOINTER_TO_INT(data)) {
	case COMMAND_NEW:
		thoth_showhide(TRUE, 0);
		/* Follow through */
	case FILE_NEW:
		thoth_command_file_new();
		break;
	case COMMAND_OPEN:
		thoth_showhide(TRUE, 0);
		/* Follow through */
	case FILE_OPEN:
		thoth_command_file_open();
		break;
	case COMMAND_OPEN_WEB:
		thoth_showhide(TRUE, 0);
		/* Follow through */
	case FILE_OPEN_WEB_ADDRESS:
		thoth_command_file_open_web_address();
		break;
	case FILE_RELOAD:
		thoth_command_file_reload();
		break;
	case COMMAND_SIDEPANEL:
		thoth_command_sidepanel();
		break;
	case COMMAND_SAVE:
		thoth_command_file_save();
		break;
	case FILE_SAVE_AS:
		thoth_command_file_save_as();
		break;
	case FILE_SAVE_TO_WEB:
		thoth_command_file_save_to_web(NULL);
		break;
	case FILE_PRINT_PREVIEW:
		thoth_command_file_print_preview();
		break;
	case FILE_PRINT:
		thoth_command_file_print();
		break;
	case FILE_PROPERTIES:
		thoth_command_file_properties();
		break;
	case FILE_CLOSE:
		thoth_command_file_close();
		break;
	case COMMAND_UNDO:
		thoth_command_undo();
		break;
	case COMMAND_REDO:
		thoth_command_redo();
		break;
	case EDIT_CUT:
		thoth_command_edit_cut();
		break;
	case EDIT_COPY:
		thoth_command_edit_copy();
		break;
	case COMMAND_PASTE_NEW:
		thoth_showhide(TRUE, 0);
		thoth_command_file_new();
		/* Follow through */
	case EDIT_PASTE:
		thoth_command_edit_paste();
		break;
	case EDIT_SELECT_ALL:
		thoth_command_edit_selectall();
		break;
	case EDIT_FIND:
		gtk_widget_show(thoth_findwidget);
		gtk_widget_grab_focus(thoth_findentry);
		break;
	case EDIT_JUMPTO:
		thoth_command_edit_jumpto();
		break;
	case EDIT_INDENT:
		thoth_indent(1);
		break;
	case EDIT_UNINDENT:
		thoth_indent(-1);
		break;
	case EDIT_UPPERCASE:
		thoth_format_selection(EDIT_UPPERCASE);
		break;
	case EDIT_LOWERCASE:
		thoth_format_selection(EDIT_LOWERCASE);
		break;
	case EDIT_REMOVE_TRAILING_SPACES:
		thoth_format_selection(EDIT_REMOVE_TRAILING_SPACES);
		break;
	case OPTIONS_WRAP_LINES:
		thoth_command_options_wrap_lines();
		break;
	case OPTIONS_SHOW_LINE_NUMBERS:
		thoth_command_options_show_line_numbers();
		break;
	case OPTIONS_AUTOINDENT:
		thoth_command_options_autoindent();
		break;
	case OPTIONS_KEEP_ABOVE:
		thoth_command_options_keepabove();
		break;
	case OPTIONS_REOPEN_FILES_ON_STARTUP:
		thoth_command_options_reopen_files_on_startup();
		break;
	case OPTIONS_PREFERENCES:
		thoth_command_options_preferences();
		break;
	case OPTIONS_ABOUT:
		thoth_command_options_about();
		break;
	case COMMAND_FIND_NEXT:
		thoth_command_find_next(1, TRUE);
		break;
	case COMMAND_FIND_PREVIOUS:
		thoth_command_find_next(-1, TRUE);
		break;
	case COMMAND_FIND_REPLACE:
		thoth_command_find_replace(FALSE);
		break;
	case COMMAND_FIND_REPLACE_ALL:
		thoth_command_find_replace(TRUE);
		break;
	case COMMAND_FIND_CLOSE:
		gtk_widget_hide(thoth_findwidget);
		gtk_widget_grab_focus(thoth_editor->textview);
		break;
	case COMMAND_JUMPTO:
		thoth_command_jumpto();
		break;
	case COMMAND_CLOSE_WINDOW:
		thoth_command_close_window();
		break;
	case FILE_QUIT:
	case COMMAND_QUIT:
		thoth_command_quit();
		break;
	default:
		printf("%s:%4d.%4d Unknown command. data=%d.\n", __FILE__, __LINE__, dbg++, data);
	}
}

/* Add buttons */
static void thoth_add_box_button(GtkWidget *box, gint ev, gchar *icon_name, gchar *tooltip,
		GtkAccelGroup *accel_group, guint accel_key, GdkModifierType accelmods)
{
	GtkWidget *button = gtk_button_new_from_icon_name(icon_name, 1);
	gtk_widget_set_tooltip_text(GTK_WIDGET(button), tooltip);
	gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(box), button, FALSE, FALSE, 0);
	g_signal_connect(G_OBJECT(button), "clicked",
			G_CALLBACK(thoth_command_activate), GINT_TO_POINTER(ev));
	if (accel_group && accel_key) {
		gtk_widget_add_accelerator(GTK_WIDGET(button), "clicked",
				accel_group, accel_key, accelmods, GTK_ACCEL_VISIBLE);
	}
}

/**
 * Show/hide menus with toggle button.
 * Also, reset toggle button when menu disappears.
 */
static void thoth_toggle_menu_close(GtkWidget *widget, gpointer data)
{
	gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(data), FALSE);
}

static void thoth_toggle_menu(GtkWidget *widget, gpointer data)
{
	if (!gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(widget))) {
		return; /* Toggle is not on. Leave. */
	}

	gtk_menu_popup_at_widget(GTK_MENU(data), widget,
			GDK_GRAVITY_SOUTH_WEST, GDK_GRAVITY_NORTH_WEST, NULL);

	g_signal_connect(G_OBJECT(data), "unmap",
			G_CALLBACK(thoth_toggle_menu_close), widget);
}

static void thoth_add_menu_item(GtkWidget *menu, gint ev, gchar *label, gboolean checked,
		GtkAccelGroup *accel_group, guint accel_key, GdkModifierType accelmods)
{
	GtkWidget *menuitem;
	if (checked) {
		menuitem = gtk_check_menu_item_new_with_mnemonic(label);
	} else {
		menuitem = gtk_menu_item_new_with_mnemonic(label);
	}
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
	gtk_widget_show(menuitem);

	gulong handler = g_signal_connect(G_OBJECT(menuitem), "activate",
			G_CALLBACK(thoth_command_activate), GINT_TO_POINTER(ev));
	if (accel_group && accel_key) {
		GtkWidget *child = gtk_bin_get_child(GTK_BIN(menuitem));
		gtk_accel_label_set_accel(GTK_ACCEL_LABEL(child), accel_key, accelmods);
		gtk_widget_add_accelerator(GTK_WIDGET(menuitem), "activate",
				accel_group, accel_key, accelmods, GTK_ACCEL_VISIBLE);
	}
	thoth_menuitem[ev] = menuitem;
	thoth_signalhandler[ev] = handler;
}

static GtkToolItem* thoth_add_toolbar_item(GtkWidget *toolbar, gint ev,
		gchar *icon_name, gchar *tooltip, gboolean sensitive,
		GtkAccelGroup *accel_group, guint accel_key, GdkModifierType accelmods)
{
	GtkToolItem *toolitem = gtk_tool_button_new(NULL, tooltip);
	gtk_widget_set_tooltip_text(GTK_WIDGET(toolitem), tooltip);
	gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(toolitem), icon_name);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolitem, -1);
	gtk_widget_set_sensitive(GTK_WIDGET(toolitem), sensitive);
	g_signal_connect(G_OBJECT(toolitem), "clicked",
			G_CALLBACK(thoth_command_activate), GINT_TO_POINTER(ev));
	if (accel_group && accel_key) {
		gtk_widget_add_accelerator(GTK_WIDGET(toolitem), "clicked",
				accel_group, accel_key, accelmods, GTK_ACCEL_VISIBLE);
	}
	return toolitem;
}

/**
 * - [location] import uri lists. If NULL, read from user config (Default)
 *   If importing, existing entries and the recent file queue are not overwritten.
 */
static void thoth_load_uris(gchar *location)
{
	GError *err = NULL;
	gboolean loadstate;
	gsize i, num_uris;

	gchar *path = g_build_filename(g_get_user_config_dir(), "mirrorisland",
			"thoth-uris.ini", NULL);
	GKeyFile *keyfile = g_key_file_new();
	loadstate = g_key_file_load_from_file(keyfile, path, 0, &err);
	g_free(path);
	if (!loadstate) {
		g_key_file_free(keyfile);
		return; /* Can't load config - nothing more to do. */
	}

	/* Add recent files to menu. */
	gchar **recenturis = g_key_file_get_string_list(keyfile, "RecentURIs",
			"RecentURIList", &num_uris, &err);
	if (num_uris > thoth_recentfilemax)
		num_uris = thoth_recentfilemax;

	for (i = 0; i < num_uris; i++) {
		thoth_add_recent_uri(recenturis[i], FALSE);
	}
	g_strfreev(recenturis);

	/* Add pages to the sidepanel. */
	gchar **pageurilist = g_key_file_get_string_list(keyfile, "PageURIs",
			"PageURIList", &num_uris, &err);
	for (i = 0; i < num_uris; i++) {
		gchar *uridata = pageurilist[i];
		gchar *uri = NULL;
		gint64 accesstime = 0;

		GRegex *uriregex = g_regex_new("([0-9]+)::(.+)", G_REGEX_CASELESS, 0, NULL);
		GMatchInfo *matchinfo;
		g_regex_match(uriregex, uridata, 0, &matchinfo);
		if (g_match_info_matches(matchinfo)) {
			gchar *accesstimematch = g_match_info_fetch(matchinfo, 1);
			uri = g_match_info_fetch(matchinfo, 2);

			accesstime = g_ascii_strtoll(accesstimematch, 0, 0);

			g_free(accesstimematch);
		} else {
			uri = g_strdup(uridata);
		}
		g_match_info_free(matchinfo);
		g_regex_unref(uriregex);

		/* Show page if already opened, otherwise try opening it. */
		gboolean editorid = thoth_open_file(uri, THOTH_PAGE, TRUE, FALSE);

		if (editorid) {
			thoth_add_sidepanel_item(&thoth_pagesiter, THOTH_PAGE, editorid,
					uri, uri, TRUE);
		}
		g_free(uri);

		/* Restore accesstime, if possible, for Ctrl+Tab and tray icon list ordering */
		if (accesstime) {
			thoth_editor->accesstime = accesstime;
		}
	}
	g_strfreev(pageurilist);

	/* Add notes to the sidepanel. */
	gchar **noteurilist = g_key_file_get_string_list(keyfile, "NoteURIs",
			"NoteURIList", &num_uris, &err);
	for (i = 0; i < num_uris; i++) {
		gchar *uridata = noteurilist[i];
		gchar *uri = NULL;
		gint64 accesstime = 0;

		GRegex *uriregex = g_regex_new("([0-9]+)::(.+)", G_REGEX_CASELESS, 0, NULL);
		GMatchInfo *matchinfo;
		g_regex_match(uriregex, uridata, 0, &matchinfo);
		if (g_match_info_matches(matchinfo)) {
			gchar *accesstimematch = g_match_info_fetch(matchinfo, 1);
			uri = g_match_info_fetch(matchinfo, 2);

			accesstime = g_ascii_strtoll(accesstimematch, 0, 0);

			g_free(accesstimematch);
		} else {
			uri = g_strdup(uridata);
		}
		g_match_info_free(matchinfo);
		g_regex_unref(uriregex);

		/* Show note if already opened, otherwise try opening it. */
		gboolean editorid = thoth_open_file(uri, THOTH_NOTE, TRUE, FALSE);

		if (editorid) {
			gchar *tooltip = thoth_get_snippet(255, FALSE);
			thoth_add_sidepanel_item(&thoth_notesiter, THOTH_NOTE, editorid,
					uri, tooltip, TRUE);
			g_free(tooltip);
		}
		g_free(uri);

		/* Restore accesstime, if possible, for Ctrl+Tab and tray icon list ordering */
		if (accesstime) {
			thoth_editor->accesstime = accesstime;
		}
	}
	g_strfreev(noteurilist);

	/* Reopen local files, if requested. */
	if (thoth_reopenfilesonstartup) {
		gchar **fileurilist = g_key_file_get_string_list(keyfile, "FileURIs",
				"FileURIList", &num_uris, &err);
		for (i = 0; i < num_uris; i++) {
			gchar *uridata = fileurilist[i];
			gchar *uri = NULL;
			gint64 accesstime = 0;

			GRegex *uriregex = g_regex_new("([0-9]+)::(.+)", G_REGEX_CASELESS, 0, NULL);
			GMatchInfo *matchinfo;
			g_regex_match(uriregex, uridata, 0, &matchinfo);
			if (g_match_info_matches(matchinfo)) {
				gchar *accesstimematch = g_match_info_fetch(matchinfo, 1);
				uri = g_match_info_fetch(matchinfo, 2);

				accesstime = g_ascii_strtoll(accesstimematch, 0, 0);

				g_free(accesstimematch);
			} else {
				uri = g_strdup(uridata);
			}
			g_match_info_free(matchinfo);
			g_regex_unref(uriregex);

			gchar *filename = thoth_filename_from_uri(uri);

			/* Show file if already opened, otherwise try opening it. */
			gboolean editorid = thoth_open_file(uri, THOTH_FILE, TRUE, FALSE);

			if (editorid) {
				thoth_add_recent_uri(uri, TRUE);
				thoth_prune_recent_uri_list();
				thoth_add_sidepanel_item(&thoth_openfilesiter, THOTH_FILE,
						editorid, uri, filename, TRUE);
			}
			g_free(filename);
			g_free(uri);

			/* Restore accesstime, if possible, for Ctrl+Tab and tray icon list ordering */
			if (accesstime) {
				thoth_editor->accesstime = accesstime;
			}
		}
		g_strfreev(fileurilist);

	}

	/* Restore the last opened file, if it is there. */
	gchar *fileuri = g_key_file_get_string(keyfile, "FileURIs", "FileURI", &err);
	if (fileuri) {
		/* Select the already opened file, note or page. */
		GtkTreeIter *iter = thoth_treestore_iter_find_uri(
				thoth_treemodel, fileuri);
		if (iter) {
			/* Focus on already opened file instead of reopening it. */
			thoth_select_sidepanel_iter(thoth_treemodel, iter, TRUE);
			thoth_set_current_uri(TRUE);
		}
		g_free(fileuri);
	}

	g_key_file_free(keyfile);
}

static void thoth_callback_delete_event(GtkWidget *widget, gpointer data)
{
	thoth_command_quit();
}

/**
 * Callback when recent file clicked
 */
void thoth_callback_open_recent(GtkWidget *widget, gpointer data)
{
	const gchar *uri_label = gtk_menu_item_get_label(GTK_MENU_ITEM(widget));
	/* TODO: Cycle through thoth_recentfilelist, find matching menu widget instead. */
	gchar *uri = g_strdup(uri_label);

	/* Show file if already opened, otherwise try opening it. */
	gint editorid = 0;
	gchar *filename = thoth_filename_from_uri(uri);
	GtkTreeIter *iter = thoth_treestore_iter_find_uri(thoth_treemodel, uri);
	if (iter) {
		/* Focus on already opened file instead of reopening it. */
		thoth_select_sidepanel_iter(thoth_treemodel, iter, TRUE);
	} else if (G_LIKELY(filename)) {
		editorid = thoth_open_file(uri, THOTH_FILE, TRUE, FALSE);
	}

	if ((editorid == 0) && (iter == NULL)) {
		gint confirm = thoth_confirm_dialog("Remove invalid recent entry?",
				"This recent files entry can not be opened.\n\n"
				"Do you want to remove it from recent files?",
				"_Do Not Remove", "_Remove The Entry");

		if (confirm == GTK_RESPONSE_ACCEPT) {
			thoth_remove_recent_uri(uri);
		}
	} else {
		thoth_add_recent_uri(uri, TRUE);
		thoth_prune_recent_uri_list();
		thoth_set_current_uri(TRUE);
		if (iter == NULL) {
			thoth_add_sidepanel_item(&thoth_openfilesiter, THOTH_FILE, editorid,
					uri, filename, TRUE);
		}
	}
	g_free(uri);
	g_free(filename);
}

/**
 * Handle sidepanel treeview and searchentry events.
 */
gchar *thoth_sidepanel_entry_text;

static gboolean thoth_sidepanel_search_treemodel_foreach_func(GtkTreeModel *model,
		GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	gchar *title, *uri;
	gint category;
	gboolean visible = FALSE;
	gtk_tree_model_get(model, iter, SPL_COL_URI, &uri, SPL_COL_TITLE, &title,
			SPL_COL_CATEGORY, &category, -1);
	if (title) {
		gchar *t = g_utf8_casefold(title, -1);
		if (g_strrstr(t, thoth_sidepanel_entry_text))
			visible = TRUE;
		g_free(t);
	}
	if ((uri) && (category != THOTH_NOTE)) {
		gchar *u = g_utf8_casefold(uri, -1);
		if (g_strrstr(uri, thoth_sidepanel_entry_text))
			visible = TRUE;
		g_free(u);
	}
	gtk_tree_store_set(GTK_TREE_STORE(model), iter, SPL_COL_VISIBLE, visible, -1);

	g_free(title);
	g_free(uri);

	/* Show parent nodes, if filtering a treeview/treemodel. */
	if (visible) {
		GtkTreeIter parentiter;
		gboolean valid = gtk_tree_model_iter_parent(model, &parentiter, iter);
		while (valid) {
			gtk_tree_store_set(GTK_TREE_STORE(model), &parentiter,
					SPL_COL_VISIBLE, TRUE, -1);
			GtkTreeIter childiter = parentiter;
			valid = gtk_tree_model_iter_parent(model, &parentiter, &childiter);
		}
	}

	return FALSE;
}

static gboolean thoth_activateSidepanelTreePath(GtkWidget *treeview, GtkTreePath *path)
{
	gint id;
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	gtk_tree_model_get_iter(model, &iter, path);
	gtk_tree_model_get(model, &iter, SPL_COL_ID, &id, -1);
	if (id == 0)
		return FALSE; /* Root iter. */

	GList *listitem = thoth_editorlist_find_id(id);
	if (!listitem)
		return FALSE;

	ThothEditorStruct *editor = (ThothEditorStruct *)listitem->data;
	if ((thoth_editor->category == THOTH_NOTE) && (thoth_editor != editor)) {
		thoth_save_file(thoth_editor, thoth_editor->filepath);
	}

	if (!editor)
		return FALSE;

	thoth_editor = editor;
	thoth_set_textview_font(editor->textview, thoth_textview_font);
	thoth_set_textview_tab_size(editor->textview, thoth_tabsize);

	if (editor->wraplines != thoth_wraplines) {
		editor->wraplines = thoth_wraplines;
		thoth_set_textview_wrap_mode(editor->textview, thoth_wraplines);
	}

	if (editor->linenumbers != thoth_showlinenumbers) {
		editor->linenumbers = thoth_showlinenumbers;
		thoth_set_textview_line_numbers(editor->textview,
				thoth_showlinenumbers);
	}
	gint num = gtk_notebook_page_num(GTK_NOTEBOOK(thoth_notebook),
			editor->scrolledwindow);
	gtk_notebook_set_current_page(GTK_NOTEBOOK(thoth_notebook), num);

	return TRUE;
}

static void thoth_callback_sidepanelentry_search_changed(GtkSearchEntry *entry,
		gpointer data)
{
	const gchar *entrytext = gtk_entry_get_text(GTK_ENTRY(entry));
	gint len = g_utf8_strlen(entrytext, -1);
	if (len) {
		/* Iterate through tree and list models setting nodes visible. */
		thoth_sidepanel_entry_text = g_utf8_casefold(entrytext, -1);
		gtk_tree_model_foreach(thoth_treemodel,
				thoth_sidepanel_search_treemodel_foreach_func, 0);
		g_free(thoth_sidepanel_entry_text);

		/* Display the updated tree model through a tree model filter. */
		GtkTreeModel *treefilter = gtk_tree_model_filter_new(thoth_treemodel, NULL);
		gtk_tree_model_filter_set_visible_column(
				GTK_TREE_MODEL_FILTER(treefilter), SPL_COL_VISIBLE);
		gtk_tree_view_set_model(GTK_TREE_VIEW(thoth_treeview), treefilter);
		gtk_tree_view_expand_all(GTK_TREE_VIEW(thoth_treeview));
	} else {
		gtk_tree_view_set_model(GTK_TREE_VIEW(thoth_treeview), thoth_treemodel);

		/* Focus on current editor page. */
		GtkTreeIter *treeiter = thoth_treestore_iter_find_uri(
				thoth_treemodel, thoth_editor->uri);
		thoth_select_sidepanel_iter(thoth_treemodel, treeiter, TRUE);
	}
}

static gboolean thoth_callback_treeview_button_press_event(GtkWidget *treeview,
		GdkEventButton *ev, gpointer data)
{
	return FALSE;

	if (ev->button == 3) {
		/* Handle right-click. */
		GtkTreePath *path;
		gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview), ev->x, ev->y, &path,
				NULL, NULL, NULL);
		if (!path)
			return FALSE;

		gint id;
		GtkTreeIter iter;
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
		gtk_tree_model_get_iter(model, &iter, path);
		gtk_tree_model_get(model, &iter, SPL_COL_ID, &id, -1);
		if (id == 0)
			return FALSE; /* Root iter. */

		/* Do things with the returned editor ID... */
	}
	return FALSE;
}

static gboolean thoth_callback_treeview_key_press_event(GtkWidget *treeview,
		GdkEventKey *ev, gpointer data)
{
	/* printf("%4d TODO handle key presses.\n", __LINE__); */
	return FALSE;
}

static gboolean thoth_callback_treeview_row_activated(GtkWidget *treeview,
		GtkTreePath *treepath, GtkTreeViewColumn *column, gpointer data)
{
	thoth_activateSidepanelTreePath(treeview, treepath);
	return FALSE;
}

static gboolean thoth_notebook_switch_page(GtkNotebook *notebook, GtkWidget *page,
		guint page_num, gpointer data)
{
	if (thoth_editor) {
		if (!thoth_editor->uri)
			return FALSE;
		thoth_set_current_uri(TRUE);
		thoth_callback_modified_changed(thoth_editor->textbuffer, NULL);
		gtk_widget_grab_focus(thoth_editor->textview);
		if (thoth_loaded) {
			/* Conditional; thoth_load_uris() sets accesstime */
			thoth_editor->accesstime = g_get_real_time();
		}
	}
	return FALSE;
}

static void thoth_undo_init(ThothEditorStruct *editor)
{
	GtkTextBuffer *textbuffer = editor->textbuffer;

	gulong inserttextid = g_signal_connect_after(G_OBJECT(textbuffer), "insert-text",
			G_CALLBACK(thoth_callback_inserttext), NULL);
	gulong deleterangeid = g_signal_connect(G_OBJECT(textbuffer), "delete-range",
			G_CALLBACK(thoth_callback_deleterange), NULL);
	gulong beginuseractionid = g_signal_connect_after(G_OBJECT(textbuffer),
			"begin-user-action", G_CALLBACK(thoth_callback_unblockuseraction), NULL);
	gulong enduseractionid = g_signal_connect(G_OBJECT(textbuffer), "end-user-action",
			G_CALLBACK(thoth_callback_blockuseraction), NULL);
	thoth_callback_blockuseraction(textbuffer);

	editor->inserttextid = inserttextid;
	editor->deleterangeid = deleterangeid;
	editor->beginuseractionid = beginuseractionid;
	editor->enduseractionid = enduseractionid;
}

/**
 * Add another editor page.
 */
static ThothEditorStruct *thoth_new_window_add_editor(gint category)
{
	GtkWidget *editorscrolledwindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_set_size_request(editorscrolledwindow, -1, -1);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(editorscrolledwindow),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type(
			GTK_SCROLLED_WINDOW(editorscrolledwindow), GTK_SHADOW_IN);
	gtk_widget_show(editorscrolledwindow);

	GtkWidget *textview = gtk_text_view_new();
	gtk_container_add(GTK_CONTAINER(editorscrolledwindow), textview);
	gtk_text_view_set_top_margin(GTK_TEXT_VIEW(textview), 4);
	gtk_text_view_set_right_margin(GTK_TEXT_VIEW(textview), 4);
	gtk_text_view_set_bottom_margin(GTK_TEXT_VIEW(textview), 4);
	gtk_text_view_set_left_margin(GTK_TEXT_VIEW(textview), 4);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(textview), TRUE);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(textview), TRUE);
	gtk_widget_set_sensitive(textview, TRUE);
	thoth_set_textview_font(textview, thoth_textview_font);
	thoth_set_textview_tab_size(textview, thoth_tabsize);
	thoth_set_textview_wrap_mode(textview, thoth_wraplines);
	thoth_set_textview_line_numbers(textview, thoth_showlinenumbers);
	gtk_widget_show(textview);
	GtkWrapMode wrapmode = GTK_WRAP_NONE;
	if (thoth_wraplines)
		wrapmode = GTK_WRAP_WORD;
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textview), wrapmode);
	gtk_widget_add_events(textview, GDK_SCROLL_MASK);

	GtkTextBuffer *textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	gulong keypresseventid = g_signal_connect(G_OBJECT(textview), "key-press-event",
			G_CALLBACK(thoth_textview_key_press_event), NULL);
	gulong scrolleventid = g_signal_connect(G_OBJECT(textview), "scroll-event",
			G_CALLBACK(thoth_textview_scroll_event), NULL);
	gulong modifiedchangedid = g_signal_connect(G_OBJECT(textbuffer), "modified-changed",
			G_CALLBACK(thoth_callback_modified_changed), NULL);

	gint notebookindex = gtk_notebook_append_page(GTK_NOTEBOOK(thoth_notebook),
			editorscrolledwindow, NULL);
	gtk_notebook_set_current_page(GTK_NOTEBOOK(thoth_notebook), notebookindex);

	thoth_lasteditorid++;

	/* Keep track of active editors. */
	ThothEditorStruct *editor = g_malloc(sizeof(ThothEditorStruct));
	editor->id = thoth_lasteditorid;
	editor->category = category;
	editor->uri = NULL;
	editor->title = NULL;
	editor->protocol = NULL;
	editor->username = NULL;
	editor->password = NULL;
	editor->base64iv = NULL; /* Used to decode password with master password */
	editor->unixpermissions = -1;
	editor->mrispermissions = 0;
	editor->scrolledwindow = editorscrolledwindow;
	editor->textview = textview;
	editor->textbuffer = textbuffer;
	editor->undolist = NULL;
	editor->undoindex = 0;
	editor->accesstime = g_get_real_time();
	editor->modifiedchangedid = modifiedchangedid;
	editor->keypresseventid = keypresseventid;
	editor->scrolleventid = scrolleventid;
	editor->modified = FALSE;
	editor->wraplines = thoth_wraplines;
	editor->linenumbers = thoth_showlinenumbers;
	thoth_editorlist = g_list_append(thoth_editorlist, editor);

	thoth_undo_init(editor);

	return editor;
}

/**
 * Generate a GList of open files, ordered by the most recently viewed, used by
 * the open file switcher and tray icon menu.
 * - maxsize: maximum number of list entries to return.
 * Returns a GList of "ThothEditorStruct"s in order of most recently viewed,
 * which must be freed after using it.
 */
static GList* thoth_get_recenteditorlist(gint maxsize)
{
	/* Generate the list first */
	GList *editorlist = thoth_editorlist;
	GList *switcherlist = NULL;
	while (editorlist != NULL) {
		ThothEditorStruct *editor = (ThothEditorStruct *)editorlist->data;
		GList *insertpoint = NULL;
		GList *switcher = switcherlist;

		while ((switcher != NULL) && (insertpoint == NULL)) {
			if (editor->accesstime > ((ThothEditorStruct *)switcher->data)->accesstime) {
				/* Insert BEFORE current entry if it's accessed at a later time */
				insertpoint = switcher;
			} else {
				switcher = switcher->next;
			}
		}

		switcherlist = g_list_insert_before(switcherlist,
				insertpoint, editor);

		/* Crop the last items if they exceed the max length */
		while (g_list_length(switcherlist) > maxsize) {
			switcherlist = g_list_delete_link(switcherlist, g_list_last(switcherlist));
		}

		editorlist = editorlist->next;
	}

	return switcherlist;
}

/**
 * Following functions set up the menu for tray icon.
 */
static void thoth_callback_statusmenuitem(GtkWidget *widget, gpointer data)
{
	gint id = GPOINTER_TO_INT(data);

	/* Quit/Exit is given id of zero (0) */
	if (id == 0) {
		thoth_command_quit();
		return;
	}
}

/**
 * Adds a menu item to the status icon menu. If a ThothEditorStruct is given,
 * a "Copy" button is added to the entry.
 * - id: Position of menu item to insert if editor given, otherwise the menu
 *    item is appended with id as "pointer data".
 * Returns the GtkMenuItem, which could contain other widgets inside it.
 */
static GtkWidget* thoth_addstatusmenuitem(gchar *label, gint id, ThothEditorStruct *editor)
{
	GtkWidget *menuitem;

	/* Thoth editors aren't NULL */
	if (editor) {
		menuitem = ticb_menu_item_new(label);
		gtk_menu_shell_insert(GTK_MENU_SHELL(thoth_statusmenu), menuitem, id);

		TicbMenuItemPrivate *priv = ticb_menu_item_get_instance_private(
				TICB_MENU_ITEM(menuitem));
		priv->editor = editor;

		thoth_statusmenulist = g_list_append(thoth_statusmenulist, menuitem);
	} else {
		menuitem = gtk_menu_item_new_with_label(label);
		gtk_widget_show(menuitem);
		g_signal_connect(G_OBJECT(menuitem), "activate",
				G_CALLBACK(thoth_callback_statusmenuitem),
				GINT_TO_POINTER(id));
		gtk_menu_shell_append(GTK_MENU_SHELL(thoth_statusmenu), menuitem);
	}
	return menuitem;
}

static GtkWidget* thoth_addstatusmenuseparator(const gchar *text)
{
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	GtkWidget *label = gtk_label_new(text);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
	GtkWidget *separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	g_object_set(G_OBJECT(separator), "valign", GTK_ALIGN_CENTER, NULL);
	gtk_box_pack_start(GTK_BOX(hbox), separator, TRUE, TRUE, 0);

	GtkWidget *menuitem = gtk_menu_item_new();
	g_object_set(G_OBJECT(menuitem), "child", hbox,
			"sensitive", FALSE, NULL);
	gtk_widget_show_all(menuitem);
	gtk_menu_shell_append(GTK_MENU_SHELL(thoth_statusmenu), menuitem);

	return menuitem;
}

static void thoth_updatestatusmenu()
{
	/* Reset the status icon menu if needed */
	if (thoth_statusmenulist) {
		GList *listitem = thoth_statusmenulist;
		while (listitem != NULL) {
			TicbMenuItem *item = (TicbMenuItem *)listitem->data;
			gtk_widget_destroy(GTK_WIDGET(item));

			listitem = listitem->next;
		}
		g_list_free(thoth_statusmenulist);
		thoth_statusmenulist = NULL;
	}

	/* Now add items to the status icon menu */
	GList *recentlist = thoth_get_recenteditorlist(15); /* TODO: Make user-configurable */
	GList *listitem = recentlist;
	gint ct = 0;
	while (listitem != NULL) {
		ThothEditorStruct *editor = (ThothEditorStruct *)listitem->data;
		gchar *title = g_strdup(editor->title);
		if ((editor->modified) && (editor->category != THOTH_NOTE)) {
			gchar *t = g_strconcat("(Modified) ", title, NULL);
			g_free(title);
			title = t;
		}
		GtkWidget *menuitem = thoth_addstatusmenuitem(title, ct, editor);
		g_free(title);

		listitem = listitem->next;
		ct++;
	}
	g_list_free(recentlist);
}

static void thoth_statusicon_activate_cb(GtkApplication *app, gpointer user_data)
{
	thoth_showhide(0, 0);
}

static void thoth_statusicon_popupmenu_cb(GtkApplication *app, gpointer user_data)
{
	thoth_updatestatusmenu();
	gtk_menu_popup_at_pointer(GTK_MENU(thoth_statusmenu), NULL);
}

/**
 * Generate a GList of open files, mainly used with the open file switcher.
 * Sets the following globals: GList *thoth_switcherlist,
 * 	gint thoth_switcherlist_index, gint thoth_switcherlist_count.
 * Returns TRUE on success, FALSE on error.
 */
static gboolean thoth_switcher_init()
{
	/* Reset the switcher list if needed */
	if (thoth_switcherlist) {
		GList *listitem = thoth_switcherlist;
		while (listitem != NULL) {
			ThothSwitcherStruct *switcher = (ThothSwitcherStruct *)listitem->data;
			g_free(switcher);
			listitem = listitem->next;
		}
		g_list_free(thoth_switcherlist);
		thoth_switcherlist = NULL;
	}

	/* Create and populate the button box */
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	GtkStyleContext *switchercontext = gtk_widget_get_style_context(GTK_WIDGET(hbox));
	gtk_style_context_add_class(switchercontext, "switcherhbox");

	GtkWidget *frame = gtk_frame_new(NULL);
	gtk_container_add(GTK_CONTAINER(frame), hbox);

	thoth_switcher = gtk_window_new(GTK_WINDOW_POPUP);
	gtk_container_add(GTK_CONTAINER(thoth_switcher), frame);

	thoth_switcherlist_count = 0;
	thoth_switcherlist_index = 0;

	/* Now add the buttons and save them to the switcher list */
	GList *switcherlist = thoth_get_recenteditorlist(thoth_switcherlist_max);
	GList *listitem = switcherlist;
	while (listitem != NULL) {
		GtkWidget *button = gtk_toggle_button_new();
		ThothEditorStruct *editor = (ThothEditorStruct *)listitem->data;
		gchar *title = g_strdup(editor->title);
		if ((editor->modified) && (editor->category != THOTH_NOTE)) {
			gchar *t = g_strconcat("(Modified) ", title, NULL);
			g_free(title);
			title = t;
		}
		GtkWidget *label = gtk_label_new(title);
		g_free(title);
		gtk_label_set_xalign(GTK_LABEL(label), 0.0);
		gtk_container_add(GTK_CONTAINER(button), label);
		GtkStyleContext *context = gtk_widget_get_style_context(GTK_WIDGET(button));
		gtk_style_context_add_class(context, "switcherbutton");
		gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);
		gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);

		ThothSwitcherStruct *switcher = g_malloc(sizeof(ThothSwitcherStruct));
		switcher->editor = (ThothEditorStruct *)listitem->data;
		switcher->button = button;
		thoth_switcherlist = g_list_append(thoth_switcherlist, switcher);
		thoth_switcherlist_count++;

		listitem = listitem->next;
	}
	g_list_free(switcherlist);

	gtk_window_set_transient_for(GTK_WINDOW(thoth_switcher), GTK_WINDOW(thoth_window));
	gtk_window_set_position(GTK_WINDOW(thoth_switcher), GTK_WIN_POS_CENTER_ON_PARENT);
	gtk_widget_show_all(thoth_switcher);

	return TRUE;
}

/**
 * Show open files is a popup window.
 * - direction: 1 for forwards, -1 for backwards.
 * Returns false if no menu shown.
 */
static gboolean thoth_switcher_cycle(gint direction)
{
	/* Create the menu if it doesn't exist */
	if (!thoth_switcher) {
		thoth_switcher_init();
	}

	thoth_switcherlist_index += direction;
	if (thoth_switcherlist_index > thoth_switcherlist_count - 1) {
		thoth_switcherlist_index = 0;
	} else if (thoth_switcherlist_index < 0) {
		thoth_switcherlist_index = thoth_switcherlist_count - 1;
	}

	gint ct = 0;
	GList *list = thoth_switcherlist;
	while (list != NULL) {
		ThothSwitcherStruct *switcher = ((ThothSwitcherStruct *)list->data);
		gboolean state = (thoth_switcherlist_index == ct);
		gtk_toggle_button_set_active((GtkToggleButton *)switcher->button, state);

		list = list->next;
		ct++;
	}

	return TRUE;
}

/**
 * Handle keypresses from anywhere in the app window.
 */
static gboolean thoth_app_key_press_event(GtkWidget *widget, GdkEventKey *evkey,
		gpointer data)
{
	switch(evkey->keyval) {
	case GDK_KEY_Control_L:
		thoth_modkeys |= THOTH_MODKEY_CTRL_L;
		break;

	case GDK_KEY_Control_R:
		thoth_modkeys |= THOTH_MODKEY_CTRL_R;
		break;

	case GDK_KEY_Tab:
		if (evkey->state & GDK_CONTROL_MASK) {
			thoth_switcher_cycle(1);
			return TRUE;
		}
		break;

	case GDK_KEY_ISO_Left_Tab:
		if (evkey->state & GDK_CONTROL_MASK) {
			thoth_switcher_cycle(-1);
			return TRUE;
		}
		break;

	//default:
		/* Unassigned key */
	}

	return FALSE;
}

/**
 * Handle key releases.
 * e.g. when Ctrl is released when cycling through the open file menu.
 */
static gboolean thoth_app_key_release_event(GtkWidget *widget, GdkEventKey *evkey,
		gpointer data)
{
	switch (evkey->keyval) {
	case GDK_KEY_Control_L:
		thoth_modkeys ^= THOTH_MODKEY_CTRL_L;
		break;

	case GDK_KEY_Control_R:
		thoth_modkeys ^= THOTH_MODKEY_CTRL_R;
		break;
	}

	if (thoth_switcher && !(thoth_modkeys & (THOTH_MODKEY_CTRL_L | THOTH_MODKEY_CTRL_R))) {
		gint ct = 0;
		GList *listitem = thoth_switcherlist;
		while (listitem != NULL) {
			ThothSwitcherStruct *switcher = (ThothSwitcherStruct *)listitem->data;
			if (thoth_switcherlist_index == ct) {
				ThothEditorStruct *editor = (ThothEditorStruct *)switcher->editor;
				GtkTreeIter *treeiter = thoth_treemodel_iter_find_id(
						thoth_treemodel, editor->id);
				thoth_select_sidepanel_iter(thoth_treemodel, treeiter, TRUE);
			}
			g_free(switcher);
			ct++;
			listitem = listitem->next;
		}
		g_list_free(thoth_switcherlist);
		thoth_switcherlist = NULL;
		gtk_widget_destroy(thoth_switcher);
		thoth_switcher = NULL;
	}
}

/**
 *
 */
static void thoth_new_window(GtkApplication *app)
{
	GtkAccelGroup *accel_group;
	GtkToolItem *toolitem;
	GtkWidget *sidepanelframe;
	GtkWidget *box, *sidepanelentry;
	GtkWidget *treeviewscrolledwindow;
	GtkTreeIter *treeiter;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkWidget *editorvbox;
	GtkWidget *grid;
	GtkWidget *label, *entry, *button, *checkbutton;
	GtkEntryBuffer *entrybuffer;
	GtkWidget *menu, *menuitem;
	GtkWidget *editmenu, *optionsmenu;

	/* Use AppDir (AppImage) resource locations, if possible */
	gchar *csspath, *iconpath;
	const gchar *appdir = getenv("APPDIR");
	if (appdir) {
		csspath = g_build_filename(appdir, "usr/share/thoth/thoth.css", NULL);
		iconpath = g_build_filename(appdir, "usr/share/thoth/thoth.png", NULL);
	} else {
		csspath = g_strdup("thoth.css");
		iconpath = g_strdup("thoth.png");
	}

	/* Set up styles */
	GtkCssProvider *provider = gtk_css_provider_new();
	GdkDisplay *display = gdk_display_get_default();
	GdkScreen *screen = gdk_display_get_default_screen(display);
	gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER(provider),
			GTK_STYLE_PROVIDER_PRIORITY_USER);
	gtk_css_provider_load_from_path(provider, csspath, NULL);

	thoth_window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(thoth_window), "Thoth");
	gtk_window_set_icon_from_file(GTK_WINDOW(thoth_window), iconpath, NULL);
	accel_group = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW(thoth_window), accel_group);
	g_signal_connect(G_OBJECT(thoth_window), "delete_event",
			G_CALLBACK(thoth_callback_delete_event), NULL);
	gulong appkeypressid = g_signal_connect(G_OBJECT(thoth_window), "key-press-event",
			G_CALLBACK(thoth_app_key_press_event), NULL);
	gulong appkeyreleaseid = g_signal_connect(G_OBJECT(thoth_window), "key-release-event",
			G_CALLBACK(thoth_app_key_release_event), NULL);

	GtkWidget *toolbar = gtk_toolbar_new();
	GtkStyleContext *titlebarcontext = gtk_widget_get_style_context(GTK_WIDGET(toolbar));
	gtk_style_context_add_class(titlebarcontext, "thothtitlebar");
	gtk_toolbar_set_icon_size(GTK_TOOLBAR(toolbar), GTK_ICON_SIZE_MENU);
	gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS);
	gtk_window_set_titlebar(GTK_WINDOW(thoth_window), toolbar);

	sidepanelframe = gtk_frame_new(NULL);
	thoth_editoroverlay = gtk_overlay_new();

	thoth_editorpaned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_paned_pack1(GTK_PANED(thoth_editorpaned), sidepanelframe, FALSE, TRUE);
	gtk_paned_pack2(GTK_PANED(thoth_editorpaned), thoth_editoroverlay, TRUE, TRUE);
	gtk_widget_set_size_request(sidepanelframe, 200, -1);
	gtk_container_add(GTK_CONTAINER(thoth_window), thoth_editorpaned);
	gtk_widget_show(sidepanelframe);
	thoth_sidepanelvisible = TRUE;
	thoth_sidepanelframe = sidepanelframe;

	gtk_window_resize(GTK_WINDOW(thoth_window), 640, 512);
	gtk_widget_show_all(thoth_window);

	/* Pack editor frame */
	editorvbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show(editorvbox);

	thoth_findwidget = gtk_frame_new(NULL); /* Show/hide this */
	gtk_box_pack_start(GTK_BOX(editorvbox), thoth_findwidget, FALSE, FALSE, 0);
	grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(thoth_findwidget), grid);
	gtk_grid_set_column_homogeneous(GTK_GRID(grid), FALSE);
	gtk_widget_show(grid);

	/* TODO: Add GtkEntryCompletion to find field using words in autocomplete list. */
	label = gtk_label_new("Find:");
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 1, 1, 1);
	entrybuffer = gtk_entry_buffer_new(NULL, -1);
	thoth_findentry = thoth_entry_new_with_buffer(GTK_ENTRY_BUFFER(entrybuffer));
	gtk_widget_set_hexpand(thoth_findentry, TRUE);
	gtk_widget_show(thoth_findentry);
	gtk_grid_attach(GTK_GRID(grid), thoth_findentry, 2, 1, 1, 1);
	g_signal_connect(thoth_findentry, "activate",
			G_CALLBACK(thoth_command_activate), GINT_TO_POINTER(COMMAND_FIND_NEXT));

	label = gtk_label_new("Replace:");
	gtk_widget_show(label);
	gtk_grid_attach(GTK_GRID(grid), label, 1, 2, 1, 1);
	entrybuffer = gtk_entry_buffer_new(NULL, -1);
	thoth_replaceentry = thoth_entry_new_with_buffer(GTK_ENTRY_BUFFER(entrybuffer));
	gtk_widget_set_hexpand(thoth_replaceentry, TRUE);
	gtk_widget_show(thoth_replaceentry);
	gtk_grid_attach(GTK_GRID(grid), thoth_replaceentry, 2, 2, 1, 1);
	box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_show(box);
	gtk_grid_attach(GTK_GRID(grid), box, 1, 3, 2, 1);

	/* return widget in following lines to add accelerator keys (F3 / Shift+F3, etc.) */
	thoth_add_box_button(box, COMMAND_FIND_PREVIOUS, "go-previous",
			"Find Backwards (Shift+F3)",
			accel_group, GDK_KEY_F3, GDK_SHIFT_MASK);
	thoth_add_box_button(box, COMMAND_FIND_NEXT, "go-next", "Find Next (F3)",
			accel_group, GDK_KEY_F3, 0);
	thoth_add_box_button(box, COMMAND_FIND_REPLACE, "insert-text",
			"Replace and Find Next (Ctrl+H)",
			accel_group, GDK_KEY_H, GDK_CONTROL_MASK);
	thoth_add_box_button(box, COMMAND_FIND_REPLACE_ALL, "edit-select-all", "Replace All",
			0, 0, 0);
	thoth_casesensitivecheck = gtk_check_button_new_with_mnemonic("_Case Sensitive");
	gtk_widget_show(thoth_casesensitivecheck);
	gtk_box_pack_start(GTK_BOX(box), thoth_casesensitivecheck, TRUE, TRUE, 0);
	/* Set Actions here. */
	thoth_add_box_button(box, COMMAND_FIND_CLOSE, "window-close", "Close Find dialog",
			accel_group, GDK_KEY_Escape, 0);

	thoth_notebook = gtk_notebook_new();
	gtk_notebook_set_show_tabs(GTK_NOTEBOOK(thoth_notebook), FALSE);
	gtk_notebook_set_show_border(GTK_NOTEBOOK(thoth_notebook), FALSE);
	gtk_widget_show(thoth_notebook);
	gtk_widget_set_hexpand(thoth_notebook, TRUE);
	gtk_widget_set_vexpand(thoth_notebook, TRUE);
	gtk_box_pack_end(GTK_BOX(editorvbox), thoth_notebook, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(thoth_notebook), "switch-page",
			G_CALLBACK(thoth_notebook_switch_page), NULL);
	gtk_container_add(GTK_CONTAINER(thoth_editoroverlay), editorvbox);

	/* Pack toolbar items */
	toolitem = gtk_toggle_tool_button_new();
	gtk_widget_set_tooltip_text(GTK_WIDGET(toolitem), "Places & Bookmarks (F2)");
	gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(toolitem), "user-bookmarks-symbolic");
	gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(toolitem), TRUE);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolitem, -1);
	gtk_widget_add_accelerator(gtk_bin_get_child(GTK_BIN(toolitem)), "clicked",
			accel_group, GDK_KEY_F2, 0, 0);
	g_signal_connect(G_OBJECT(toolitem), "clicked", G_CALLBACK(thoth_command_activate),
			GINT_TO_POINTER(COMMAND_SIDEPANEL));
	thoth_sidepaneltoolitem = toolitem;

	toolitem = gtk_toggle_tool_button_new();
	gtk_widget_set_tooltip_text(GTK_WIDGET(toolitem), "Files Menu (Alt+F)");
	gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(toolitem),
			"document-properties-symbolic");
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolitem, -1);
	gtk_widget_add_accelerator(gtk_bin_get_child(GTK_BIN(toolitem)), "clicked",
			accel_group, GDK_KEY_F, GDK_MOD1_MASK, 0);
	menu = gtk_menu_new();
	g_signal_connect(G_OBJECT(toolitem), "clicked",
			G_CALLBACK(thoth_toggle_menu), menu);
	thoth_add_menu_item(menu, FILE_NEW, "_New", FALSE,
			accel_group, GDK_KEY_N, GDK_CONTROL_MASK);
	thoth_add_menu_item(menu, FILE_OPEN, "_Open...", FALSE,
			accel_group, GDK_KEY_O, GDK_CONTROL_MASK);
	thoth_add_menu_item(menu, FILE_OPEN_WEB_ADDRESS, "Open _Web Address...", FALSE,
			accel_group, GDK_KEY_L, GDK_CONTROL_MASK);
	thoth_add_menu_item(menu, FILE_RELOAD, "_Reload", FALSE,
			accel_group, GDK_KEY_F5, 0);
	thoth_add_menu_item(menu, FILE_SAVE_AS, "Save _As...", FALSE,
			accel_group, GDK_KEY_S, GDK_CONTROL_MASK | GDK_SHIFT_MASK);
	thoth_add_menu_item(menu, FILE_SAVE_TO_WEB, "Save _to Web...", FALSE,
			accel_group, GDK_KEY_T, GDK_CONTROL_MASK | GDK_SHIFT_MASK);
	thoth_add_menu_item(menu, FILE_SHARE, "S_hare...", FALSE, 0, 0, 0);
	gtk_widget_set_sensitive(thoth_menuitem[FILE_SHARE], FALSE); /* Disable until feature complete */
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), gtk_separator_menu_item_new());
	thoth_add_menu_item(menu, FILE_PRINT_PREVIEW, "Print Pre_view...", FALSE,
			accel_group, GDK_KEY_P, GDK_CONTROL_MASK | GDK_SHIFT_MASK);
	thoth_add_menu_item(menu, FILE_PRINT, "_Print...", FALSE,
			accel_group, GDK_KEY_P, GDK_CONTROL_MASK);
	thoth_add_menu_item(menu, FILE_EXPORT, "_Export HTML/PDF...", FALSE,
			accel_group, GDK_KEY_E, GDK_CONTROL_MASK);
	gtk_widget_set_sensitive(thoth_menuitem[FILE_EXPORT], FALSE); /* Disable until feature complete */
	thoth_add_menu_item(menu, FILE_PROPERTIES, "Properti_es...", FALSE, 0, 0, 0);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), gtk_separator_menu_item_new());
	thoth_add_menu_item(menu, FILE_CLOSE, "_Close File", FALSE,
			accel_group, GDK_KEY_W, GDK_CONTROL_MASK);
	thoth_add_menu_item(menu, FILE_QUIT, "_Quit", FALSE,
			accel_group, GDK_KEY_Q, GDK_CONTROL_MASK);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), gtk_separator_menu_item_new());
	thoth_add_menu_item(menu, FILE_RECENT_PLACEHOLDER, "Recent files", FALSE, 0, 0, 0);
	gtk_widget_set_sensitive(thoth_menuitem[FILE_RECENT_PLACEHOLDER], FALSE);
	gtk_widget_show_all(menu);
	thoth_filesmenu = menu;

	thoth_savetoolitem = thoth_add_toolbar_item(toolbar, COMMAND_SAVE,
			"document-save-symbolic", "Save (Ctrl+S)", TRUE,
			accel_group, GDK_KEY_S, GDK_CONTROL_MASK);

	toolitem = gtk_toggle_tool_button_new();
	gtk_widget_set_tooltip_text(GTK_WIDGET(toolitem), "Edit Menu (Alt+E)");
	gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(toolitem), "edit-find-replace-symbolic");
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolitem, -1);
	gtk_widget_add_accelerator(gtk_bin_get_child(GTK_BIN(toolitem)), "clicked",
			accel_group, GDK_KEY_E, GDK_MOD1_MASK, 0);
	editmenu = gtk_menu_new();
	g_signal_connect(G_OBJECT(toolitem), "clicked",
			G_CALLBACK(thoth_toggle_menu), editmenu);
	thoth_add_menu_item(editmenu, EDIT_CUT, "Cu_t", FALSE,
			accel_group, GDK_KEY_X, GDK_CONTROL_MASK);
	thoth_add_menu_item(editmenu, EDIT_COPY, "_Copy", FALSE,
			accel_group, GDK_KEY_C, GDK_CONTROL_MASK);
	thoth_add_menu_item(editmenu, EDIT_PASTE, "_Paste", FALSE,
			accel_group, GDK_KEY_V, GDK_CONTROL_MASK);
	gtk_menu_shell_append(GTK_MENU_SHELL(editmenu), gtk_separator_menu_item_new());
	thoth_add_menu_item(editmenu, EDIT_SELECT_ALL, "Select _All", FALSE,
			accel_group, GDK_KEY_A, GDK_CONTROL_MASK);
	gtk_menu_shell_append(GTK_MENU_SHELL(editmenu), gtk_separator_menu_item_new());
	thoth_add_menu_item(editmenu, EDIT_FIND, "Find/Replace...", FALSE,
			accel_group, GDK_KEY_F, GDK_CONTROL_MASK);
	thoth_add_menu_item(editmenu, EDIT_JUMPTO, "Jump to line...", FALSE,
			accel_group, GDK_KEY_J, GDK_CONTROL_MASK);
	gtk_menu_shell_append(GTK_MENU_SHELL(editmenu), gtk_separator_menu_item_new());
	thoth_add_menu_item(editmenu, EDIT_INDENT, "Indent", FALSE,
			accel_group, GDK_KEY_Tab, 0);
	thoth_add_menu_item(editmenu, EDIT_UNINDENT, "Unindent", FALSE,
			accel_group, GDK_KEY_Tab, GDK_SHIFT_MASK);
	gtk_menu_shell_append(GTK_MENU_SHELL(editmenu), gtk_separator_menu_item_new());
	thoth_add_menu_item(editmenu, EDIT_UPPERCASE, "_UPPERCASE", FALSE, 0, 0, 0);
	thoth_add_menu_item(editmenu, EDIT_LOWERCASE, "_lowercase", FALSE, 0, 0, 0);
//	thoth_add_menu_item(editmenu, MENU_INCOMPLETE, "Sort Lines", FALSE, 0, 0, 0);
//	thoth_add_menu_item(editmenu, MENU_INCOMPLETE, "Sort Lines Backwards", FALSE, 0, 0, 0);
//	thoth_add_menu_item(editmenu, MENU_INCOMPLETE, "Reverse case", FALSE, 0, 0, 0);
	gtk_menu_shell_append(GTK_MENU_SHELL(editmenu), gtk_separator_menu_item_new());
//	thoth_add_menu_item(editmenu, MENU_INCOMPLETE, "Remove line breaks between sentences",
//			FALSE, 0, 0, 0);
//	thoth_add_menu_item(editmenu, MENU_INCOMPLETE, "Remove all line breaks", FALSE, 0, 0, 0);
//	thoth_add_menu_item(editmenu, MENU_INCOMPLETE, "Remove leading spaces", FALSE, 0, 0, 0);
//	thoth_add_menu_item(editmenu, MENU_INCOMPLETE, "Remove excessive spaces", FALSE, 0, 0, 0);
	thoth_add_menu_item(editmenu, EDIT_REMOVE_TRAILING_SPACES, "Remove trailing spaces", FALSE, 0, 0, 0);
	gtk_widget_show_all(editmenu);

	thoth_undotoolitem = thoth_add_toolbar_item(toolbar, COMMAND_UNDO,
			"edit-undo-symbolic", "Undo (Ctrl+Z)", FALSE,
			accel_group, GDK_KEY_Z, GDK_CONTROL_MASK);
	thoth_redotoolitem = thoth_add_toolbar_item(toolbar, COMMAND_REDO,
			"edit-redo-symbolic", "Redo (Ctrl+Y)", FALSE,
			accel_group, GDK_KEY_Y, GDK_CONTROL_MASK);

	toolitem = gtk_toggle_tool_button_new();
	gtk_widget_set_tooltip_text(GTK_WIDGET(toolitem), "Options Menu (Alt+O)");
	gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(toolitem), "document-page-setup-symbolic");
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolitem, -1);
	gtk_widget_add_accelerator(gtk_bin_get_child(GTK_BIN(toolitem)), "clicked",
			accel_group, GDK_KEY_O, GDK_MOD1_MASK, 0);
	optionsmenu = gtk_menu_new();
	g_signal_connect(G_OBJECT(toolitem), "clicked",
			G_CALLBACK(thoth_toggle_menu), optionsmenu);
	thoth_add_menu_item(optionsmenu, OPTIONS_WRAP_LINES, "_Wrap lines", TRUE, 0, 0, 0);
	thoth_add_menu_item(optionsmenu, OPTIONS_SHOW_LINE_NUMBERS,
			"_Line numbers", TRUE, 0, 0, 0);
//	thoth_add_menu_item(optionsmenu, MENU_INCOMPLETE, "Show formatting marks", TRUE,
//			0, 0, 0);
//	thoth_add_menu_item(optionsmenu, MENU_INCOMPLETE, "Hilite current line", TRUE,
//			0, 0, 0);
//	thoth_add_menu_item(optionsmenu, MENU_INCOMPLETE, "Hilite matching brackets", TRUE,
//			0, 0, 0);
	thoth_add_menu_item(optionsmenu, OPTIONS_AUTOINDENT, "_Autoindent", TRUE, 0, 0, 0);
	thoth_add_menu_item(optionsmenu, OPTIONS_KEEP_ABOVE, "_Keep Above", TRUE, 0, 0, 0);
	thoth_add_menu_item(optionsmenu, OPTIONS_REOPEN_FILES_ON_STARTUP,
			"_Reopen files on startup", TRUE, 0, 0, 0);
	gtk_menu_shell_append(GTK_MENU_SHELL(optionsmenu), gtk_separator_menu_item_new());
	thoth_add_menu_item(optionsmenu, OPTIONS_PREFERENCES, "_Preferences...", FALSE, 0, 0, 0);
	thoth_add_menu_item(optionsmenu, OPTIONS_ABOUT,
			"About _Thoth...", FALSE, 0, 0, 0);
	gtk_widget_show_all(optionsmenu);

	toolitem = gtk_tool_item_new();
	gtk_tool_item_set_expand(toolitem, TRUE);
	thoth_label = gtk_label_new(NULL);
	gtk_container_add(GTK_CONTAINER(toolitem), thoth_label);
	gtk_widget_set_halign(thoth_label, GTK_ALIGN_START);
	gtk_widget_set_hexpand(thoth_label, TRUE);
	gtk_label_set_ellipsize(GTK_LABEL(thoth_label), PANGO_ELLIPSIZE_END);
	gtk_label_set_single_line_mode(GTK_LABEL(thoth_label), TRUE);
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), toolitem, -1);

	toolitem = thoth_add_toolbar_item(toolbar, COMMAND_CLOSE_WINDOW,
			"window-close-symbolic", "Quit (Ctrl+Q)", TRUE,
			accel_group, FALSE, GDK_CONTROL_MASK);

	gtk_widget_show_all(GTK_WIDGET(toolbar));

	/* Pack sidepanel */
	box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	sidepanelentry = gtk_search_entry_new();
	gtk_box_pack_start(GTK_BOX(box), sidepanelentry, FALSE, FALSE, 0);
	g_signal_connect(GTK_SEARCH_ENTRY(sidepanelentry), "search-changed",
			G_CALLBACK(thoth_callback_sidepanelentry_search_changed), NULL);
	g_signal_connect(sidepanelentry, "focus-in-event",
			G_CALLBACK(thoth_callback_textview_focus_change), (gpointer)FALSE);
	g_signal_connect(sidepanelentry, "focus-out-event",
			G_CALLBACK(thoth_callback_textview_focus_change), (gpointer)TRUE);

	treeviewscrolledwindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(treeviewscrolledwindow);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(treeviewscrolledwindow),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start(GTK_BOX(box), treeviewscrolledwindow, TRUE, TRUE, 0);
	gtk_widget_show_all(GTK_WIDGET(box));
	gtk_container_add(GTK_CONTAINER(sidepanelframe), box);

	/* Pack treeview. Read "Treestore columns" above for what each one is. */
	thoth_treemodel = GTK_TREE_MODEL(gtk_tree_store_new(SPL_NUM_COLS,
			GDK_TYPE_PIXBUF, GDK_TYPE_RGBA, G_TYPE_STRING,
			G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN,
			G_TYPE_BOOLEAN, G_TYPE_INT, G_TYPE_INT));
	thoth_treeview = gtk_tree_view_new_with_model(thoth_treemodel);
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes(
			"Filename", renderer, "text", SPL_COL_TITLE,
			"foreground-rgba", SPL_COL_COLOUR, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(thoth_treeview), column);
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(thoth_treeview), FALSE);
	gtk_tree_view_set_activate_on_single_click(GTK_TREE_VIEW(thoth_treeview), TRUE);
	gtk_tree_view_set_tooltip_column(GTK_TREE_VIEW(thoth_treeview), SPL_COL_TOOLTIP);
	gtk_container_add(GTK_CONTAINER(treeviewscrolledwindow), thoth_treeview);
	gtk_widget_show(thoth_treeview);
	g_signal_connect(GTK_TREE_VIEW(thoth_treeview), "button-press-event",
			G_CALLBACK(thoth_callback_treeview_button_press_event), NULL);
	g_signal_connect(GTK_TREE_VIEW(thoth_treeview), "key-press-event",
			G_CALLBACK(thoth_callback_treeview_key_press_event), NULL);
	g_signal_connect(GTK_TREE_VIEW(thoth_treeview), "row-activated",
			G_CALLBACK(thoth_callback_treeview_row_activated), NULL);
	GtkTreeSelection *treesel = gtk_tree_view_get_selection(GTK_TREE_VIEW(thoth_treeview));
	gtk_tree_selection_set_mode(treesel, GTK_SELECTION_BROWSE);

	GtkStyleContext *sidepanelctx = gtk_widget_get_style_context(GTK_WIDGET(treeviewscrolledwindow));
	gtk_style_context_add_class(sidepanelctx, "thothsidepaneltree");

	GdkRGBA rgba;
	gdk_rgba_parse(&rgba, "#888");
	gtk_tree_store_append(GTK_TREE_STORE(thoth_treemodel), &thoth_pagesiter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(thoth_treemodel), &thoth_pagesiter,
			SPL_COL_TITLE, "Pages",
			SPL_COL_TOOLTIP, "Notes saved on another server.",
			SPL_COL_VISIBLE, TRUE,
			SPL_COL_COLOUR, &rgba, -1);

	gtk_tree_store_append(GTK_TREE_STORE(thoth_treemodel), &thoth_notesiter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(thoth_treemodel), &thoth_notesiter,
			SPL_COL_TITLE, "Notes",
			SPL_COL_TOOLTIP, "Notes saved on this device.",
			SPL_COL_VISIBLE, TRUE,
			SPL_COL_COLOUR, &rgba, -1);

	gtk_tree_store_append(GTK_TREE_STORE(thoth_treemodel), &thoth_openfilesiter, NULL);
	gtk_tree_store_set(GTK_TREE_STORE(thoth_treemodel), &thoth_openfilesiter,
			SPL_COL_TITLE, "Open Files",
			SPL_COL_TOOLTIP, "Files currently opened.",
			SPL_COL_VISIBLE, TRUE,
			SPL_COL_COLOUR, &rgba, -1);

	thoth_protocol = NULL;
	thoth_username = NULL;
	thoth_password = NULL;
	thoth_savepassword = FALSE;
	thoth_switcher = NULL;
	thoth_switcherlist_index = 0;
	thoth_statusmenulist = NULL;

	thoth_load_config();
	thoth_load_uris(NULL);
	g_timeout_add_seconds(AUTOSAVE_TIMEOUT_SECONDS, thoth_autosave_timeout, NULL);

	if (!thoth_editorlist) {
		thoth_command_file_new();
	}

	/* Set up status icon */
	if (!thoth_statusicon) {
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
		thoth_statusicon = gtk_status_icon_new_from_file(iconpath);
G_GNUC_END_IGNORE_DEPRECATIONS
		g_signal_connect(G_OBJECT(thoth_statusicon), "activate",
				G_CALLBACK(thoth_statusicon_activate_cb), NULL);
		g_signal_connect(G_OBJECT(thoth_statusicon), "popup-menu",
				G_CALLBACK(thoth_statusicon_popupmenu_cb), NULL);
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
		gtk_status_icon_set_visible(thoth_statusicon, TRUE);
		gtk_status_icon_set_tooltip_text(thoth_statusicon, "Thoth");
G_GNUC_END_IGNORE_DEPRECATIONS

		thoth_statusmenu = gtk_menu_new();
		GtkWidget *menuitem;

		gtk_menu_shell_append(GTK_MENU_SHELL(thoth_statusmenu),
				gtk_separator_menu_item_new());
		thoth_add_menu_item(thoth_statusmenu, COMMAND_NEW, "_New", FALSE, 0, 0, 0);
		thoth_add_menu_item(thoth_statusmenu, COMMAND_PASTE_NEW, "_Paste as New", FALSE, 0, 0, 0);
		thoth_add_menu_item(thoth_statusmenu, COMMAND_OPEN, "_Open...", FALSE, 0, 0, 0);
		thoth_add_menu_item(thoth_statusmenu, COMMAND_OPEN_WEB, "Open _Web Address...", FALSE, 0, 0, 0);
		menuitem = thoth_addstatusmenuseparator("Thoth Notes");
		thoth_add_menu_item(thoth_statusmenu, COMMAND_QUIT, "_Quit", FALSE, 0, 0, 0);
		gtk_widget_show_all(thoth_statusmenu);
	}

	thoth_loaded = TRUE;
}

static void thoth_activate_cb(GtkApplication *app, gpointer user_data)
{
	thoth_new_window(app);
}

static void thoth_open_cb(GtkApplication *app, GFile **files, gint n_files, const gchar *hint)
{
	thoth_new_window(app);

	gint i;
	for (i = 0; i < n_files; i++) {
		gchar *uri = g_file_get_uri(files[i]);
		gint editorid = thoth_open_file(uri, THOTH_FILE, TRUE, FALSE);
		if (editorid) {
			gchar *filename = thoth_filename_from_uri(uri);
			thoth_add_recent_uri(uri, TRUE);
			thoth_prune_recent_uri_list();
			thoth_add_sidepanel_item(&thoth_openfilesiter, THOTH_FILE, editorid,
					uri, filename, TRUE);
			thoth_set_current_uri(TRUE);
			g_free(filename);
		}
		g_free(uri);
	}
}

int main(int argc, char **argv)
{
	int status;

	thoth_app = gtk_application_new("com.mirrorisland.thoth", G_APPLICATION_HANDLES_OPEN);
	g_signal_connect(thoth_app, "activate", G_CALLBACK(thoth_activate_cb), NULL);
	g_signal_connect(thoth_app, "open", G_CALLBACK(thoth_open_cb), NULL);
	g_application_set_inactivity_timeout(G_APPLICATION(thoth_app), 10000);

	status = g_application_run(G_APPLICATION(thoth_app), argc, argv);
	g_object_unref(thoth_app);

	return status;
}

