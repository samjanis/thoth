#include <gtk/gtk.h>

#define THOTH_VERSION_MAJOR 0
#define THOTH_VERSION_MINOR 5
#define THOTH_VERSION_PATCH 4
#define THOTH_COPYRIGHT_YEAR 2024

#define MIN_FONT_SIZE 6
#define MAX_FONT_SIZE 72

#define TYPE_TICB_MENU_ITEM         (ticb_menu_item_get_type())
#define TICB_MENU_ITEM(o)           (G_TYPE_CHECK_INSTANCE_CAST((o), TYPE_TICB_MENU_ITEM, TicbMenuItem))
#define TICB_MENU_ITEM_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), TYPE_TICB_MENU_ITEM, TicbMenuItemClass))
#define IS_TICB_MENU_ITEM(o)        (G_TYPE_CHECK_INSTANCE_TYPE((o), TYPE_TICB_MENU_ITEM))
#define IS_TICB_MENU_ITEM_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), TYPE_TICB_MENU_ITEM))
#define TICB_MENU_ITEM_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS((o), TYPE_TICB_MENU_ITEM, TicbMenuItemClass))

typedef struct _TicbMenuItem TicbMenuItem;
typedef struct _TicbMenuItemClass TicbMenuItemClass;
typedef struct _TicbMenuItemPrivate TicbMenuItemPrivate;

struct _TicbMenuItem {
	GtkMenuItem parent_instance;
	TicbMenuItemPrivate *priv;
};

struct _TicbMenuItemClass {
	GtkMenuItemClass parent_class;
};

GType ticb_menu_item_get_type (void) G_GNUC_CONST;
