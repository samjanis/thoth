Thoth - A compact note taker and text editor.
============================================================

![Thoth Screenshot](thoth-screenshot.png)

Sam Allen <samjanis@mirrorisland.com>

https://www.mirrorisland.com/thoth

https://gitlab.com/samjanis/thoth

Thoth is a compact text editor with note taking and networking features.
It is designed to be supported on all major platforms including
Windows, macOS, GNU/Linux, and Android.

This program is released under the GNU General Public License (GPL)
Version 3.0


Current Features
----------------

- Standard edit functions (undo/redo, find/replace, autoindent)
- Reopen files being edited on startup
- Note taking
- Read and save posts from Mirror Island
- Edit files over FTP


How Note Taking Works
---------------------

When a new text file is made (e.g. by File->New) it becomes a note
that automatically saves changes as you make them. They are also found
under "Notes" in the side panel.

However, when they are saved as an actual file (i.e. by File->Save As...)
they became regular text files and no longer save changes automatically.

Features In Progress
--------------------

- Autocomplete
- Upload to a git server
- Encryption and decryption
- Markdown support
- Spell checking
- Printing
- Export as PDF and HTML


System Requirements
-------------------

- Any computer or device capable of running GTK3.
- Network access is not required but can be used for uploading and file sharing.
- GCC or equivalent if compiling from source.
- The following development headers if compiling from source:
	- libgtk-3 (for the user interface)
	- libcurl (network access)
	- libssl (encryption/decryption)

Compiling
---------

Before compiling the required development headers
need to be installed.

On Debian-compatible operating systems (Ubuntu, SteamOS,
etc.) or Ubuntu derivatives they can be installed using
the command (as root or with `sudo`):

```
apt install libgtk-3-dev libcurl4-openssl-dev libssl-dev
```


With the required headers and libraries installed,
Thoth can be compiled using the command:

```
gcc src/main.c `pkg-config --libs --cflags gtk+-3.0 libcurl libcrypto` -o thoth
```

Running Thoth
-------------

Once compiled, running the `thoth` executable is all that's
needed to get it started. Thoth also accepts filenames as command-line options to open that file for editing.


Known Issues
------------

During compiling, the warning "'gtk_widget_override_font' is deprecated" will
be displayed. This is because GTK3 encourages UI styling through CSS.
While not an actual error, the program will function properly when run.

